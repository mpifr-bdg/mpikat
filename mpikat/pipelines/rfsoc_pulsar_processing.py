"""
The EddPulsarPipeline receives data in MPIFR EDD packetizer Format from both
polarizations and create different pulsar data product or capture baseband data. Computing
requires one numa node for both polarizations.

Data is received using `mkrecv`_, stored in a `dada`_ buffer, the merged to
dspsr readable format.



Configuration Settings
----------------------

    mode
        Timing, Searching, Baseband, Leap

    cdd_dm
        dm for coherent filterbanking, tested up to 3000, this will overwrite the DM got from par file if it is non-zero

    npol
        For search mode product, output 1 (Intensity) or 4 (Coherence) products

    decimation
        decimation factor in frequency for filterbank output

    filterbank_nchannels
        number of filterbank channels before decimation in digifits

    tsamp
        default value for filterbank time resolution

    nsblk
        number of samples per block

    file_length
        filterbank file length in time, it must be the multiples of tsamp * nsblk.

    zaplist
        frequency zap list (List of frequency ranges [MHz] that are blanked in the online display. )

    tzpar
        timing solution will be read from this directory

    nchannels
        number of channels in pulsar archive, used in timing mode

    nbins
        number of time bins in pulsar archive, used in timing mode

    tempo2_telescope_name
        telescope name for tempo2 predictor calculation (e.g. Effelsberg, Meerkat)

    tempo2_ntimecoeff
        number of time coefficients

    tempo2_nfreqcoeff
        number of freq coefficients

    merge_application
        edd_merge, edd_merge_10to8, edd_roach_merge, edd_pfb_merge

    npart
        number of parts need to be meaged from input data stream

    merge_threads
        number of threads used merging input streams.

    dspsr_threads
        number of threads used dspsr process.

    fft_length
        fft filter length for dedispersion filter


Output Data Product
-------------------
    All data products will be written to disk under the pipeline_name

.. _mkrecv:
    https://gitlab.mpifr-bonn.mpg.de/mhein/mkrecv

.. _dada:
    http://psrdada.sourceforge.net/

.. _dspsr:
    http://dspsr.sourceforge.net

"""


import asyncio
import shlex
import shutil
import os
import base64
from subprocess import Popen, PIPE, TimeoutExpired
import tempfile
import datetime
import json
import time
import copy
import re

import aiokatcp
import pytz
import git

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from astropy.time import Time
import astropy.units as u
from astropy.coordinates import SkyCoord
from aiokatcp.sensor import Sensor

import mpikat.pipelines.digpack_controller  # pylint: disable=unused-import
from mpikat.utils.sensor_watchdog import conditional_update
from mpikat.utils.process_tools import ManagedProcess, command_watcher
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.mk_tools import MkrecvSensors
import mpikat.utils.dada_tools as dada
from mpikat.utils import numa
from mpikat.utils import output
from mpikat.utils.core_manager import CoreManager

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change, StateChange
from mpikat.core.datastore import EDDDataStore
from mpikat.core import logger
from mpikat.utils.dada_header_rfsoc import render_dada_header, make_dada_key_string
from mpikat.pipelines.pulsar_processing_blank_image import BLANK_IMAGE
from mpikat.core.data_stream import DataStreamRegistry
#from datetime import datetime
_log = logger.getLogger("mpikat.pipelines.PulsarProcessing")


DataStreamRegistry.register({"type": "RFSOC:1",
                             "description": "Channelized complex voltage ouptut.",
                             "ip": None,
                             "port": None,
                             "sample_rate": None,
                             "central_freq": None,
                             "sync_time": None,
                             "predecimation_factor": None,
                             })


_DEFAULT_CONFIG = {
    "eddid": "",
    "active": 1,
    "id": "PulsarPipeline",
    "type": "PulsarPipeline",
    "dummy_input" : False,                   # Use dummy input instead of mkrecv process. Should be input data stream option.
    "mode": "Timing",                        # Timing, Searching, Baseband, Leap
    "cdd_dm": 0,                             # DM for coherent filterbanking, tested up to 3000, this will overwrite the DM got from par file if it is non-zero
    "npol": 4,                               # For search mode product, output 1 (Intensity) or 4 (Coherence) products
    "single_pol": 0,                         # Flag for enable single polarization mode
    "pol_number": 0,                         # Which polarization stream to capture
    "decimation": 1,                         # Decimation in frequency for filterbank output
    "filterbank_nchannels": 512,             # Number of filterbank channels before decimation in digifits
    "tsamp": 0.0000512,                      # Default value for filterbank time Gresolution
    "nsblk": 2048,                           # Number of samples per block
    "file_length": "",                       # Filterbank file length in time, it must be the multiples of tsamp * nsblk.
    "zaplist": "1200:1230",                  # Frequency zap list
    "tzpar_repo": "https://gitlab.mpcdf.mpg.de/jasonwu/eff_psr_obs.git",  # Git repository for storing par files
    "tzpar_path": "tempo1/tzpar",             # path to par files
    "git_branch": "master",                   # Which git branch to use
    "git_flag": 1,                           # 0 for local version, 1 for pulling from git
    "nchannels": 1024,                        # Number of channels in pulsar archive, only used in timing mode
    "nbins": 1024,                           # Number of time bins in pulsar archive, only used in timing mode
    "tempo2_telescope_name": "Effelsberg",   # Effelsberg, Meerkat
    "tempo2_ntimecoeff": 12,                 # Number of time coefficient used for predictor calculation
    "tempo2_nfreqcoeff": 2,                  # Number of frequency coefficient used for predictor calculation
    "predictor_block_length": 3599.999999999,  # Block length of predictor in seconds for predictor calculation
    "predictor_mjd_half_range": 0.5,         # Half range of MJD for predictor calculation time range
    "merge_type": "pol",
    "npart": 2,                              # Number of groups (either polarisations or multicast groups from SKARAB output) capturing from the data stream
    "heap_size":1024,
    "merge_threads": 2,                      # Number of threads used for merging capture raw data
    "dspsr_threads": 1,                      # Number of threads used for dspsr processing
    "dbdisk_writing_threads": 2,             # Number of threads used for dbdisk_multithread writing
    "fft_length": 16384,                      # 2048 for searching -x option in dspsr
    "mkrecv_nthreads": 4,                    # number of threads used for mkrecv_v4 capture
    "mkrecv_ncore": 4,                       # number of core used for mkrecv_v4 capture
    "intergration_time": 10,
    "cal_period": 1.0,
    "effelsberg_cal_convention": True,       # source names ending in _R trigger calibrator scans
    "timezone" : 'Europe/Berlin',
    "data_directory": "auto",                # 'auto' - automatically choose directory, otherwise use string. Keywords in {} like {source_name} will be replaced by the current value from the data store
    "data_root_directory": "/beegfsEDD/JWU/",
    "input_data_streams":
    [   
        {
            "format": "RFSOC:1",
            "ip": "239.0.0.120+3",
            "port": "60002",
            "sample_rate":2048000000,
            "bit_depth": 8,
            "sync_time":0,
            "central_freq" : 2048
        }
    ],      
    "dada_header_params":
    {
        "filesize": 838860800,
        "instrument": "EDD",
        "receiver_name": "P217",
        "mode": "PSR",
        "calfreq": 1,
        "npol": 2,
        "bandwidth": 512,
        "bandwidth_hz": 512000000,
        "frequency_hz": 1486000000,
        "resolution": 1,
        "tsamp": 0.00125,
        "sample_clock": "unset",
        "heaps_nbytes": 1024,
        "nindices": 3,
        "idx1_step": 1048576,
        "idx1_modulo": "unset",
        "idx2_item": 1,
        "idx2_list": "0,1",
        "idx2_mask": "0x1",
        "idx3_item": 1,
        "idx3_list": "0:1024:2",
        "idx3_mask": "0xffe",
        "tsamp_1": 1e-6,
        "nsamples_1": 512,
        "nantennas": 1,
        "nchans": 512,
        "slots_skip": 4,
        "dada_nslots": 3,
        },
    "dspsr_params":
    {
        "args": "-r -minram 1024"
    },
    "dada":
    {
        "size": "838860800",
        "number": "20"
    },
    "dadc":
    {
        "size": "838860800",
        "number": "20"
    }
}


async def time_it(func):
    start_time = time.monotonic()
    try:
        result = await func
    finally:
        end_time = time.monotonic()
        _log.debug(f"{func.__name__} took {end_time - start_time} seconds to execute.")
    return result

class ArchiveAdder(FileSystemEventHandler):
    """
    Create time, frequency scrunched archive and monitoring PNGs when new archive is written to disk
    """
    def __init__(self, single_pol, timezone, zaplist, cpubind):
        super().__init__()
        self.first_file = True
        self.freq_zap_list = ""
        self.time_zap_list = ""
        self.single_pol = single_pol
        self.desired_timezone = pytz.timezone(timezone)
        self.cpubind = cpubind
        self.update_freq_zaplist(zaplist)



    def _syscall(self, cmd):
        _log.debug(f"Calling: {cmd}")
        with Popen(shlex.split(cmd), stdout=PIPE, stderr=PIPE) as proc:
            proc.wait()
        if proc.returncode != 0:
            _log.error(proc.stderr.read())
        else:
            _log.debug("Call success")

    def fscrunch(self, fname):
        # frequency scrunch done here all fscrunch archive
        self._syscall(
            f"taskset -c {self.cpubind} paz {self.freq_zap_list} -e zapped {fname}"
        )
        self._syscall(
            f'taskset -c {self.cpubind} pam -F -e fscrunch {fname.replace(".ar", ".zapped")}'
        )
        return fname.replace(".ar", ".fscrunch")

    def first_tscrunch(self, fname):
        self._syscall(
            f"taskset -c {self.cpubind} paz -w 0 {self.freq_zap_list} -e first {fname}"
        )

    def update_freq_zaplist(self, zaplist):
        for item in range(len(zaplist.split(","))):
            self.freq_zap_list = (
                f"""{str(self.freq_zap_list)} -F '{zaplist.split(",")[item]}' """
            )

        self.freq_zap_list = self.freq_zap_list.replace(":", " ")
        _log.debug(f"Latest frequency zaplist {self.freq_zap_list}")

    def update_time_zaplist(self, zaplist):
        self.time_zap_list = ""
        for item in range(len(zaplist.split(":"))):
            self.time_zap_list = f'{str(self.time_zap_list)} {zaplist.split(":")[item]}'

        _log.debug(f"Latest time zaplist {self.time_zap_list}")

    def process(self, fname):
        fscrunch_fname = self.fscrunch(fname)
        if self.first_file:
            self._prepare_first_scrunch(fscrunch_fname, fname)
        else:
            self._scrunch(fname, fscrunch_fname)
        os.remove(fscrunch_fname)
        os.remove(fscrunch_fname.replace(".fscrunch", ".zapped"))
        _log.debug("Accessing archive PNG files")

    def _scrunch(self, fname, fscrunch_fname):
        self._syscall(
            f"taskset -c {self.cpubind} psradd -T -inplace sum.tscrunch {fname}"
        )
            # update fscrunch here with the latest list, cannot go backward
            # (i.e. cannot redo zap)
        self._syscall(
            f"taskset -c {self.cpubind} paz {self.freq_zap_list} -m sum.tscrunch"
        )
        self._syscall(
            f"taskset -c {self.cpubind} psradd -inplace sum.fscrunch {fscrunch_fname}"
        )
        current_date_time = datetime.datetime.now(self.desired_timezone)
        formatted_time = current_date_time.strftime("%Y-%m-%d %H:%M:%S %Z")
        if self.single_pol == 1:
            self._syscall(
                f"taskset -c {self.cpubind} psrplot -p freq+ -jDp -c above:r=\"{formatted_time}\" -D ./png/tscrunch.png/png sum.tscrunch"
            )
        else:
            self._syscall(
                f"taskset -c {self.cpubind} psrplot -N 1x2 -p freq+ -jD -l pol=0,1 -r 1.5 -w 15 -c above:r=\"{formatted_time}\" -D ./png/tscrunch.png/png sum.tscrunch"
            )
        self._syscall(
            f"taskset -c {self.cpubind} pav -DFTp sum.fscrunch  -g ./png/profile.png/png"
        )
        self._syscall(
            f"taskset -c {self.cpubind} pav -FYp sum.fscrunch  -g ./png/fscrunch.png/png"
        )
        _log.debug(f"removing {fscrunch_fname}")

    def _prepare_first_scrunch(self, fscrunch_fname, fname):
        _log.debug("First file in set. Copying to sum.?scrunch.")
        shutil.copy2(fscrunch_fname, "sum.fscrunch")
        self._syscall(
            f"taskset -c {self.cpubind} paz -w 0 -m sum.fscrunch"
        )
        self.first_tscrunch(fname)
        shutil.copy2(fname.replace(".ar", ".first"), "sum.tscrunch")
        os.remove(fname.replace(".ar", ".first"))
        self.first_file = False

    def on_created(self, event):
        _log.debug(f"New file created: {event.src_path}")
        try:
            fname = event.src_path
            _log.debug(fname.find('.ar.') != -1)
            if fname.find('.ar.') != -1:
                _log.debug(f"Passing archive file {fname[:-9]} for processing")
                time.sleep(1)
                self.process(fname[:-9])
        except Exception as E:
            _log.error(E)



class EddPulsarPipelineError(Exception):
    pass


class PulsarBase():
    # common pulsar pipeline function
    def __init__(self, config=None):
        self.mkrec_cmd = []
        self._recording = False
        self._config = {} if config is None else config
        self._data_root_directory = self._config.get("data_root_directory", "/beegfsEDD/JWU/")
        self.data_directory = None
        self.edd_data_store = None

        self._dada_buffers = []
        self._dada_buffers_spec = {'dada': {}, 'dadc':{}}
        self._data_processing_proc = None
        self._mkrecv_ingest_proc = []
        self._archive_directory_monitor = None
        self._png_monitor_callback = None
        self._folder_size_monitor_callback = None
        self._ready_pipeline_task = None
        self._folder_size_task = None
        self._merge_proc = None

        self.tzpar_file = None

        self._header = None
        self._tstr = None
        self.dada_header_file = None
        self.dada_key_file = None
        self._subprocessMonitor = None
        self._coreManager = None
        self.tzpar_dir = None
        self.cuda_number = None
        self.old_files = set()


        # Pick first available numa node. Disable non-available nodes via
        # EDD_ALLOWED_NUMA_NODES environment variable
        # Convert to list as generator in python3
        self.numa_number = list(numa.getInfo().keys())[0]
        self.sensors = aiokatcp.sensor.SensorSet()
        self.setup_sensors()

    def setup_sensors(self):
        # setup pulsar pipeline sensors
        self._tscrunch_png_sensor = Sensor(str,
            "tscrunch_PNG",
            description="tscrunch png",
            default=BLANK_IMAGE,
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._tscrunch_png_sensor)

        self._fscrunch_png_sensor = Sensor(str,
            "fscrunch_PNG",
            description="fscrunch png",
            default=BLANK_IMAGE,
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fscrunch_png_sensor)

        self._profile_png_sensor = Sensor(str,
            "profile_PNG",
            description="pulse profile png",
            default=BLANK_IMAGE,
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._profile_png_sensor)

        self._central_freq_sensor = Sensor(str,
            "_central_freq",
            description="_central_freq",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._central_freq_sensor)

        self._source_name_sensor = Sensor(str,
            "target_name",
            description="target name",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._source_name_sensor)

        self._nchannels = Sensor(str,
            "_nchannels",
            description="_nchannels",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._nchannels)

        self._nbins = Sensor(str,
            "_nbins",
            description="_nbins",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._nbins)

        self._time_processed = Sensor(str,
            "_time_processed",
            description="_time_processed",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._time_processed)

        self._dm_sensor = Sensor(str,
            "_source_dm",
            description="_source_dm",
            default=0,
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._dm_sensor)

        self._par_dict_sensor = Sensor(str,
            "_par_dict_sensor",
            description="_par_dict_sensor",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._par_dict_sensor)

        self._directory_size_sensor = Sensor(str,
            "_directory_size_sensor",
            description="_directory_size_sensor",
            default="N/A",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._directory_size_sensor)

        self._input_buffer_fill_level = Sensor(float,
                "input-buffer-fill-level",
                description="Fill level of the input buffer")
        self.sensors.add(self._input_buffer_fill_level)

        self._input_buffer_total_write = Sensor(float,
                "input-buffer-total-write",
                description="Total write into input buffer ")
        self.sensors.add(self._input_buffer_total_write)

        self._output_buffer_fill_level = Sensor(float,
                "output-buffer-fill-level",
                description="Fill level of the output buffer")
        self.sensors.add(self._output_buffer_fill_level)

        self._output_buffer_total_read = Sensor(float,
                "output-buffer-total-read",
                description="Total read from output buffer")
        self.sensors.add(self._output_buffer_total_read)

        self._polarization_sensors = {}

    def _subprocess_error(self, proc):
        """
        Sets the error state because process has ended.
        """
        _log.error("Error handle called because subprocess {} ended with return code {}".format(proc.pid, proc.returncode))
        self._subprocessMonitor.stop()
        raise StateChange("error")

    def _is_accessible(self, path, mode='r'):
        """
        Check if the file or directory at `path` can
        be accessed by the program using `mode` open flags.
        """
        try:
            with open(path, mode, encoding='UTF8'):
                pass
        except IOError:
            return False
        return True

    def check_cal_active(self):
        """
        Check if the cal is on or off
        """
        ndp = json.loads(self.edd_data_store.getTelescopeDataItem("noise_diode_pattern", "value"))
        return float(ndp['percentage']) > 0

    def _png_monitor(self):
        try:
            processed_count = int(os.popen(f"ls {self.data_directory}/*ar | wc -l").read())
            self._time_processed.set_value(f"{processed_count * int(self._config['intergration_time'])} s")
            _log.info(f"processed {processed_count * int(self._config['intergration_time'])}s")
        except Exception as E:
            _log.debug(E)
        _log.debug(f"reading png from: {self.data_directory}/png")
        try:
            pngs = ["fscrunch", "tscrunch", "profile"]
            for png in pngs:
                path = os.path.join(self.data_directory, 'png', f"{png}.png")
                with open(path, "rb") as image_file:
                    sensor = getattr(self, f"_{png}_png_sensor")
                    sensor.set_value(base64.b64encode(image_file.read()))
                    _log.debug(f"reading {path}")
        except Exception as E:
            _log.debug(E)
        try:
            total_size = 0
            for root, _, files in os.walk(f"{self.data_directory}"):
                for f in files:
                    total_size += os.path.getsize(os.path.join(root, f))
            total_size_in_mb = total_size / 1024 / 1024.
            self._directory_size_sensor.set_value(f"{int(total_size_in_mb)} MB")
            _log.debug(f"Directory size = {total_size_in_mb} MB")
        except Exception as E:
            _log.debug(E)


    async def _folder_size_monitor(self):
        try:
            total_size = 0
            for root, _, files in os.walk(f"{self.data_directory}"):
                for f in files:
                    total_size += os.path.getsize(os.path.join(root, f))
            total_size_in_gb = total_size / 1024 / 1024 / 1024.
            self._directory_size_sensor.set_value(f"{int(total_size_in_gb)} GB")
            _log.debug(f"Directory size = {total_size_in_gb} GB")
        except Exception as E:
            _log.debug(E)

    def add_input_stream_sensor(self, streamid):
        """
        Add sensors for i/o buffers for an input stream with given streamid.
        """
        self._polarization_sensors[streamid] = {
            "mkrecv_sensors": MkrecvSensors(streamid)
        }
        for s in self._polarization_sensors[streamid]["mkrecv_sensors"].sensors.values():
            self.sensors.add(s)

    async def _create_ring_buffer(self, numa_node):
        """
        Create a ring buffer of given size with given key on specified numa node.
               Adds and register an appropriate sensor to the list
        """
        # always clear buffer first. Allow fail here

        # Create dada buffer for input data
        for buffer in self._dada_buffers:
            _log.debug(f"\tbuffer '{buffer.key}'")
            await buffer.destroy()
        self._dada_buffers = []

        self._dada_buffers.append(
            dada.DadaBuffer(self._dada_buffers_spec["dada"]["size"], n_slots=self._dada_buffers_spec["dada"]["number"], node_id=numa_node, pin=True, lock=True)
        )
        await self._dada_buffers[-1].create()
        # Create dada buffer for output data
        self._dada_buffers.append(
            dada.DadaBuffer(self._dada_buffers_spec["dadc"]["size"], n_slots=self._dada_buffers_spec["dadc"]["number"], node_id=numa_node, pin=True, lock=True)
        )
        await self._dada_buffers[-1].create()

        for buffer in self._dada_buffers:
            buffer.get_monitor(self._buffer_status_handle).start()


    def _buffer_status_handle(self, status):
        """
        Process a change in the buffer status
        """
        timestamp = time.time()
        if status['key'] == "dada":
            conditional_update(self._input_buffer_total_write, status['written'], timestamp=timestamp)
            self._input_buffer_fill_level.set_value(status['fraction-full'], timestamp=timestamp)

        elif status['key'] == "dadc":
            self._output_buffer_fill_level.set_value(status['fraction-full'], timestamp=timestamp)
            conditional_update(self._output_buffer_total_read, status['read'], timestamp=timestamp)

    def _check_tzpar_repo(self):
        _repo_base = self._config["tzpar_repo"].split("/")[-1].split(".")[0]
        if self._config["git_flag"] == 1:
            if os.path.isdir(f"/mnt/{_repo_base}") is False:
                try:
                    _log.info(f'Git cloning from :{self._config["tzpar_repo"]}')
                    repo_url = self._config["tzpar_repo"]
                    token_file = "/tmp/git_token"
                    if os.path.exists(token_file):
                        with open(token_file, encoding='UTF8') as f:
                            token = f.read().strip()
                        repo_url = repo_url.replace("https://", f"https://oauth2:{token}@")
                    git.Repo.clone_from(repo_url, f"/mnt/{_repo_base}")
                    _log.info("Git clone done")
                except Exception as E:
                    _log.warning(f"Git clone error: {E}")
            else:
                try:
                    _log.info(f'Git pulling from :{self._config["tzpar_repo"]}')
                    repo = git.Repo(f"/mnt/{_repo_base}")
                    origin = repo.remote(name='origin')
                    origin.pull()
                    _log.info("Git pull done")
                except Exception as E:
                    _log.warning(f"Git pull error: {E}")
            try:
                repo = git.Repo(f"/mnt/{_repo_base}")
                result = repo.git.checkout(f'{self._config["git_branch"]}')
                _log.info(result)
            except Exception as E:
                _log.warning(f"Git checkout error: {E}")
        else:
            _log.info("Git is not enabled, using local version for now")

        tzpar_dir = os.path.join("/mnt/", _repo_base, self._config["tzpar_path"])
        if not os.path.isdir(tzpar_dir):
            _log.error(f"Directory not found {tzpar_dir} !")
            raise RuntimeError(f"tzpar directory is not found: {tzpar_dir}")
        return tzpar_dir

    def _retrive_telescope_metedata(self):

        data = {}
        
        data['source_name'] = self.edd_data_store.getTelescopeDataItem("source-name")

        ra = self.edd_data_store.getTelescopeDataItem("ra")
        dec = self.edd_data_store.getTelescopeDataItem("dec")
        if ra is None or dec is None:
            ra = 0
            dec = 0
        c = SkyCoord(ra=ra, dec=dec, unit=(u.deg, u.deg))
        data["ra"] = c.to_string("hmsdms").split(" ")[0].replace(
            "h", ":").replace("m", ":").replace("s", "")
        data["dec"] = c.to_string("hmsdms").split(" ")[1].replace(
            "d", ":").replace("m", ":").replace("s", "")

        data['obs_id'] = self.edd_data_store.getTelescopeDataItem("observation-id")

        data['receiver_name'] = self.edd_data_store.getTelescopeDataItem("receiver")
        _log.info(
            f"Retrieved data from telescope:\n   Source name: {data['source_name']}\n   RA = {data['ra']},  decl = {data['dec']}, receiver = {data['receiver_name']}"
        )
        return data


    async def build_header(self, data):
        """
        Create actual dada header from header params and TMI data
        """
        header = copy.deepcopy(self._config["dada_header_params"])
        header["telescope"] = self._config["tempo2_telescope_name"]

        header["ra"] = data['ra']
        header["dec"] = data['dec']

        header["key"] = self._dada_buffers[0].key
        header["mc_source"] = ""
        stream_description = copy.deepcopy(self._config['input_data_streams'][0])
        if len(self._config['input_data_streams']) > 1:
            for stream in self._config['input_data_streams'][1:]:
                stream_description["ip"] += ",{}".format(stream["ip"])

        band_numbers = [int(stream['source'].split('group')[-1]) for stream in self._config['input_data_streams']]
        band_central_freq = [stream['central_freq'] for stream in self._config['input_data_streams']] 

        header.update({
            "mc_source": stream_description["ip"],
            "mc_streaming_port": stream_description["port"],
            "interface": numa.getFastestNic(self.numa_number)[1]['ip'],
            "frequency_mhz": (max(band_central_freq) + min(band_central_freq)) / 2,
            "sync_time": stream_description["sync_time"],
            "sample_clock" : float(stream_description["sample_rate"]),
            "idx1_modulo" : float(stream_description["sample_rate"]),
            "idx3_list": f"{min(band_numbers) * 256}:{max(band_numbers) * 256 + 256}:2",
            "obs_id" : data['obs_id'],
            "receiver_name" : data['receiver_name']
        })
        _log.info(f"  - Tempo2 telescope name: {header['telescope']}")
        _log.debug(f"  - Dada key: {header['key']}")
        _log.info(f"  - mc source: {header['mc_source']}")
        _log.info(f"  - mc streaming port: {header['mc_streaming_port']}")
        _log.info(f"  - mc interface: {header['interface']}")
        _log.info(f"  - sync time: {header['sync_time']}")
        _log.info(f"  - sample_clock: {header['sample_clock']}")
        _log.info(f"  - obs_id: {header['obs_id']}")
        _log.info(f"  - receiver_name: {header['receiver_name']}")
        return header

    async def write_header(self, header):
        self.dada_header_file = tempfile.NamedTemporaryFile(  # pylint: disable=consider-using-with
            mode="w",
            prefix="edd_dada_header_",
            suffix=".txt",
            delete=False)
        _log.debug(
            "Writing dada header file to {0}".format(
                self.dada_header_file.name))
        self.dada_header_file.write(render_dada_header(header))
        self.dada_key_file = tempfile.NamedTemporaryFile(  # pylint: disable=consider-using-with
            mode="w",
            prefix="dada_keyfile_",
            suffix=".key",
            delete=False)
        _log.debug("Writing dada key file to {0}".format(
            self.dada_key_file.name))
        if self._config["single_pol"] == 0:
            key_string = make_dada_key_string(self._dada_buffers[1].key)
            self.dada_key_file.write(make_dada_key_string(self._dada_buffers[1].key))
        else:
            key_string = make_dada_key_string(self._dada_buffers[0].key)
            self.dada_key_file.write(make_dada_key_string(self._dada_buffers[0].key))
        _log.debug("Dada key file contains:\n{0}".format(key_string))
        self.dada_header_file.close()
        self.dada_key_file.close()

        attempts = 0
        retries = 5
        while True:
            if attempts >= retries:
                E = "could not read dada_key_file"
                _log.warning(f"{E}. Will not react until next measurement start")
                raise StateChange("ready")

            await asyncio.sleep(1)
            if self._is_accessible(f'{self.dada_key_file.name}'):
                _log.debug(f'found {self.dada_key_file.name}')
                break
            attempts += 1

    def _managed_process_wrapper(self, cmd, arg1, stdout_handler):
        _log.info("Running command: %s", cmd)
        _log.info(arg1)
        result = ManagedProcess(cmd, stdout_handler=stdout_handler)
        self._subprocessMonitor.add(result, self._subprocess_error)

        return result

    def _stop_subprocess_monitor(self):
        self._subprocessMonitor = SubprocessMonitor()
        for callback_name in ["_png_monitor_callback", "_folder_size_monitor_callback"]:
            try:
                if callback := getattr(self, callback_name):
                    callback.cancel()
            except Exception as e:
                _log.debug(f"{callback_name} already stopped: {e}")

    def extract_dm_information(self): #used in timing and search class, not baseband and leap
        par_dict = {}
        try:
            with open(os.path.join(self.tzpar_dir, f'{self._source_name_sensor.value.split("_")[0][1:]}.par'), encoding='UTF8') as fh:
                for line in fh:
                    tokens = line.strip().split()
                    if len(tokens) == 2:
                        key, value = tokens
                    elif len(tokens) == 3:
                        key, value, error = tokens
                    elif len(tokens) == 4:
                        key, value, lock, error = tokens
                    par_dict[key] = value.strip()
        except IOError as E:
            _log.error(E)
        try:
            dm = float(par_dict["DM"].replace('D', 'E'))
        except (ValueError, KeyError) as E:
            _log.error(
                "Key not found or ValueError, error message : {E}, will use default value of %f", self._config["cdd_dm"]
            )
            dm = self._config["cdd_dm"]
        if self.check_cal_active():
            _log.info("This is a calibrator scan, will set dm to zero")
            dm = 0
        self._dm_sensor.set_value(dm)
        self._par_dict_sensor.set_value(json.dumps(par_dict).strip("{").strip("}").replace(",", "\n"))
        return dm

    def _blank_image(self):
        self._fscrunch_png_sensor.set_value(BLANK_IMAGE)
        self._tscrunch_png_sensor.set_value(BLANK_IMAGE)
        self._profile_png_sensor.set_value(BLANK_IMAGE)

    def _check_pipeline_active_state(self):
        if self._config["active"] == 0:
            _log.info("Pipeline is not active")
            raise StateChange("ready")

    async def _ready_pipeline(self):
        """
        Task to prepare the pipeline for the next measurement in the background
        """
        _log.debug("Ready the pipeline for next measurement")
        _log.debug('Waiting for dada buffers to deplete')

        starttime = time.time()

        while age:=time.time() - starttime < 30:
            _log.debug('Waiting for buffer depletion since %.1f sec', age)
            _log.debug('sensor read writer: %i, %i', self._input_buffer_total_write.value, self._output_buffer_total_read.value)
            if self._input_buffer_total_write.value == self._output_buffer_total_read.value:
                _log.debug('All written buffers read - waiting for 2 seconds to be sure that everything was processed')
                await asyncio.sleep(2)
                break
            await asyncio.sleep(.5)

        #for buffer in self._dada_buffers:
        #    buffer.get_monitor(self._buffer_status_handle).stop()

        await self._create_ring_buffer(self.numa_number)

        self._subprocessMonitor = None

        if self._data_processing_proc:
            _log.debug("Waiting for processing (pid: %i ) to terminate (for at most 3 seconds)", self._data_processing_proc.pid)
            # asyncio.create_subprocess_exec. in the future
            counter = 0
            while self._data_processing_proc.is_alive():
                counter +=1
                await asyncio.sleep(.5)
                if counter > 3:
                    _log.warning('data processing not finished after 3 sec. Keeping it running (pid: %i)', self._data_processing_proc.pid)
                    #self._data_processing_proc.terminate(1)
                    break
            self._data_processing_proc = None

    async def _wait_for_pipeline_ready(self):
        _log.debug('Waiting for pipeline preparation')
        await self._ready_pipeline_task
        _log.debug('preparation task done')

    def get_buffer_sizes(self):
        self._dada_buffers_spec["dada"] = {"size": self._config["dada"]["size"], "number": self._config["dada"]["number"]}
        self._dada_buffers_spec["dadc"] = {"size": self._config["dadc"]["size"], "number": self._config["dadc"]["number"]}



    async def configure(self):
        _log.info("Configuring EDD backend for processing")
        _log.info("Final configuration:\n%s", json.dumps(self._config, indent=4))

        self.edd_data_store = EDDDataStore(host=self._config["data_store"]["ip"], port=self._config["data_store"]["port"])
        self._coreManager = CoreManager(self.numa_number)
        self._coreManager.add_task("mkrecv",  8 + 1, prefere_isolated=True)
        self._coreManager.add_task("dspsr", 1)
        self._coreManager.add_task("archive_monitor",1)
        self._dada_buffers_spec["dada"] = self._config["dada"]
        self._dada_buffers_spec["dadc"] = self._config["dadc"]

        await self._create_ring_buffer(self.numa_number)
        self._ready_pipeline_task = None
        self.tzpar_dir = self._check_tzpar_repo()

        if os.path.isfile("/tmp/t2pred.dat"): #clean up tempo2 temp file
            os.remove("/tmp/t2pred.dat")

        self.add_input_stream_sensor("")

    def mode_header_update(self, header, data):
        header['source_name'] = data['source_name']
        return header

    async def set_data_directory(self, header=None):
        """
        Set and create data directory. If data directory in config is 'auto', a directory from current date and file size will be created. Alternatively, as string with the data path can be given.

        If a string is given, parameters such as {foo} will be looked up in the
        data store respectively the provided header and inserted. values in the
        header take precedence over values in the data store.
        """
        if header is None:
            header = {}

        if self._config.get("data_directory", "auto") == "auto":
            _log.debug("Automatically creating data directory based on current date")
            self.data_directory = os.path.join(output.create_datebased_subdirs(self._data_root_directory), self._config.get('eddid', output.get_random_string(5)))
        else:
            _log.debug("Creating data directory from given template: %s", self._config["data_directory"])
            keys = re.findall(r'\{(.*?)\}', self._config["data_directory"])
            _log.debug("Found %i keys, getting from data store", len(keys))
            data = {}
            data['date'] = datetime.datetime.now().strftime('%Y-%m-%d')
            if data['date'] is None:
                _log.warning('Date not known from data store!')
                data['date'] = '1969-07-16'
            for k in keys:
                v = self.edd_data_store.getTelescopeDataItem(k)
                v = header.get(k, v)
                if v is None:
                    v = output.get_random_string(8)
                data[k] = v

            data['year'] = data['date'][:4]
            data['month'] = data['date'][5:7]
            data['day'] = data['date'][8:]

            data_directory = self._config["data_directory"].format(**data)
            _log.debug("Final data directory: %s", data_directory)

            self.data_directory = os.path.join(self._data_root_directory, data_directory)
        os.makedirs(self.data_directory, exist_ok=True)
        os.makedirs(os.path.join(self.data_directory, 'png'), exist_ok=True)

        # Scan for old files
        self.old_files = set()
        for (dirpath, dirnames, filenames) in os.walk(self.data_directory):
            for f in filenames:
                self.old_files.add(os.path.join(self.data_directory, dirpath, f))


    async def measurement_prepare(self, config):
        _log.info("Measurement prepare string:\n%s", json.dumps(config, indent=4))

        if self._ready_pipeline_task is not None:
            await self._wait_for_pipeline_ready()

        self._config.update(config)


    async def measurement_start(self):
        # common preparation steps for all pipelines
        self._check_pipeline_active_state()
        self._stop_subprocess_monitor()
        self._blank_image()
        tmi_data = self._retrive_telescope_metedata()
        header = await self.build_header(tmi_data)
        header = self.mode_header_update(header, tmi_data)
        await self.write_header(header)

        # This should be removed
        self._header = header

        #
        await self.set_data_directory(header)

        # update sensors
        self._nbins.set_value(self._config["nbins"])
        self._source_name_sensor.set_value(tmi_data['source_name'])
        self._central_freq_sensor.set_value(str(header["frequency_mhz"]))
        self._nchannels.set_value(str(self._config["nchannels"])) # update the rounded channel number
        self._tstr = Time.now().isot

        self.cuda_number = numa.getInfo()[self.numa_number]['gpus'][0]

        _log.info("  - Running on cuda core: {}".format(self.cuda_number))

    async def measurement_stop(self):
        if self._subprocessMonitor is not None:
            self._subprocessMonitor.stop()

    async def capture_start(self):
        pass

    async def deconfigure(self):
        if self._ready_pipeline_task is not None:
            await self._wait_for_pipeline_ready()

        for buffer in self._dada_buffers:
            _log.debug(f"\tdestroying buffer '{buffer.key}'")
            await buffer.destroy()

class EddPulsarTimingPipeline(PulsarBase):
    def __init__(self, config=None):
        super().__init__(config)
        self.pulsar_flag = None
        self._archive_observer = None
        self._handler = None
        self._png_monitor_task = None

    async def _calculate_predictor(self):
        os.chdir("/tmp/")
        _log.debug("Creating the predictor with tempo2")
        if (not self.check_cal_active()) and self._is_accessible(self.tzpar_file):
            cmd = f"numactl -m {self.numa_number} taskset -c {self._coreManager.get_coresstr('dspsr')} tempo2 -f {self.tzpar_file} -pred".split()
            cmd.append(
                f'{self._config["tempo2_telescope_name"]} {Time.now().mjd - self._config["predictor_mjd_half_range"]} {Time.now().mjd + self._config["predictor_mjd_half_range"]} {float(self._header["frequency_mhz"]) - float(self._header["bandwidth"]) / 2} {float(self._header["frequency_mhz"]) + float(self._header["bandwidth"]) / 2} {self._config["tempo2_ntimecoeff"]} {self._config["tempo2_nfreqcoeff"]} {self._config["predictor_block_length"]}'
            )
            _log.info(f"Command to run: {cmd}")
            await command_watcher(cmd, allow_fail=True)
            attempts = 0
            retries = 5
            while True:
                if attempts >= retries:
                    E = "Could not read t2pred.dat"
                    _log.warning(f"{E}. Will not react until next measurement start")
                    raise StateChange("ready")

                await asyncio.sleep(1)
                if self._is_accessible(f'{os.getcwd()}/t2pred.dat'):
                    _log.debug(f'found {os.getcwd()}/t2pred.dat')
                    break
                attempts += 1

    def _par_file_check(self, source_name):
        tzpar_file = os.path.join(
            self.tzpar_dir, f'{source_name.split("_")[0][1:]}.par'
        )
        _log.debug(f"Checking parfile file {tzpar_file}")
        pulsar_flag = self._is_accessible(tzpar_file)
        if (not self.check_cal_active()) and (not pulsar_flag):
            _log.warning(
                f"source {source_name} is neither pulsar nor calibrator. Will not react until next measurement start"
            )
            self._recording = False
            raise StateChange("measuring")
        return tzpar_file, pulsar_flag

    async def measurement_start(self):
        await super().measurement_start()
        self.tzpar_file, self.pulsar_flag = self._par_file_check(self._header['source_name'])
        await self._calculate_predictor()
        os.chdir(self.data_directory)
        dm = self.extract_dm_information()

        _log.debug("source_name = %s", self._header['source_name'])


        #cmd = "dbnull -k dadc"
        
        if (not self.check_cal_active()) and self.pulsar_flag:
            cmd = "numactl -m {numa} dspsr {args} {intergration_time} {nchan} {nbin} -fft-bench -x {fft_length} -cpu {cpus} -cuda {cuda_number} -P {predictor} -N {name} -U 1105 -O {filename} -E {parfile} {keyfile}".format(
                numa=self.numa_number,
                fft_length=self._config["fft_length"],
                args=self._config["dspsr_params"]["args"],
                intergration_time=f'-L {self._config["intergration_time"]}',
                nchan=f'-F {self._config["nchannels"]}:D',
                nbin=f'-b {self._config["nbins"]}',
                name=self._source_name_sensor.value.split("_")[0],
                filename=f'{self._tstr.split("T")[1].split(".")[0]}',
                predictor="/tmp/t2pred.dat",
                parfile=self.tzpar_file,
                cpus=12,
               # cpus=self._coreManager.get_coresstr('dspsr'),
                cuda_number=self.cuda_number,
                keyfile=self.dada_key_file.name,
            )
        elif self.check_cal_active():
            cmd = "numactl -m {numa} dspsr {args} {intergration_time} -c {period} -D 0.0001 -fft-bench  {nchan} -x {fft_length} -cpu {cpus} -N {name} -U 9349 -O {filename} -cuda {cuda_number} {keyfile}".format(
                numa=self.numa_number,
                args=self._config["dspsr_params"]["args"],
                intergration_time=f'-L {self._config["intergration_time"]}',
                period=self._config["cal_period"],
                fft_length=self._config["fft_length"],
                nchan=f'-F {self._config["nchannels"]}:D',
                name=self._source_name_sensor.value,
                filename=f'{self._tstr.split("T")[1].split(".")[0]}',
                #cpus=self._coreManager.get_coresstr('dspsr'),
                cpus=12,
                cuda_number=self.cuda_number,
                keyfile=self.dada_key_file.name,
            )
        else:
            E = "source is unknown"
            raise EddPulsarPipelineError(E)
        
        self._data_processing_proc = self._managed_process_wrapper(
            cmd, "Staring dspsr", None
        )
        if self._config["single_pol"] == 0:
            cmd = "numactl -m 0 taskset -c 14-22 rfsoc2tafp --input-key dada --output-key dadc -n 8 --log_level=info"
            self._merge_proc = self._managed_process_wrapper(
                cmd, "Staring EDDPolnMerge", None
            )

        if not self._config['dummy_input']:
            cmd = "numactl -m {numa} taskset -c 0-5 mkrecv_v4 --header {dada_header} --nthreads 4 --with-ps --lst --quiet".format(
                numa=self.numa_number, cpu=self._coreManager.get_coresstr('mkrecv'), threads=self._config["mkrecv_nthreads"], dada_header=self.dada_header_file.name)
            mk = ManagedProcess(cmd, stdout_handler=self._polarization_sensors[""]["mkrecv_sensors"].stdout_handler)
            _log.info("Running mkrecv_v4 command; {}".format(cmd))
        else:
            _log.warning("Creating Dummy input instead of listening to network!")
            mk = dada.JunkDb(self._dada_buffers[-2].key, header=self.dada_header_file.name, data_rate=1e9, duration=3600).start()
        self._mkrecv_ingest_proc.append(mk)
        self._subprocessMonitor.add(mk, self._subprocess_error)

        _log.info("Staring archive monitor")
        self._archive_observer = Observer()
        self._archive_observer.daemon = False
        _log.info(f"Input directory: {self.data_directory}")
        _log.info("Setting up ArchiveAdder handler")
        self._handler = ArchiveAdder(self._config["single_pol"], self._config["timezone"], self._config["zaplist"],  self._coreManager.get_coresstr('archive_monitor'))
        self._archive_observer.schedule(self._handler, str(self.data_directory), recursive=False)
        _log.info("Starting directory monitor")
        self._archive_observer.start()
        async def _png_monitor_callback():
            while True:
                self._png_monitor()
                await asyncio.sleep(5)
        self._png_monitor_task = asyncio.create_task(_png_monitor_callback())
        self._subprocessMonitor.start()
        cfs = json.dumps(self._config, indent=4)
        _log.info("Final configuration:\n%s", cfs)
        self._recording = True

    async def measurement_stop(self):
        if not self._recording:
            raise StateChange("ready")
        await super().measurement_stop()
        try:
            self._archive_observer.stop()
        except Exception as E:
            _log.error(f"Error stopping _archive_observer: {E}")
        try:
            if self._png_monitor_task:
                self._png_monitor_task.cancel()
        except Exception as E:
            _log.error(f"Error stopping _png_monitor_callback: {E}")

        if self._config["single_pol"] == 0:
            try:
                for proc in self._mkrecv_ingest_proc:
                    proc.terminate(timeout=1)
                self._merge_proc.terminate(timeout=1)
            except Exception as E:
                _log.error(f"Error in stopping : {E}")
        else:
            try:
                for proc in self._mkrecv_ingest_proc:
                    proc.terminate(timeout=1)
            except Exception as E:
                _log.error(f"Error in stopping for mkrecv: {E}")
        if os.path.isfile("/tmp/t2pred.dat"):
            os.remove("/tmp/t2pred.dat")
        _log.info("reset DADA buffer")
        self._ready_pipeline_task = asyncio.create_task(self._ready_pipeline())
        if os.path.isfile("./core"):
            os.remove("./core")

class EddPulsarPipeline(EDDPipeline):
    """
    Interface object which accepts KATCP commands
    """
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    pipeline_classes = {
        "Timing": EddPulsarTimingPipeline
    }

    def __init__(self, ip, port, loop=None):
        EDDPipeline.__init__(self, ip, port, _DEFAULT_CONFIG, loop)
        self.active = False
        self._implementation = None
        self._edd_data_store = None

    def register_subpipeline_sensors(self, sensors):
        for sensorname, sensor in sensors.items():
            _log.info(f"Adding sensor : {sensorname}")
            self.sensors.add(sensor)
        self.mass_inform('interface-changed')

    def deregister_subpipeline_sensors(self, sensors):
        for sensor in sensors.values():
            try:
                _log.info(f"Removing sensor: {sensor.name}")
                self.sensors.remove(sensor)
            except AttributeError as e:
                _log.error(f"Error removing sensor: {e}")
        self.mass_inform('interface-changed')


    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the pipeline based on the mode specified in the configuration.
        """

        try:
            implementation_class = self.pipeline_classes[self._config['mode']]
        except KeyError as E:
            raise ValueError(f"Invalid mode specified: {self._config['mode']}") from E
        self._implementation = implementation_class(self._config)
        await time_it(self._implementation.configure())
        self.register_subpipeline_sensors(self._implementation.sensors)

        self._edd_data_store = EDDDataStore(self._config["data_store"]["ip"], self._config["data_store"]["port"])


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        await time_it(self._implementation.capture_start())


    @state_change(target="set", allowed=['ready'], intermediate="measurement_preparing")
    async def measurement_prepare(self, config=None):
        """
        Update pipeline configuration
        """
        if config is None:
            config = {}

        # Update the active state based on the config / prepare config
        self.active = config.get("active", self._config["active"])

        if self.active:
            await time_it(self._implementation.measurement_prepare(config))


        else:
            _log.info("Pipeline is not active")

    @state_change(target="measuring", allowed=["set"], intermediate="measurement_starting")
    async def measurement_start(self):
        """
        Create output directory, create mkrecv header, calculate tempo2 predictor file if needed, start data capture, processing, and monitoring
        """
        if self.active:
            await time_it(self._implementation.measurement_start())
        else:
            _log.info("Pipeline is not active")

    @state_change(target="ready", allowed=["measuring", "set"], ignored=['ready'], intermediate="measurement_stopping")
    async def measurement_stop(self):
        """stop mkrecv, merging application, processing instances, reset DADA buffers."""
        if self.active:
            await time_it(self._implementation.measurement_stop())
            for (dirpath, dirnames, filenames) in os.walk(self._implementation.data_directory):
                for f in filenames:
                    if f in self._implementation.old_files:
                        continue
                    self.add_output_file(os.path.join(os.path.join(self._implementation.data_directory, dirpath, f)), {'pipeline_mode': self._config['mode'], "cal_active": self._implementation.check_cal_active()})
        else:
            _log.info("Pipeline is not active")

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """Deconfigure the pipeline."""
        if not self._implementation:
            # Never configured
            return

        try:
            self.deregister_subpipeline_sensors(self._implementation.sensors)
            await time_it(self._implementation.deconfigure())
        except AttributeError as e:
            _log.error(f"Error deconfiguring implementation: {e}")

if __name__ == "__main__":
    asyncio.run(launchPipelineServer(EddPulsarPipeline))


