"""
The Gate Full-Stokes Spectrometer pipeline receives data in MPIFR EDD packetizer Format from both
polarizations and calculates the resulting Stokes spectra IQUV.


Configuration Settings
----------------------
    input_data_streams
        The spectrometer requires two input data streams ``polarization_0,
        polarization_1`` in MPIFR_EDD_Packetizer format.

    fft_length (int)
        Number of samples to be included in a FFT. As the dc channel is
        dropped, the output contains fft_length/2 channels.

    naccumulate (int)
        Number of output spectra to be accumulated into one output spectrum.

Note:
    The integration time is given by fft_length * naccumulate / sampling_rate
    of the packetizer.


Output Data Streams
-------------------
    Eight data streams in GatedSpectrometer format corresponding to Stokes IQUV
    spectra for noise diode on and off.


Warning:
    Although the output is labeled as IQUV throughout the pipeline and the data
    processing code, this is only correct for a linear feed and the secondary
    (primary?) focus.  For circular feed Q->V ,U->Q, V->U. Observing in the
    (primary?) focus results in a factor -1 due to the effective polarization
    flip from the single reflection.


Expert/Debugging Settings
-------------------------
    samples_per_block (int)
        Configure the size of the internal ring buffer. (Default 256*1024*1024)
        This limits the maximum size of the FFT to 128 M Channels by default.
        With this option the code can be tweaked to run on low-mem GPUs or if
        the GPU is  shared with other codes.

    output_rate_factor (float)
        The nominal output data rate is multiplied by this factor and the
        result used to send the data fast enough to keep up with the data
        stream while avoiding bursts. Default: 1.10

    output_type ('network', 'disk', 'null')
        Instead of sending the output to the network, it can be dropped ('null') or written to 'disk'.

    output_directory (str)
        Output directory used for output_type 'disk'. Defaults to '/mnt'

    dummy_input (bool)
        Don't connect to packetizer data streams but use randomly generated
        input data.

    disable_gate (-1,0,1):
        Disable the calculation of gate 0 or 1 to reduce strain on the GPU. Set
        to -1 (default) to calcualte both gates.  Note that also for a disabled
        gate output data is written to the dada_buffer and send to a stream.
        The data will be random numbers though.

    nonfatal_numacheck
        The code check the available numa nodes and selects only nodes for
        execution with GPU and network card. If no suitable numa node exists,
        configuration will fail unless this this option is set to True. If set
        to True, only a warning is emitted. Used e.g. in automatic testing.
"""

import os
import json
import tempfile
import copy
import time
import asyncio

from aiokatcp.sensor import Sensor
from aiokatcp.connection import FailReply
from aiokatcp.server import RequestContext

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.utils.core_manager import CoreManager
from mpikat.utils.process_tools import ManagedProcess
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog, conditional_update
from mpikat.utils.mk_tools import MkrecvSensors
from mpikat.utils.ip_utils import ipstring_to_list
import mpikat.utils.dada_tools as dada
import mpikat.utils.numa as numa

import mpikat.pipelines.digpack_controller  # pylint: disable=unused-import
from mpikat.pipelines.gated_spectrometer import GatedSpectrometerDataStreamFormat

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.pipelines.GatedFullStokesSpectrometerPipeline")

_DEFAULT_CONFIG = {
        "id": "GatedFullStokesSpectrometer",                           # default cfgs for master controller. Needs to get a unique ID -- TODO, from ansible
        "type": "GatedFullStokesSpectrometer",
        "supported_input_formats": {"MPIFR_EDD_Packetizer": [1]},      # supported input formats name:version
        "samples_per_block": 256 * 1024 * 1024,             # 256 Mega samples per buffer block to allow high res  spectra - the
                                                            # theoretical  maximum is thus  128 M Channels.
        "input_buffer_critical_level": 0.85,                # Fill level above which the input buffer is regarded as critical
        "input_buffer_critical_timeout": 30,                # If critical for more than this number of seconds, or no data arrives within thistime window, the pipeline will go to error state
        "input_data_streams":
        {
            "polarization_0":
            {
                "format": "MPIFR_EDD_Packetizer:1",         # Format has version separated via colon
                "ip": "225.0.0.156+3",
                "port": "7148",
                "bit_depth" : 8,
                "sample_rate" : 3200000000,
            },
             "polarization_1":
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip": "225.0.0.156+3",
                "port": "7148",
                "bit_depth" : 8,
                "sample_rate" : 3200000000,
            }
        },
        "output_data_streams":
        {
            "Stokes_I_0":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.172",
                "port": "7152",
            },
            "Stokes_I_1":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.173",
                "port": "7152",
            },
            "Stokes_Q_0":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.174",
                "port": "7152",
            },
            "Stokes_Q_1":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.175",
                "port": "7152",
            },
            "Stokes_U_0":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.176",
                "port": "7152",
            },
            "Stokes_U_1":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.177",
                "port": "7152",
            },
            "Stokes_V_0":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.178",
                "port": "7152",
            },
            "Stokes_V_1":
            {
                "format": "GatedSpectrometer:1",
                "ip": "225.0.0.179",
                "port": "7152",
            }
        },
        "computing_cores": {                                # If auto, cores are selected automatically for numa node. otherwise, a list pf cores is selected and directly used
                "mkrecv": "auto",
                "mksend": "auto",
                "gated_spectrometer": "auto"
                },

        "fft_length": 1024 * 1024 * 2 // 2,
        "naccumulate": 512,
        "disable_gate": -1,                                 # Disable calculation of a gate.
        "output_directory": "/mnt",                         # ToDo: Should be a output data stream def.
        "output_type": 'network',                           # ['network', 'disk', 'null']  ToDo: Should be a output data stream def.
        "dummy_input": False,                               # Use dummy input instead of mkrecv process. Should be input data stream option.
        "nonfatal_numacheck": False,                             # Ignore numa node constraints, e.g. due to missing networks for debugging
        "log_level": "debug",

        "output_rate_factor": 1.10,                         # True output date rate is multiplied by this factor for sending.
        "idx1_modulo": "auto",
    }


# static configuration for mkrec. all items that can be configured are passed
# via cmdline
_mkrecv_header = """
## Dada header configuration
HEADER          DADA
HDR_VERSION     1.0
HDR_SIZE        4096
DADA_VERSION    1.0

## MKRECV configuration
PACKET_SIZE         8400
IBV_VECTOR          -1          # IBV forced into polling mode
IBV_MAX_POLL        10
BUFFER_SIZE         128000000

SAMPLE_CLOCK_START  0 # This is updated with the sync-time of the packetiser to allow for UTC conversion from the sample clock

DADA_NSLOTS         3
SLOTS_SKIP          4  # Skip the first four slots

#SPEAD specifcation for EDD packetiser data stream
NINDICES            2      # Although there is more than one index, we are only receiving one polarisation so only need to specify the time index

# The first index item is the running timestamp
IDX1_ITEM           0      # First item of a SPEAD heap

IDX2_ITEM 1
IDX2_LIST 0,1
IDX2_MASK 0x1

# Add side item to buffer
SCI_LIST            2
"""

# static configuration for mksend. all items that can be configured are passed
# via cmdline
_mksend_header = """
HEADER          DADA
HDR_VERSION     1.0
HDR_SIZE        4096
DADA_VERSION    1.0

# MKSEND CONFIG
NETWORK_MODE  1
PACKET_SIZE 8400
IBV_VECTOR   -1          # IBV forced into polling mode
IBV_MAX_POLL 10

SYNC_TIME           unset  # Default value from mksend manual
SAMPLE_CLOCK        unset  # Default value from mksend manual
UTC_START           unset  # Default value from mksend manual

#number of heaps with the same time stamp.
HEAP_COUNT 1
HEAP_ID_OFFSET  1
HEAP_ID_STEP    13

NSCI            2
NITEMS          11
ITEM1_ID        5632    # timestamp, slowest index
ITEM1_SERIAL

ITEM2_ID        5633    # polarization
ITEM2_LIST      0,1,2,3
ITEM2_INDEX     2

ITEM3_ID        5634    # noise diode status
ITEM3_LIST      0,1
ITEM3_INDEX     3

ITEM4_ID        5635    # fft_length

ITEM5_ID        5636
ITEM5_SCI       1       # number of input heaps with ndiode on/off

ITEM6_ID        5637    # sync_time

ITEM7_ID        5638    # sampling rate
ITEM8_ID        5639    # naccumulate

ITEM9_ID        5640    # payload item (empty step, list, index and sci)

ITEM10_ID        5641    # payload item (empty step, list, index and sci)
ITEM10_SCI       2       # number of saturated heaps

ITEM11_ID       5642    # type
"""


class GatedFullStokesSpectrometerPipeline(EDDPipeline):
    """Full Stokes Spectrometer
    """

    def __init__(self, ip, port, loop=None):
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)
        self._mkrec_proc = []
        self._dada_buffers = []
        self.__watchdogs = []
        self.__coreManager = None
        self.stream_description = {}

        self.input_buffer_last_good = None


    def setup_sensors(self):
        """
        Setup monitoring sensors
        """
        super().setup_sensors()

        self._integration_time_status = Sensor(
            float, "integration-time",
            description="Integration time [s]",
            initial_status=Sensor.Status.UNKNOWN
        )
        self.sensors.add(self._integration_time_status)

        self._output_rate_status = Sensor(
            float, "output-rate",
            description="Output data rate [Gbyte/s]",
            initial_status=Sensor.Status.UNKNOWN
        )
        self.sensors.add(self._output_rate_status)

        self._mkrecv_sensors = MkrecvSensors("")

        for s in self._mkrecv_sensors.sensors.values():
            self.sensors.add(s)

        self._input_buffer_fill_level = Sensor(
            float, "input-buffer-fill-level",
            description="Fill level of the input buffer"
        )
        self.sensors.add(self._input_buffer_fill_level)

        self._input_buffer_total_write = Sensor(
            float, "input-buffer-total-write",
            description="Total write into input buffer"
        )
        self.sensors.add(self._input_buffer_total_write)

        self._output_buffer_fill_level = Sensor(
            float, "output-buffer-fill-level",
            description="Fill level of the output buffer"
        )
        self.sensors.add(self._output_buffer_fill_level)
        self._output_buffer_total_read = Sensor(
            float, "output-buffer-total-read",
            description="Total read from output buffer"
        )
        self.sensors.add(self._output_buffer_total_read)


    def buffer_status_handle(self, status):
        """
        Process a change in the buffer status
        """
        timestamp = time.time()
        if status['key'] == self._dada_buffers[-2].key:
            conditional_update(self._input_buffer_total_write, status['written'], timestamp=timestamp)
            self._input_buffer_fill_level.set_value(status['fraction-full'], timestamp=timestamp)
            if status['fraction-full'] < self._config['input_buffer_critical_level']:
                self.input_buffer_last_good = timestamp
            elif self.state == 'streaming':
                _log.warning('Input buffer fill level %f > %f!', status['fraction-full'],
                        self._config['input_buffer_critical_level'])
                if timestamp - self.input_buffer_last_good > self._config['input_buffer_critical_timeout']:
                    self.state = "error"

        elif status['key'] == self._dada_buffers[-1].key:
            self._output_buffer_fill_level.set_value(status['fraction-full'], timestamp=timestamp)
            conditional_update(self._output_buffer_total_read, status['read'], timestamp=timestamp)


    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD gated spectrometer
        """
        _log.info("Configuring EDD backend for processing")

        _log.info("Final configuration:\n%s", json.dumps(self._config, indent=4))

        self.__numa_node_pool = []
        # remove numa nodes with missing capabilities
        for node in numa.getInfo():
            if len(numa.getInfo()[node]['gpus']) < 1:
                _log.debug("Not enough gpus on numa node {} - removing from pool.".format(node))
                continue
            elif len(numa.getInfo()[node]['net_devices']) < 1:
                _log.debug("Not enough nics on numa node {} - removing from pool.".format(node))
                continue
            else:
                self.__numa_node_pool.append(node)

        _log.debug("{} numa nodes remaining in pool after constraints.".format(len(self.__numa_node_pool)))

        if len(self.__numa_node_pool) == 0:
            if self._config['nonfatal_numacheck']:
                _log.warning("Not enough numa nodes to process data!")
                self.__numa_node_pool = list(numa.getInfo().keys())
            else:
                raise FailReply("Not enough numa nodes to process data!")

        self._subprocessMonitor = SubprocessMonitor()

        if len(self._config['input_data_streams']) != 2:
            raise FailReply("Require 2 polarization input, got {}".format(len(self._config['input_data_streams'])))

        _log.debug("Merging ip ranges")
        self.stream_description = copy.deepcopy(self._config['input_data_streams']['polarization_0'])
        self.stream_description["ip"] += ",{}".format(self._config['input_data_streams']['polarization_1']["ip"])

        _log.debug("Merged ip ranges: {}".format(self.stream_description["ip"]))

        self.input_heapSize = self.stream_description["samples_per_heap"] * self.stream_description['bit_depth'] // 8

        nHeaps = self._config["samples_per_block"] // self.stream_description["samples_per_heap"]
        input_bufferSize = nHeaps * (self.input_heapSize + 64 // 8)
        _log.info('Input dada parameters created from configuration:\n\
                heap size:        {} byte\n\
                heaps per block:  {}\n\
                buffer size:      {} byte'.format(self.input_heapSize, nHeaps, input_bufferSize))

        # calculate output buffer parameters
        nSlices = max(self._config["samples_per_block"] // 2 // self._config['fft_length'] // self._config['naccumulate'], 1)
        nChannels = self._config['fft_length'] // 2 + 1
        # on / off spectrum  + one side channel item per spectrum

        output_bufferSize = nSlices * (8 * (nChannels * 32 // 8 + 2*8))

        output_heapSize = nChannels * 32 // 8
        integrationTime = self._config['fft_length'] * self._config['naccumulate'] / (float(self.stream_description["sample_rate"]))
        self._integration_time_status.set_value(integrationTime)
        rate = output_heapSize / integrationTime   # in spead documentation BYTE per second and not bit!
        rate *= self._config["output_rate_factor"]        # set rate to (100+X)% of expected rate
        self._output_rate_status.set_value(rate / 1E9)

        _log.info('Output parameters calculated from configuration:\n\
                spectra per block:  {} \n\
                nChannels:          {} \n\
                buffer size:        {} byte \n\
                integrationTime :   {} s \n\
                heap size:          {} byte\n\
                rate ({:.0f}%):        {} Gbps'.format(nSlices, nChannels, output_bufferSize, integrationTime, output_heapSize, self._config["output_rate_factor"]*100, rate / 1E9))

        numa_node = self.__numa_node_pool[0]
        _log.debug("Associating with numa node {}".format(numa_node))

        # configure dada buffer

        self._dada_buffers.append(
            dada.DadaBuffer(input_bufferSize, n_slots=64, node_id=numa_node, pin=True, lock=True)
        )
        await self._dada_buffers[-1].create()

        self._dada_buffers[-1].get_monitor(self.buffer_status_handle).start()

        self._dada_buffers.append(
            dada.DadaBuffer(output_bufferSize, n_slots=8 * nSlices, node_id=numa_node, pin=True, lock=True)
        )
        await self._dada_buffers[-1].create()
        self._dada_buffers[-1].get_monitor(self.buffer_status_handle).start()

        ## specify all subprocesses
        self.__coreManager = CoreManager(numa_node, self._config['computing_cores'])
        self.__coreManager.add_task("gated_spectrometer", 1)

        N_inputips = 0
        for p in self.stream_description["ip"].split(','):
            N_inputips += len(ipstring_to_list(p))
        _log.debug("Found {} input ips".format(N_inputips))

        if not self._config["dummy_input"]:
            self.__coreManager.add_task("mkrecv", N_inputips + 1, prefere_isolated=True)

        if self._config["output_type"] == "network":
            self.__coreManager.add_task("mksend", 2)

        if self._config['disable_gate'] in [0, 1]:
            disable_gate_opt = "--disable_gate={}".format(self._config['disable_gate'])
        else:
            disable_gate_opt = ""

        # Configure + launch

        cmd = "taskset -c {physcpu} gated_spectrometer {disable_gate_opt} --nsidechannelitems=1 --input_key={dada_key} --speadheap_size={heapSize} --selected_sidechannel=0 --nbits={bit_depth} --fft_length={fft_length} --naccumulate={naccumulate} -o {ofname} --log_level={log_level} --output_format=Stokes  --input_polarizations=Dual --output_type=dada".format(dada_key=self._dada_buffers[-2].key, ofname=self._dada_buffers[-1].key, heapSize=self.input_heapSize, numa_node=numa_node, bit_depth=self.stream_description['bit_depth'], physcpu=self.__coreManager.get_coresstr('gated_spectrometer'), disable_gate_opt=disable_gate_opt, **self._config)
        _log.debug("Command to run: {}".format(cmd))

        cudaDevice = numa.getInfo()[numa_node]['gpus'][0]
        gated_cli = ManagedProcess(cmd, env={"CUDA_VISIBLE_DEVICES": cudaDevice})
        _log.debug("Visble Cuda Device: {}".format(cudaDevice))
        self._subprocessMonitor.add(gated_cli, self._subprocess_error)
        self._subprocesses.append(gated_cli)

        cfg = self._config.copy()
        cfg.update(self.stream_description)
        cfg["dada_key"] = self._dada_buffers[-2].key

        ip_range = []
        port = set()
        for key in self._config["output_data_streams"]:
            ip_range.append(self._config["output_data_streams"][key]['ip'])
            port.add(self._config["output_data_streams"][key]['port'])
        if len(port) != 1:
            raise FailReply("Output data has to be on the same port! ")

        if self._config["output_type"] == 'network':
            mksend_header_file = tempfile.NamedTemporaryFile(delete=False, mode='w')
            mksend_header_file.write(_mksend_header)
            mksend_header_file.close()

            nhops = len(ip_range)

            timestep = cfg["fft_length"] * cfg["naccumulate"]

            # select network interface
            fastest_nic, nic_params = numa.getFastestNic(numa_node)
            heap_id_start = 0  # 2 * i    # two output spectra per pol

            _log.info("Sending data on NIC {} [ {} ] @ {} Mbit/s".format(fastest_nic, nic_params['ip'], nic_params['speed']))
            cmd = "taskset -c {physcpu} mksend --header {mksend_header} --heap-id-start {heap_id_start} --dada-key {ofname} --ibv-if {ibv_if} --port {port_tx} --sync-epoch {sync_time} --sample-clock {sample_rate} --item1-step {timestep} --item4-list {fft_length} --item6-list {sync_time} --item7-list {sample_rate} --item8-list {naccumulate} --rate {rate} --item11-list 1 --heap-size {heap_size} --nhops {nhops} {mcast_dest}".format(mksend_header=mksend_header_file.name, heap_id_start=heap_id_start, timestep=timestep,
                        ofname=self._dada_buffers[-1].key, nChannels=nChannels, physcpu=self.__coreManager.get_coresstr('mksend'), integrationTime=integrationTime,
                        rate=rate, nhops=nhops, heap_size=output_heapSize, ibv_if=nic_params['ip'],
                        mcast_dest=" ".join(ip_range),
                        port_tx=port.pop(), **cfg)

            _log.debug("Command to run: {}".format(cmd))
            mks = ManagedProcess(cmd, env={"CUDA_VISIBLE_DEVICES": cudaDevice})

        elif self._config["output_type"] == 'disk':
            ofpath = os.path.join(cfg["output_directory"], self._dada_buffers[-1].key)
            _log.debug("Writing output to {}".format(ofpath))

            if not os.path.isdir(ofpath):
                os.makedirs(ofpath)
            mks = dada.DbDisk(self._dada_buffers[-1].key, directory=ofpath).start()
        else:
            _log.warning("Selected null output. Not sending data!")
            mks = dada.DbNull(self._dada_buffers[-1].key, zerocopy=True).start()

        self._subprocessMonitor.add(mks, self._subprocess_error)
        self._subprocesses.append(mks)

        self._subprocessMonitor.start()


    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self, config_json=""):
        """
        Start streaming of spectrometer output.
        """
        _log.info("Starting capturing")
        self.input_buffer_last_good = time.time()
        try:
            mkrecvheader_file = tempfile.NamedTemporaryFile(delete=False, mode="w")
            _log.debug("Creating mkrec header file: {}".format(mkrecvheader_file.name))
            mkrecvheader_file.write(_mkrecv_header)
            # DADA may need this
            # ToDo: Check for input stream definitions
            mkrecvheader_file.write("NBIT {}\n".format(self.stream_description["bit_depth"]))
            mkrecvheader_file.write("HEAP_SIZE {}\n".format(self.input_heapSize))

            mkrecvheader_file.write("\n#OTHER PARAMETERS\n")
            mkrecvheader_file.write("samples_per_block {}\n".format(self._config["samples_per_block"]))

            mkrecvheader_file.write("\n#PARAMETERS ADDED AUTOMATICALLY BY MKRECV\n")
            mkrecvheader_file.close()

            cfg = copy.deepcopy(self._config)
            cfg.update(self.stream_description)
            cfg["dada_key"] = self._dada_buffers[-2].key
            if not self._config['dummy_input']:
                numa_node = self.__numa_node_pool[0]
                fastest_nic, nic_params = numa.getFastestNic(numa_node)
                _log.info("Receiving data on NIC {} [ {} ] @ {} Mbit/s".format(fastest_nic, nic_params['ip'], nic_params['speed']))

                if self._config['idx1_modulo'] == 'auto':    # Align along output ranges
                    idx1modulo = self._config['fft_length'] * self._config['naccumulate']
                else:
                    idx1modulo = self._config['idx1_modulo']

                cmd = "taskset -c {physcpu} mkrecv_v4 --quiet --dnswtwr --lst --header {mkrecv_header} --idx1-step {samples_per_heap} --heap-size {input_heap_size} --idx1-modulo {idx1modulo}  --nthreads {nthreads}\
                --dada-key {dada_key} --sync-epoch {sync_time} --sample-clock {sample_rate} \
                --ibv-if {ibv_if} --port {port} {ip}".format(mkrecv_header=mkrecvheader_file.name, physcpu=self.__coreManager.get_coresstr('mkrecv'), ibv_if=nic_params['ip'], input_heap_size=self.input_heapSize, idx1modulo=idx1modulo, nthreads=len(self.__coreManager.get_cores('mkrecv')) - 1, **cfg )
                mk = ManagedProcess(cmd, stdout_handler=self._mkrecv_sensors.stdout_handler)
            else:
                _log.warning("Creating Dummy input instead of listening to network!")
                mk = dada.JunkDb(self._dada_buffers[-2].key, header=mkrecvheader_file.name, data_rate=1e9, duration=3600).start()

            self._mkrec_proc.append(mk)
            self._subprocessMonitor.add(mk, self._subprocess_error)
            self._subprocesses.append(mk)

        except Exception as E:
            _log.error("Error starting pipeline: {}".format(E))
            raise E

        wd = SensorWatchdog(self._input_buffer_total_write, self._config['input_buffer_critical_timeout'], self.watchdog_error)
        self.__watchdogs.append(wd)
        # Wait for one integration period before finishing to ensure
        # streaming has started before OK
        await asyncio.sleep(self._integration_time_status.value)
        wd.start()


    @state_change(target="idle", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        """
        Stop streaming of data
        """
        _log.info("Stopping spectrometer")
        await self.deconfigure()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        Deconfigure the gated spectrometer pipeline.
        """
        _log.info("Deconfiguring EDD backend")
        for wd in self.__watchdogs:
            wd.stop_event.set()
        if self._subprocessMonitor is not None:
            self._subprocessMonitor.stop()

        _log.debug("Stopping mkrecv processes ...")
        for proc in self._mkrec_proc:
            proc.terminate()
        # This will terminate also the gated spectrometer automatically
        for proc in self._subprocesses:
            proc.terminate()

        _log.debug("Destroying dada buffers")

        tasks = []
        for buf in self._dada_buffers:
            tasks.append(asyncio.create_task(buf.destroy()))
        await asyncio.gather(*tasks)
        self._dada_buffers = []
        self._mkrec_proc = []
        self.__coreManager = None


if __name__ == "__main__":
    asyncio.run(launchPipelineServer(GatedFullStokesSpectrometerPipeline))
