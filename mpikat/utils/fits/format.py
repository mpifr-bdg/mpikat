import ctypes
import numpy as np
import io
import time
from typing import List

TYPE_MAP = {
    "F": (ctypes.c_float, np.dtype('float32') ,"F"),
    "I": (ctypes.c_uint32, np.dtype('uint32'), "I")
}
# Also add additional lookups for ctypes / numpy types
for i in list(TYPE_MAP.values()):
    TYPE_MAP[i[0]] = i
    TYPE_MAP[i[1]] = i


def btdt(dtype):
    """
    Helper to format the dtype in the lookup functions
    """
    if isinstance(dtype, bytes):
        dt = str(dtype, 'utf-8').strip()
    else:
        dt = dtype
    return dt

def c_dtype(dtype: str="F") -> ctypes:
    """ lookup ctype from fits type , resp. numpy type"""
    return TYPE_MAP[btdt(dtype)][0]

def np_dtype(dtype: str="F") -> np.dtype:
    """ lookup numpy type from fits type , resp. c type"""
    return TYPE_MAP[btdt(dtype)][1]

def f_dtype(dtype) -> str:
    """ lookup fits type from numpy type , resp. c type"""
    return TYPE_MAP[btdt(dtype)][2]


def itemsize(dtype: str="F") -> int:
    return ctypes.sizeof(c_dtype(dtype))

def _check_payload(data: np.ndarray) -> tuple:
    if len(data.shape) == 2:
        return data.shape
    if len(data.shape) == 1:
        return 1, len(data)
    else:
        raise ValueError("Unexpected shape of data")


def getFitsTime(t: float) -> str:
    """_summary_

    Args:
        t (float): _description_

    Returns:
        str: _description_
    """
    ts = time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(t))
    ts += '.' + f'{t:.4f}UTC'.split('.')[1]
    return ts.encode("ascii")



class FitsHeader(ctypes.LittleEndianStructure):
    # Fits header is of exactly 64 bytes, thus _pack alignment does not need to
    # be adjusted
    _fields_ = [
        ("data_type", ctypes.c_char * 4),
        ("channel_data_type", ctypes.c_char * 4),
        ("packet_size", ctypes.c_uint32),
        ("backend_name", ctypes.c_char * 8),
        ("timestamp", ctypes.c_char * 28),
        ("integration_time", ctypes.c_uint32),
        ("blank_phases", ctypes.c_uint32),
        ("nsections", ctypes.c_uint32),
        ("blocking_factor", ctypes.c_uint32)
    ]

    def __init__(self):
        super().__init__()
        self.timestamp = getFitsTime(0) 

    def __repr__(self):
        return "<{} {}>".format(self.__class__.__name__, ", ".join(
            ["{} = {}".format(
                key, getattr(self, key)) for key, _ in self._fields_]))

    def __str__(self) -> str:
        s = "\n----------\nHeader data:\n----------\n"
        for key, __ in self._fields_:
            s += f"{key}: {getattr(self, key)}\n"
        return s


    @property
    def item_size(self) -> int:
        return itemsize(self.channel_data_type)

    @property
    def np_dtype(self) -> np.dtype:
        return np_dtype(self.channel_data_type)

    @property
    def c_dtype(self) -> ctypes:
        return c_dtype(self.channel_data_type)


class FitsSection(ctypes.LittleEndianStructure):
    _pack_ = 1   # Disable memory alignment of cstruct. This is important for correct network transfer of arrays of byte lengths that are not a power of two.
    _fields_ = [
        ('section_id', ctypes.c_uint32),
        ('nchannels', ctypes.c_uint32)
    ]

    def __init__(self, section_id: int=1, nchannels: int=None, data: np.ndarray=None):
        self.section_id = section_id

        if nchannels is None and data is None:
            raise RuntimeError("FitsSection cosntruction requires either 'nchannels' or 'data'")
        if nchannels is None:
            nchannels = data.size
        if data is None:
            data = np.zeros(nchannels)

        self.nchannels = nchannels
        self._data = data

        assert self._data.size == nchannels

    def __repr__(self):
        return "<{} {}>".format(self.__class__.__name__, ", ".join(
            ["{} = {}".format(
                key, getattr(self, key)) for key, _ in self._fields_]))

    def __str__(self) -> str:
        s = ""
        for key, __ in self._fields_:
            s += f"{key}: {getattr(self, key)}\n"
        return s

    @property
    def data(self) -> np.ndarray:
        return self._data

    @data.setter
    def data(self, data: np.ndarray):
        if self.nchannels != len(data):
            raise ValueError(f"Shape mismatch, expected {self.nchannels} got {len(data)}")
        self._data = data

    @property
    def size(self):
        return ctypes.sizeof(FitsSection) + self._data.nbytes

class FitsPayload:

    def __init__(self):
        self.sections = []
        self._index = 0

    def __str__(self) -> str:
        s = "\n----------\nSection data:\n----------\n"
        for sec in self:
            s += str(sec).replace("\n", "\t") + "\n"
        return s

    def __bytes__(self) -> bytes:
        b = b''
        for sec in self.sections:
            b += bytes(sec) + sec.data.tobytes()
        return b

    def __getitem__(self, index):
        if 0 <= index < len(self.sections):
            return self.sections[index]
        else:
            raise IndexError("Index out of range")

    def __iter__(self):
        self._index = 0
        return self

    def __next__(self):
        if self._index < len(self.sections):
            result = self.sections[self._index]
            self._index += 1
            return result
        else:
            raise StopIteration

    def frombytes(self, raw: bytes, dtype: np.dtype=np.float32) -> None:
        stream = io.BytesIO(raw)
        while stream.tell() < len(raw):
            self.sections.append(FitsSection.from_buffer_copy(stream.read(ctypes.sizeof(FitsSection))))
            self.sections[-1].data = np.frombuffer(
                stream.read(self.sections[-1].nchannels * dtype.itemsize), dtype=dtype)

    def fromarray(self, data: List[np.ndarray]):
        for i, d in enumerate(data):
            self.sections.append(FitsSection(i+1, data=d))
        return self

    def add(self, section: FitsSection):
        if not isinstance(section, FitsSection):
            raise TypeError(f"Setting section with type {type(section)} is not allowd")
        self.sections.append(section)

    def section(self, section_id):
        return self.sections[section_id-1]

    @property
    def channel_data_type(self):
        s = set([s.data.dtype for s in self.sections])
        if len(s) != 1:
            raise RuntimeError(f'Payload has {len(s)} different types of data, but can be only one!')
        return s.pop()


class FitsPacket:
    def __init__(self):
        self._sections = []
        self.dtype = TYPE_MAP["F"]
        self._hdr = FitsHeader()
        self._pay = FitsPayload()
        self._hdr_init = False

    def __bytes__(self) -> bytes:
        return bytes(self._hdr) + bytes(self._pay)

    def __str__(self) -> str:
        return str(self._hdr) + str(self._pay)

    @property
    def header_size(self) -> int:
        return ctypes.sizeof(FitsHeader)

    @property
    def item_size(self) -> int:
        return self._hdr.item_size

    @property
    def np_dtype(self) -> np.dtype:
        return self._hdr.np_dtype

    @property
    def c_dtype(self) -> ctypes:
        return self._hdr.c_dtype

    @property
    def payload_size(self):
        return self._hdr.packet_size - self.header_size

    @property
    def payload(self):
        return self._pay

    @property
    def header(self) -> FitsHeader:
        return self._hdr

    @payload.setter
    def payload(self, pay: FitsPayload) -> None:
        if not isinstance(pay, FitsPayload):
            raise TypeError(f"Setting payload with type {type(pay)} is not allowd")

        self._hdr.nsections = len(pay.sections)
        self._hdr.packet_size = self.header_size + sum([s.size for s in pay.sections])
        self._hdr.channel_data_type = f'{f_dtype(pay.channel_data_type)}   '.encode('ascii')

        self._pay = pay

    @header.setter
    def header(self, hdr: FitsHeader) -> None:
        if not isinstance(hdr, FitsHeader):
            raise TypeError("Setting header with type %s is not allowd", type(hdr))
        self._hdr = hdr
        self._hdr_init = True

    def setPayload(self, raw: bytes) -> None:
        if not self._hdr_init:
            raise RuntimeError("Setting payload without initialized FitsHeader")
        if len(raw) != self.payload_size:
            raise ValueError(f"Expected {self.payload_size} bytes, but got {len(raw)} bytes")
        self._pay.frombytes(raw, self.np_dtype)

    def setHeader(self, raw: bytes) -> None:
        if len(raw) != self.header_size:
            raise ValueError("Expected %d bytes, but got %d bytes", ctypes.sizeof(FitsHeader), len(raw))
        self._hdr = FitsHeader.from_buffer_copy(raw)
        self._hdr_init = True

    def section(self, section_id: int) -> FitsSection:
        return self._pay[section_id]

    def frombytes(self, raw: bytes):
        stream = io.BytesIO(raw)
        self.setHeader(stream.read(self.header_size))
        self.setPayload(stream.read(self.payload_size))
