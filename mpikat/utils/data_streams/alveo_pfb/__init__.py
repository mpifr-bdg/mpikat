from mpikat.core.data_stream import DataStream, DataStreamRegistry, edd_spead


class AlveoPFBFormat(DataStream):
    """
    Format description oof Redis Spectrum stream
    """
    samples_per_heap = 2048

    stream_meta = {
        "type": "AlveoPFB:1",
        "ip": "",
        "port": "",
        "bit_depth" : 8,
        "sample_rate" : 0,
        "sync_time" : 0,
        "central_freq": 0,
        "receiver_id": "",
        "polarization": None,
        "samples_per_heap": samples_per_heap,
        "description": "Spead stream of Alveo PFB data."
    }

    data_items = [edd_spead(5632, "timestep"),
                  edd_spead(5633, "channel_id"),
                  edd_spead(5634, "payload")]

DataStreamRegistry.register(AlveoPFBFormat)