import math
from subprocess import Popen, PIPE
from mpikat.utils.pipe_monitor import PipeMonitor
from mpikat.utils.process_tools import ManagedProcess
import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.utils.dada_tools.monitor")

class DbMonitor:
    """
    Monitor a dada buffer using dada_dbmonitor
    """
    def __init__(self, key, callback=None):
        """Construct a DbMonitor object for monitoring statistics of a DADA
        buffer.

        Parameters:
            key (str): key of dadabuffer to monitor
            callback (callable or list of callable): Function(s) that is/are
                parsing the stdout of the 'dada_dbmonitor'-process.

        Returns:
            DbMonitor: An instance of the the DbMonitor class.

        """
        self._key = key
        self._dbmon_proc: ManagedProcess = None
        self._callback = []
        self.add_callback(callback)

        self.__parsed_lines = 0
        self.__headersize = 12 # Skip first 12 lines

    def _stdout_parser(self, line):
        """Triggered when the 'dada_dbmonitor'-process writes anew line to
        stdout. The function parses this line and passes it to all added
        callback functions.

        Parameters:
            line (str): The line to parse

        """
        self.__parsed_lines += 1
        if self.__parsed_lines < self.__headersize:
            return

        line = line.strip()

        try:
            values = list(map(int, line.split()))
            free, full, clear, written, read = values[5:]
            try:
                fraction = float(full)/(full + free)
            except ZeroDivisionError:
                fraction = math.inf
            params = {
                "key": self._key,
                "fraction-full": fraction,
                "written": written,
                "read": read,
                "clear": clear
            }
        except Exception as error: # pylint: disable=broad-except
            _log.warning("Unable to parse line %s with error %s", line, error)
            return
        for callback in self._callback:
            try:
                callback(params)
            except Exception as error:
                _log.error("Could not use callback %s, error %s", callback, error)

    def add_callback(self, callback):
        """Add callback function(s). Callbacks are triggered when the monitor
        reads a line.

        Parameters:
            callback (callable or list of callables):
                Function(s) that is/are passed to the monitor parser.
        """
        if not isinstance(callback, list):
            callback = [callback]
        for cb in callback:
            if callable(cb):
                self._callback.append(cb)
            else:
                _log.error("The object %s is not callable", cb)

    def rm_callback(self, id):
        """Removes a callback from the list of callbacks by an given ID"""
        self._callback.pop(id)

    def get_callback(self):
        """Get a list of all added callback-function.

        Returns
            list: returns the class attribute '_callback'

        """
        return self._callback

    def start(self):
        """Start the monitor subprocess"""
        self._dbmon_proc = ManagedProcess(["dada_dbmonitor", "-k", self._key],
                                          stderr_handler=self._stdout_parser)

    def stop(self):
        """Stop the monitor subprocess"""
        _log.debug("Stopping monitor thread for dada buffer: %s", self._key)
        if self._dbmon_proc is not None:
            self._dbmon_proc.terminate(timeout=2)
