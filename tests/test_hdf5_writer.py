import unittest
import tempfile
import os
import asyncio
import json

from mpikat.pipelines.hdf5_writer import HDF5Writer
from mpikat.core import logger
from mpikat.utils import testing, get_port

from aiokatcp.client import Client

_log = logger.getLogger("mpikat.testHDF5Writer")


host = "127.0.0.1"

class TestHDF5Writer(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.port = get_port()
        self.server_thread = testing.launch_server_in_thread(HDF5Writer, host, self.port)

    async def asyncSetUp(self):
        self.client = await asyncio.wait_for(
            Client.connect(host, self.port), timeout=5.0
        )

    def tearDown(self):
        self.client.close()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)


    async def test_file_creation(self):
        """
        File should be created during measurement start-sop cycle
        """
        self.assertEqual(self.server_thread.server.state, 'idle')

        with tempfile.TemporaryDirectory() as tmpdirname:
            await self.server_thread.server.set(dict(use_date_based_subfolders=False))

            await self.client.request('configure', '{}')
            self.assertEqual(self.server_thread.server.state, 'configured')
            await self.client.request('capture-start')
            self.assertEqual(self.server_thread.server.state, 'ready')
            fname = os.path.join(tmpdirname, 'foo.h5')
            await self.client.request('measurement-prepare', json.dumps({"filename": fname, "new_file": True}))
            self.assertEqual(self.server_thread.server.state, 'set')
            await self.client.request('measurement-start')
            self.assertEqual(self.server_thread.server.state, 'measuring')

            # Waiting
            counter = 0
            while not os.path.isfile(fname):
                await asyncio.sleep(.1)
                counter += 1
                if counter > 100:
                    raise TimeoutError('Output file not created in time!')
            await self.client.request('measurement-stop')
            self.assertTrue(os.path.isfile(fname))
        await self.client.request('deconfigure')
        self.assertEqual(self.server_thread.server.state, 'idle')

if __name__ == '__main__':
    unittest.main()
