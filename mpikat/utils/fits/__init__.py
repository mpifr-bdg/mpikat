"""
The fits module provides a interface for receiving FITS data. It offers
ctype.LittleEndianess-classes to represent FITS packets. The FitsWriterClient-class
allows receiving and recodring FITS data
"""
import numpy as np
import ctypes

from mpikat.utils.fits.format import TYPE_MAP, FitsSection, FitsHeader, FitsPacket, FitsPayload, _check_payload, itemsize, np_dtype, c_dtype, getFitsTime
from mpikat.utils.fits.sender import Sender
from mpikat.utils.fits.receiver import Receiver, recv_nbytes, recv_packet
from mpikat.utils.fits.stream import FitsGatedSpectrumDataStreamFormat


hdr_defaults = {
    "data_type": "EEEI".encode("ascii"),
    "channel_data_type": "F   ".encode("ascii"),
    "nsections": 0,
    "packet_size": ctypes.sizeof(FitsHeader),
    "backend_name": "EDDSPEAD".encode("ascii"),
    "timestamp": getFitsTime(0),
    "integration_time": 1,
    "blank_phases": 1,
    "blocking_factor": 0
}

pkt_defaults = hdr_defaults
pkt_defaults["nchannels"] = 1000
pkt_defaults["data"] = None

def create_header(**kwargs) -> FitsHeader:
    """_summary_

    Returns:
        FitsHeader: _description_
    """
    header = FitsHeader()
    for key, val in hdr_defaults.items():
        kwargs.setdefault(key, val)
    for key, val in kwargs.items():
        header.__setattr__(key, val)
    return header

def create_payload(**kwargs) -> FitsPayload:
    """_summary_

    Args:
        nsections (int, optional): _description_. Defaults to 2.
        nchannels (int, optional): _description_. Defaults to 1000.
        data (np.ndarray, optional): _description_. Defaults to None.
    """
    for key, val in pkt_defaults.items():
        kwargs.setdefault(key, val)
    if kwargs["data"] is not None:
        kwargs["nsections"], kwargs["nchannels"] = _check_payload(kwargs["data"])
    else:
        dtype = np_dtype(kwargs["channel_data_type"])
        kwargs["data"] = np.random.rand(kwargs["nsections"], kwargs["nchannels"]).astype(dtype)
    payload = FitsPayload()
    payload.fromarray(kwargs["data"])
    return payload

def create_packet(**kwargs) -> FitsPacket:
    """_summary_

    Args:
        nsections (int, optional): _description_. Defaults to 2.
        nchannels (int, optional): _description_. Defaults to 1000.
        int_time (int, optional): _description_. Defaults to 1.
        blank (int, optional): _description_. Defaults to 1.

    Returns:
        FitsPacket: _description_
    """
    packet = FitsPacket()
    for key, val in pkt_defaults.items():
        kwargs.setdefault(key, val)
    if kwargs["data"] is not None:
        kwargs["nsections"], kwargs["nchannels"] = _check_payload(kwargs["data"])
    kwargs["packet_size"] = ctypes.sizeof(FitsHeader) \
        + kwargs["nsections"] * (ctypes.sizeof(FitsSection) \
        + kwargs["nchannels"] * itemsize(kwargs["channel_data_type"]))
    packet.header = create_header(**kwargs)
    packet.payload = create_payload(**kwargs)
    return packet


__all__ = ["TYPE_MAP",
    "FitsSection",
    "FitsHeader",
    "FitsPacket",
    "Sender",
    "recv_nbytes",
    "recv_packet",
    "Receiver",
    "FitsGatedSpectrumDataStreamFormat"
]
