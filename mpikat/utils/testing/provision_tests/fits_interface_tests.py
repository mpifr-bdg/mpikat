import asyncio

from mpikat.core import logger

from . registry import  register
from . common import Stage

_log = logger.getLogger('mpikat.utils.testing.provision_tests.fits_interface_tests')


@register(stage=Stage.capture_start, class_name='fits_interface')
async def test_fw_is_receiving_data(context):
    """
    The pipeline should receive data.
    """
    # Check number of expected polarizations
    _log.debug('Checking input data streams')

    await asyncio.sleep(25)
    _, results = await context.pipeline.request("sensor-value", f'complete-heaps')
    heaps = float(results[0].arguments[4].decode('ascii'))

    assert heaps > 0, "No heaps received"


@register(stage=Stage.capture_start, class_name='fits_interface')
async def test_fw_stable_receiving(context):
    """
    The number of incomplete heaps should be small
    """
    # Check number of expected polarizations
    _log.debug('Checking input data streams')

    await asyncio.sleep(5)
    _, results = await context.pipeline.request("sensor-value", f'incomplete-heaps')
    heaps = float(results[0].arguments[4].decode('ascii'))

    assert heaps < 24, "Too many incomplete heaps!"



@register(stage=Stage.measurement_start, class_name='fits_interface', tbc=True)
async def test_bandpass(context):
    """
    The pipeline should produce a bandpass PNG. Image needs to be checked manually.
    """
    # Bandpass needs some time to appear
    await asyncio.sleep(20)
    _, result = await context.pipeline.request("sensor-value", 'bandpass')
    context.output.append(result[0].arguments[4].decode('ascii'))
