import ipaddress
import os
import tempfile
import asyncio
import json

import subprocess
import netaddr
import pynq

from mpikat.utils.ip_utils import split_ipstring, is_valid_multicast_range
from mpikat.core import logger


_log = logger.getLogger("mpikat.effelsberg.edd.edd_alveo_client")

__factory = {}


def get_alveo_ids():
    """
    Return unique ids of Alveo cards on the system
    """
    with tempfile.NamedTemporaryFile() as f:
        _log.debug('Getting list of alveo cards')
        subprocess.call(f'xbmgmt examine -f json -o {f.name} --force'.split(), stdout=subprocess.DEVNULL)
        f.seek(0)
        data = json.loads(f.read())
        _log.debug(json.dumps(data, indent=4))
        cards = [d["bdf"] for d in data['system']['host']['devices']]

    _log.debug('Found %i devices', len(cards))
    ids = []
    for cid in cards:
        _log.debug('Getting date for card %s', cid)
        with tempfile.NamedTemporaryFile(delete=False) as f:
            subprocess.call(f'xbmgmt examine -f json -o {f.name} -d {cid} --force'.split(), stdout=subprocess.DEVNULL)
            f.seek(0)
            data = json.loads(f.read())
            _log.debug(json.dumps(data, indent=4))

            ids += [d["platform"]['hardware']['serial_num'] for d in data['devices']]

    return ids 


class AlveoInterfacePFB:
    """
    Interface for PFBs on Alveo cards
    """
    def __init__(self, index, firmwarefile):
        self._index = index
        self._firmwarefile = firmwarefile
        self._fpga = None
        self._client = None

    def setFirmware(self, firmwarefile):
        """
        Set the firmware-file used for the client
        """
        self._firmwarefile = firmwarefile
        self._fpga = None
        self._client = None

    async def connect(self):
        """
        Connects to a client.
        """
        self._fpga = pynq.Device.devices[self._index]
        info = self._fpga.clocks
        _log.debug("Succcessfully Connected to FPGA - Retrieved Alveo Card info: %s",
                   "\n".join(["    -  {}: {}".format(k, v) for k, v in info.items()]))

    def is_connected(self):
        """
        Returns true if connection to Alveo exists and is active
        """
        self._client = pynq.Overlay(self._firmwarefile, download=False)
        return self._client.is_loaded()

    async def program(self):
        """
        Programs FPGA with the chosen firmware
        """
        _log.debug("Loading firmware from %s ... ", self._firmwarefile)

        #_log.warning("NOT LOADING FIRMWARE DUE TO (Hard-Coded) TEST MODE")
        #res = True
        self._client = pynq.Overlay(self._firmwarefile)

        if not self._client.is_loaded():
            raise RuntimeError(f"Error loading firmware: {self._firmwarefile}")
        _log.debug("Done loading firmware %s", self._firmwarefile)

    async def initialize(self):
        """
        Connect to FPGA and try to read system information. Reprograms FPGA if this fails.
        """
        self._client = pynq.Overlay(self._firmwarefile)

        await self.connect()
        try:
            self._client.is_loaded()
        except Exception as E:
            _log.error(
                'Error getting system information for firmware %s - reprogramming', self._firmwarefile)
            _log.exception(E)
            await self.connect()
            await self.program()

            self._client.is_loaded()


    async def set_input_bits(self, n_bits):
        """
        Set bit-depth of input data stream
        """

        _log.debug("Configuring input stream bit size to %i", n_bits)

        _control = self._client.rtl_kernel_wizard_2_1.read(0x0)
        await asyncio.sleep(0.5)

        spead_ctrl = _control | 0x00000C09
        if n_bits == 8:
            spead_ctrl = _control | 0x00002C09

        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl)

    async def lock_timestamp(self, lock_time, mcnt_diff, relock_cnts, read_cnts):
        """
        Reset all internal registers to initial state.
        """
        self._client.rtl_kernel_wizard_2_1.write(0x1C, mcnt_diff)
        self._client.rtl_kernel_wizard_2_1.write(0x18, relock_cnts)
        self._client.rtl_kernel_wizard_2_1.write(0x20, read_cnts)

        lock_time_hex = lock_time
        lock_time_lo = lock_time_hex & 0x0000ffffffff
        lock_time_hi = (lock_time_hex & 0xffff00000000) >> 32

        self._client.rtl_kernel_wizard_2_1.write(0x14, lock_time_lo)
        self._client.rtl_kernel_wizard_2_1.write(0x24, lock_time_hi)

    async def capture_start(self):
        """
        Start streaming data
        """
        _log.debug("Starting capture ...")

        _control = self._client.rtl_kernel_wizard_2_1.read(0x0)
        await asyncio.sleep(0.5)

        spead_ctrl = _control & 0xfffffdff

        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl) # sys reset, 0
        await asyncio.sleep(0.5)

        spead_ctrl = spead_ctrl | 0x00004200

        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl)  # sys reset pulse
        spead_ctrl = spead_ctrl | 0x00004000

        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl) # gbe enabled

        spead_ctrl = spead_ctrl | 0x00010000
        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl) # arming

        sync_time_msw = self._client.rtl_kernel_wizard_2_1.read(0x434)
        sync_time_lsw = self._client.rtl_kernel_wizard_2_1.read(0x438)

        lock_timestamp = sync_time_lsw + (sync_time_msw << 32)
        _log.info("Alveo timestamp locked to %i", lock_timestamp)


    async def unsubscribe_all(self):
        """
        Unsubscribe all IGMP streams
        """
        raise NotImplementedError


    async def configure(self, *args):
        """Configure the card NIC(s)"""
        raise NotImplementedError

    async def configure_inputs(self, ips0, ips1, port=None):
        """Configure input data streams of the card"""
        raise NotImplementedError

    async def configure_output(self, dest_ip, dest_port, number_of_groups=64, channels_per_group=1, packet_size=8000, board_id=0x08):
        """Configure output data streams of the card"""
        raise NotImplementedError


    async def capture_stop(self):
        """
        Stop streaming data
        """
        await self.unsubscribe_all()
        _log.debug("Stopping capture ...")
        _control = self._client.rtl_kernel_wizard_2_1.read(0x0)
        await asyncio.sleep(0.5)

        spead_ctrl = _control & 0xffffbfff # gbe disable
        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl)

    async def get_fpga_clock(self):
        """Return clock speed of the FPGA"""
        clk = round(self._fpga.clocks['clock0']['frequency'])
        _log.debug("FPGA running at %f MHz", clk)
        return clk

    async def configure_quantization_factor(self, quant_factor):
        """
        Writes quantization factor value in the firmware register
        """
        self._client.rtl_kernel_wizard_2_1.write(0x34, quant_factor)
        await asyncio.sleep(0.5)
        quant_val = self._client.rtl_kernel_wizard_2_1.read(0x34)
        if quant_factor != quant_val:
            raise RuntimeError(
                "Error setting quantization factor {}".format(hex(quant_factor)))

    async def configure_fft_shift(self, fft_shift):
        """
        Writes FFT Shift
        """
        self._client.rtl_kernel_wizard_2_1.write(0x30, fft_shift)
        await asyncio.sleep(0.5)
        ffs = self._client.rtl_kernel_wizard_2_1.read(0x30)
        if ffs != fft_shift:
            raise RuntimeError("Error setting fft_shift {}".format(hex(ffs)))

    async def reset_registers(self):
        """
        Reset all internal registers to initial state.
        """
        _log.debug("Reset all internal registers to initial state.")
        _control = self._client.rtl_kernel_wizard_2_1.read(0x0)
        await asyncio.sleep(0.5)
        _log.debug("_control = 0x%08x ", _control)
        spead_ctrl = _control & 0xfffffdff
        _log.debug("spead_ctrl = 0x%08x", spead_ctrl)

        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl)  # sys reset, 0
        await asyncio.sleep(0.5)

        spead_ctrl = spead_ctrl | 0x00000200
        _log.debug("spead_ctrl = 0x%08x", spead_ctrl)
        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl)   # sys reset, pulse
        await asyncio.sleep(0.5)

        spead_ctrl = spead_ctrl & 0xfffffdff
        _log.debug("spead_ctrl = 0x%08x", spead_ctrl)
        self._client.rtl_kernel_wizard_2_1.write(0x0, spead_ctrl) # sys reset, disabled
        await asyncio.sleep(1)


    async def set_mac(self, kernel_wizard, mac, offset):
        """
        Set a mac address to a register with given offset (0 for mac0, 0x4000
        for mac1) using specified kernel_wizard
        """
        _log.debug('Configure mac %s with offset %s', mac, offset)
        # 100G Core 0 Source MAC Lower
        addr = 0x0 + offset
        kernel_wizard.write(
            addr, (int(netaddr.EUI(mac)) & 0x0000ffffffff))
        # 100G Core 0 Source MAC Upper
        addr = 0x0 + offset + 4
        kernel_wizard.write(
            addr, ((int(netaddr.EUI(mac)) & 0xffff00000000) >> 32))

    async def set_ip(self, kernel_wizard, ip, offset):
        """
        Set an IP address to a register with given offset (0 for ip0, 0x4000
        for ip1) using specified kernel_wizard
        """
        _log.debug('Configure ip %s with offset %s', ip, offset)

        addr = 0x0 + 0x28 + offset
        int_ip = int(ipaddress.IPv4Address(ip))
        _log.debug("int(ipaddress.IPv4Address(ip)= %i", int_ip)
        kernel_wizard.write(addr, int_ip)
        read = kernel_wizard.read(addr)
        return read

    async def set_input_port(self, kernel_wizard, port, offset):
        """
        Set destination port for the multicast streams
        """
        addr = 0x0 + offset + 0x2C
        mcast_ports = (port << 16) | port
        _log.debug(" configuring multicast receive port to {} ".format(mcast_ports))
        kernel_wizard.write(addr, mcast_ports)
        await asyncio.sleep(1)


    async def unsubscribe_igmp(self, kernel_wizard, N, offset):
        """
        Unsubscribe from multicast streams
        """
        _log.debug(" Unsubscribing from %i multicast streams ... ", N)
        igmp_register_data_qsfp28_0 = (N << 24) & 0xfffffff5
        addr = 0x0 + offset + 0x2000
        kernel_wizard.write(addr, igmp_register_data_qsfp28_0)

    async def subscribe_igmp(self, kernel_wizard, N, offset):
        """
        Unsubscribe from multicast streams
        """
        _log.debug("Subscribing to %i multicast streams ... ", N)
        igmp_register_data_qsfp28_0 = (N << 24) | 0x0000000D
        addr = 0x0 + offset + 0x2000
        kernel_wizard.write(addr, igmp_register_data_qsfp28_0)
        read = kernel_wizard.read(addr)
        return read

    async def configure_input_filter_control(self, kernel_wizard, offset):
        """
        Configure input filter control
        """
        _log.debug("Configure input filter control with offset %i", offset)
        addr = 0x0 + offset + 0x38
        read_data = kernel_wizard.read(addr)
        control_new = read_data & 0xfffaf0ff  # 0xfffaffff
        # 5 => filter incoming data on destination IP, mac
        control_new = control_new | 0x00300000
        kernel_wizard.write(
            addr, control_new)
        _log.debug('Control change 0x%08x -> 0x%08x', read_data, control_new)

    async def set_input_multicast_block(self, kernel_wizard, ip, N, offset):
        """
        Set an block of N input multicast addresses
        """
        for i in range(N):
            addr = 0x0 + 0x2004 + i * 4 + offset
            value = ip + i
            kernel_wizard.write(addr, int(value))
            read = kernel_wizard.read(addr)
            _log.debug(" Set input address to Address(es) %i : %s ", i, ipaddress.IPv4Address(read))

    def validate_inputs(self, ips0, ips1, port=None):
        """
        Validate inputs and return values as needed for Alveo input configuration
        """
        ip0, N0, p0 = split_ipstring(ips0)
        ip1, N1, p1 = split_ipstring(ips1)
        if not is_valid_multicast_range(ip0, N0, p0):
            raise RuntimeError("Invalid multicast range {}".format(ips0))
        if not is_valid_multicast_range(ip1, N1, p1):
            raise RuntimeError("Invalid multicast range {}".format(ips1))
        if N0 != N1:
            raise RuntimeError(
                "Multicast range of both interfaces have to match {} {}".format(ips0, ips1))
        if not port and p1:
            port = p1
        ip0 = ipaddress.IPv4Address(ip0)
        ip1 = ipaddress.IPv4Address(ip1)
        return ip0, N0, ip1, N1, port


    async def LUT_mode_setup(self, kernel_wizard, offset):
        """
        Set the 100GbE ports for LUT Farm Mode
        """
        _log.debug('Configuring 100GbE ports for LUT Farm Mode, offset = %i', offset)
        addr = 0x0 + offset + 0x48
        read_data = kernel_wizard.read(addr)
        control_new = read_data & 0xffffffef  # enable UDP checksum
        control_new = read_data | 0x00000020
        kernel_wizard.write(addr, control_new)

    async def set_N_channels(self, packet_size, board_id, number_of_groups, channels_per_group, dest_port):
        """
        Write registers to set the number of channels
        """
        addr = 0x00 + 0x04
        self._client.rtl_kernel_wizard_2_1.write(addr, 0)

        # 3 hex numbers of one byte, defining number of frequency channels in
        # each output MC, number of output MC, and a magic number 8
        x_setup = 0x01080801

        _log.debug("  - Number of groups: %i", number_of_groups)
        _log.debug("  - Channels per group: %i", channels_per_group)
        #x_setup = (channels_per_group << 16) + (number_of_groups << 8) + 8
        _log.debug("  - x_setup: 0x%08x", x_setup)

        self._client.rtl_kernel_wizard_2_1.write(0x0C, x_setup)

        # adjusting for leap mode setup, two FPGA registers need to be set.
        if channels_per_group == 1:
            _log.debug(
                "Setting up for 64 mc mode, setting new x_setup and tx_pktsize_bytes")
            _log.debug("  - tx_pktsize_bytes: %i", packet_size)

            self._client.rtl_kernel_wizard_2_1.write(0x0C, x_setup)
            self._client.rtl_kernel_wizard_2_1.write(0x10, packet_size)

        # tx_meta consists of 24 bits for the port and on byte for the board_id
        # tx_meta = 0xEA6107 :: PORT=EA61 (60001), BOARDID=07
        tx_meta = (dest_port << 16) + (board_id << 8) + board_id
        self._client.rtl_kernel_wizard_2_1.write(0x08, tx_meta)
        _log.debug("  - Board id: 0x%04x", board_id)
        _log.debug("  - tx_meta: 0x%04x", tx_meta)


    async def set_destination_multicast_groups(self, kernel_wizard, number_of_groups, dest_ip, dest_port, offset):
        """
        Write registers for the destination multicast groups
        """
        _log.debug('Configuring 100GbE destination groups, offset = %i', offset)

        mcast_dest_ports = (dest_port << 16) | dest_port
        for i in range(number_of_groups):
            addr = 0x0 + offset + 0x1800 + i * 4
            mac_lo_addr = 0x0 +  offset + 0x1000 + i * 4
            mac_hi_addr = 0x0 +  offset + 0x1400 + i * 4
            port_addr = 0x0 +  offset + 0x1C00 + i * 4

            value = int(ipaddress.IPv4Address(dest_ip)) + i
            mac_lo = (0x5e000000 | (0x00ffffff & int(
                ipaddress.IPv4Address(dest_ip)))) + i
            mac_hi = 0x00000100  # 9a01025e0001 byte-reversed => 01005e02019a

            kernel_wizard.write(mac_lo_addr, mac_lo)
            kernel_wizard.write(mac_hi_addr, mac_hi)
            kernel_wizard.write(addr, value)
            kernel_wizard.write(port_addr, mcast_dest_ports)




class AlveoInterface_PFB_U50(AlveoInterfacePFB):
    """
    Interface to Alveo accelerator cards to run the MPIfR PFB on a U50.
    """

    async def configure(self, mac0, ip0, **kwargs):  # pylint: disable=arguments-differ
        """
        Set mac and IP for the NIC.
        """
        _log.debug('Configure mac %s and ip %s', mac0, ip0)

        await self.connect()
        try:
            self._client.is_loaded()

            await self.set_mac(self._client.rtl_kernel_wizard_0_1, mac0, 0)

            read = await  self.set_ip(self._client.rtl_kernel_wizard_0_1, ip0, 0)
            _log.debug("QSFP28 Core 0 IP: {}".format(ipaddress.IPv4Address(read)))

        except Exception as E:
            _log.error('Error configuring the alveo card %s', ip0)
            _log.exception(E)

    async def unsubscribe_all(self):
        _log.debug("Unsubscribing from inputs")
        # We assume 2x4 inputs anyway
        await self.unsubscribe_igmp(self._client.rtl_kernel_wizard_0_1, 8, 0)


    async def configure_inputs(self, ips0, ips1, port=None):
        """
        Does multicast group subscription. ip0, ip1 are of format
        225.0.0.152+3 to give .152, .153, .154, .155
        """
        _log.debug('Subscribing to multicast groups ...')
        ip0, N0, ip1, N1, port = self.validate_inputs(ips0, ips1, port)

        _log.debug(" - configure pol0 to %s", ip0)
        addr = 0x0 + 0x24
        self._client.rtl_kernel_wizard_0_1.write(addr, int(ip0))

        await self.unsubscribe_all()
        _log.debug(" - configuring the number of multicast groups for pol0 to subscribe to %s ", N0)

        await self.set_input_multicast_block(self._client.rtl_kernel_wizard_0_1, ip0, N0, 0)
        await self.set_input_multicast_block(self._client.rtl_kernel_wizard_0_1, ip1, N1, N0 * 4)

        await self.set_input_port(self._client.rtl_kernel_wizard_0_1, port, 0)

        await self.configure_input_filter_control(self._client.rtl_kernel_wizard_0_1, 0)


        _log.debug("Wait before sending igmp requests...")
        await asyncio.sleep(1)

        read = await self.subscribe_igmp(self._client.rtl_kernel_wizard_0_1, N0 + N1, 0)
        _log.debug("IGMP Control #0 0x%08x", read)


    async def configure_output(self, dest_ip, dest_port, number_of_groups=64, channels_per_group=1, packet_size=8000, board_id=0x08):
        """
        Configure Alveo output
        Args:
            dest_ip
            dest_port
            number_of_groups
            channels_per_group
            board_id            Board ID [0 .. 255] inserted into the SPEAD header
        """
        _log.debug('Configuring output')

        _log.debug('Configuring 100GbE ports for LUT Farm Mode')
        await self.LUT_mode_setup(self._client.rtl_kernel_wizard_0_1, 0)

        _log.debug("  - Staring ip: %s", dest_ip)
        _log.debug("  - Port: %s", dest_port)
        _log.debug("  - Packed ip: %s", ipaddress.IPv4Address(dest_ip))

        await self.set_N_channels(packet_size, board_id, number_of_groups, channels_per_group, dest_port)

        _log.debug('Setting destination multicast addresses...')
        await self.set_destination_multicast_groups(self._client.rtl_kernel_wizard_0_1, number_of_groups, dest_ip, dest_port, 0)




class AlveoInterface_PFB_U280(AlveoInterfacePFB):
    """
    PFB on U280
    """

    async def configure(self, mac0, ip0, mac1, ip1, **kwargs):  # pylint: disable=arguments-differ
        """
        Set mac and IP for the NIC.
        """
        await self.connect()
        try:
            self._client.is_loaded()

            await self.set_mac(self._client.rtl_kernel_wizard_1_1, mac0, 0)

            read = await  self.set_ip(self._client.rtl_kernel_wizard_1_1, ip0, 0)
            _log.debug("QSFP28 Core 0 IP: %s", ipaddress.IPv4Address(read))

            await self.set_mac(self._client.rtl_kernel_wizard_1_1, mac1, 0x4000)
            read = await  self.set_ip(self._client.rtl_kernel_wizard_1_1, ip1, 0x4000)
            _log.debug("QSFP28 Core 1 IP: %s", ipaddress.IPv4Address(read))

        except Exception as E:
            _log.error('Error configuring the alveo card with ips %s, %s', ip0, ip1)
            _log.exception(E)

    async def unsubscribe_all(self):
        _log.debug("Unsubscribing from inputs")
        # We assume 2x4 inputs anyway
        await self.unsubscribe_igmp(self._client.rtl_kernel_wizard_1_1, 4, 0)
        await self.unsubscribe_igmp(self._client.rtl_kernel_wizard_1_1, 4, 0x4000)

    async def configure_inputs(self, ips0, ips1, port=None):
        """
        Does multicast group subscription. ip0, ip1 are of format
        225.0.0.152+3 to give .152, .153, .154, .155
        """
        _log.debug('Subscribing to multicast groups ...')

        ip0, N0, ip1, N1, port = self.validate_inputs(ips0, ips1, port)

        _log.debug(" - configure gbe0 to {} ".format(ip0))
        addr = 0x0 + 0x24
        self._client.rtl_kernel_wizard_1_1.write(addr, int(ip0))

        await self.unsubscribe_all()
        _log.debug(
            " - configuring the number of multicast groups for gbe0 to subscribe to {} ".format(N0))

        await self.set_input_multicast_block(self._client.rtl_kernel_wizard_1_1, ip0, N0, 0)
        await self.set_input_port(self._client.rtl_kernel_wizard_1_1, port, 0)


        _log.debug(" - configure gbe1 to {} ".format(ip1))
        addr = 0x0 + 0x4000 + 0x24
        self._client.rtl_kernel_wizard_1_1.write(addr, int(ip1))

        _log.debug(
            " - configuring the number of multicast groups for gbe1 to subscribe to {} ".format(N1))
        await self.set_input_multicast_block(self._client.rtl_kernel_wizard_1_1, ip1, N1, 0x4000)

        await self.set_input_port(self._client.rtl_kernel_wizard_1_1, port, 0x4000)

        await self.configure_input_filter_control(self._client.rtl_kernel_wizard_1_1, 0)
        await self.configure_input_filter_control(self._client.rtl_kernel_wizard_1_1, 0x4000)


        _log.debug("Wait before sending igmp requests...")
        await asyncio.sleep(1)

        read = await self.subscribe_igmp(self._client.rtl_kernel_wizard_1_1, N0, 0)
        _log.debug("IGMP Control #0 0x%08x", read)

        read = await self.subscribe_igmp(self._client.rtl_kernel_wizard_1_1, N1, 0x4000)
        _log.debug("IGMP Control #1 0x%08x", read)

    async def configure_output(self, dest_ip, dest_port, number_of_groups=64, channels_per_group=1, packet_size=8000, board_id=0x08):
        """
        Configure Alveo output
        Args:
            dest_ip
            dest_port
            number_of_groups
            channels_per_group
            board_id            Board ID [0 .. 255] inserted into the SPEAD header
        """
        _log.debug('Configuring output')

        _log.debug('Configuring 100GbE ports for LUT Farm Mode')
        await self.LUT_mode_setup(self._client.rtl_kernel_wizard_1_1, 0)
        await self.LUT_mode_setup(self._client.rtl_kernel_wizard_1_1, 0x4000)

        _log.debug("  - Staring ip: {}".format(dest_ip))
        _log.debug("  - Port: {}".format(dest_port))

        _log.debug("  - Packed ip: {}".format(ipaddress.IPv4Address(dest_ip)))

        await self.set_N_channels(packet_size, board_id, number_of_groups, channels_per_group, dest_port)

        _log.debug('Setting destination multicast addresses...')
        await self.set_destination_multicast_groups(self._client.rtl_kernel_wizard_1_1, number_of_groups, dest_ip, dest_port, 0)
        await self.set_destination_multicast_groups(self._client.rtl_kernel_wizard_1_1, number_of_groups, dest_ip, dest_port, 0x4000)




__factory["xilinx_u280_xdma_201920_3"] = {'class': AlveoInterface_PFB_U280, 'firmware': 'a_pfb_u280_x64_dual_8b12b.xclbin'}
__factory["xilinx_u55c_gen3x16_xdma_base_3"] = {'class': AlveoInterface_PFB_U280, 'firmware': 'a_pfb_u55c_x64_dual_8b12b.xclbin'}
__factory["xilinx_u50_gen3x16_xdma_201920_3"] = {'class': AlveoInterface_PFB_U50, 'firmware': 'a_pfb_u50_x64_dual_8b12b.xclbin'}



def get_interface(device_number=0):
    """
    Creates an interface class for the specified device
    """
    device = pynq.Device.devices[device_number]
    mName = device.device_info.mName.decode()
    _log.debug('using interface %s for device %i', mName, device_number)
    return __factory[mName]['class'](device_number,
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "../pipelines/alveo_firmwares", __factory[mName]['firmware']))
