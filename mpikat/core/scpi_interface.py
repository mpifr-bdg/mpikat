import json
import signal
import time
import asyncio
import mpikat.core.logger
import mpikat.core.edd_pipeline_aio as EDDPipeline
from mpikat.core.scpi import ScpiAsyncDeviceServer, scpi_request
from mpikat.core.product_controller import ProductController


_log = mpikat.core.logger.getLogger('mpikat.edd_scpi_interface')

MIN_TIME_BETWEEN_PREPARE_AND_START = 3


class EddScpiInterface(ScpiAsyncDeviceServer):
    def __init__(
            self,
            interface,
            port,
            master_ip,
            master_port,
            redis_ip,
            redis_port):
        """
        @brief      A SCPI interface for a EddMasterController instance

        @param      master_controller_ip    IP of a master-controller to send commands to
        @param      master_controller_port  Port of a master-controller to send commands to
        @param      interface    The interface to listen on for SCPI commands
        @param      port        The port to listen on for SCPI commands

        @note If no IOLoop instance is specified the current instance is used.
        """
        _log.info("Listening at {}:{}".format(interface, port))
        _log.info("Master at {}:{}".format(master_ip, master_port))
        _log.info("Datastore at {}:{}".format(redis_ip, redis_port))
        super(EddScpiInterface, self).__init__(interface, port)
        self.__controller = ProductController(
            "MASTER", master_ip, master_port)
        self._t_last_prepare = 0

    ##############################
    # SCPI calls
    ##############################

    @scpi_request()
    async def request_edd_configure(self, req):
        """
        @brief      Configure the EDD backend

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:CONFIGURE'
        """
        async def wrapper():
            await self.__controller.configure()
            await self.__controller.capture_start()
        await self._make_coroutine_wrapper(req, wrapper)()

    @scpi_request()
    async def request_edd_deconfigure(self, req):
        """
        @brief      Deconfigure the EDD backend

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:DECONFIGURE'
        """
        await self._make_coroutine_wrapper(req, self.__controller.deconfigure)()

    @scpi_request()
    async def request_edd_start(self, req):
        """
        @brief      Start the EDD backend processing

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:START'
        """
        try:
            state = await self.wait_for_states(['ready', 'set'], 100)
            if state == "ready":
                _log.info("Measurement prepare missing - sending empty prepare")
                self._t_last_prepare = time.time()
                await self.__controller.measurement_prepare({})
            now = time.time()
            dt = now - self._t_last_prepare
            if dt < MIN_TIME_BETWEEN_PREPARE_AND_START:
                _log.debug(
                    'Delaying start for %.3f s to ensure update of data store', dt)
                await asyncio.sleep(MIN_TIME_BETWEEN_PREPARE_AND_START - dt)
            await self.__controller.measurement_start()
        except Exception as error:
            _log.error(str(error))
            req.error(str(error))
        else:
            req.ok()

    @scpi_request()
    async def request_edd_stop(self, req):
        """
        @brief      Stop the EDD backend processing

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:STOP'
        """
        await self._make_coroutine_wrapper(req, self.__controller.measurement_stop)()

    @scpi_request()
    async def request_edd_abort(self, req):
        """
        @brief      Stop the EDD backend processing

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:ABORT'
        """
        await self._make_coroutine_wrapper(req, self.__controller.measurement_stop)()

    @scpi_request(str, str)
    async def request_edd_set(self, req, product_option, value):
        """
        @brief     Set an option for an EDD backend component.

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:SET ID:OPTION VALUE'
                    VALUE needs to be valid JSON, i.e. strings are marked with ""
        """
        _log.debug(" Received {} {}".format(product_option, value))
        # Create nested option dict from colon separated string
        try:
            d = {}
            g = d
            option_list = product_option.split(':')
            for el in option_list[:-1]:
                d[el] = {}
                d = d[el]
            d[option_list[-1]] = json.loads(value)
        except Exception as E:
            em = "Error parsing command: {} {}\n{}".format(
                product_option, value, E)
            _log.error(em)
            req.error(em)
        else:
            _log.debug(" - {}".format(g))
            await self._make_coroutine_wrapper(req, self.__controller.set, g)()

    @scpi_request(str)
    async def request_edd_provision(self, req, message):
        """
        @brief      Provision the EDD backend

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:PROVISON'
        """
        await self._make_coroutine_wrapper(req, self.__controller.provision, message)()

    @scpi_request()
    async def request_edd_deprovision(self, req):
        """
        @brief      Deprovision the EDD backend

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:DEPROVISON'
        """
        await self._make_coroutine_wrapper(req, self.__controller.deprovision)()

    @scpi_request(str)
    async def request_edd_measurementprepare(self, req, message):
        """
        @brief      Send measurement prepare to the EDD backend

        @param      req   An ScpiRequst object

        @note       Supports SCPI request: 'EDD:MEASUREMENTPREPARE'

        @detail Sends a raw measurement prepare JSON to the master controller.
                Note that \\ and " have to be escaped. To send a JSON dict with
                two strings {"foo":"bar"} you thus have to send the command
                EDD:MEASUREMENTPREPARE {\\\"foo\\\":\\\"bar\\\"}'
        """  # pylint: disable=anomalous-backslash-in-string
        _log.debug("Sending measurement prepare: {}".format(message))
        try:
            cfg = json.loads(message)
        except json.JSONDecodeError as E:
            em = "Not valid JSON!:\n {}. {}".format(message, E)
            _log.error(em)
            req.error(em)
            return

        try:
            await self.wait_for_states(['ready'], 100)
        except TimeoutError:
            req.error('State "ready" not reached after 100 sec!')
        else:
            self._t_last_prepare = time.time()
            await self._make_coroutine_wrapper(
                req, self.__controller.measurement_prepare, cfg)()

    async def wait_for_states(self, states, timeout):
        """
        Waits until master controller reaches one of the given states state
        """
        starttime = time.time()
        while True:
            try:
                mc_state = await self.__controller.getState()
            except Exception as E:
                _log.error("Error getting state!")
                raise E
            _log.debug("Current master controller state: %s", mc_state)
            if mc_state in states:
                break
            if time.time() - starttime > timeout:
                raise TimeoutError("Waiting for ready state timed out")
            await asyncio.sleep(0.1)
        return mc_state


async def main(args):
    server = EddScpiInterface(
        args.host, args.port, args.master_ip,
        args.master_port, args.redis_ip,
        args.redis_port)
    await server.start()

if __name__ == "__main__":

    parser = EDDPipeline.getArgumentParser()
    parser.add_argument(
        '--master-controller-ip',
        dest='master_ip',
        type=str,
        default="edd01",
        help='The ip for the master controller')
    parser.add_argument(
        '--master-controller-port',
        dest='master_port',
        type=int,
        default=7147,
        help='The port number for the master controller')
    args = parser.parse_args()
    mpikat.core.logger.setLevel(args.log_level.upper())
    loop = asyncio.get_event_loop()
    loop.create_task(main(args))
    loop.run_forever()
