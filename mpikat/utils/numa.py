"""
Get information on NUMA architecture.
"""
import os
import socket
import fcntl
import struct
import ipaddress
import sys

from mpikat.core import logger

_log = logger.getLogger("mpikat.utils.numa")

try:
    import pynvml
    pynvml.nvmlInit()
    __haspynvml = True
except Exception:
    _log.warning("pynvml not available. Cannot use GPUs (if any)")
    __haspynvml = False

__numaInfo = None


def expandlistrange(lr: str) -> set:
    """
    Expands a string containing a list of numbers '1,2,3-5' to an actual list [1,2,3,4,5]
    """
    output = set()
    if not lr.isspace():
        for ir in lr.split(','):
            try:
                il = ir.split('-')
                output.update([str(n) for n in range(int(il[0]), int(il[-1]) + 1)])
            except Exception as E:
                print("lr: {}".format(lr))
                print("ir: {}".format(ir))
                print("il: {}".format(il))
                raise E
    return output


def get_valid_numa_nodes() -> list:
    """
    Return list of valid NUMA nodes.
    If the environment variable ALLOWED_NUMA_NODES is specified, the list is limited to the intersection of allowed and existing nodes
    """
    with open("/sys/devices/system/node/possible", 'r') as f:
        nodes = f.read().strip().split('-')
    nodes = [str(n) for n in range(int(nodes[0]), int(nodes[-1]) + 1)]
    if os.getenv('EDD_ALLOWED_NUMA_NODES'):
        _log.debug("Restricting numa nodes to nodes listed in EDD_ALLOWED_NUMA_NODES")
        allowed_nodes = expandlistrange(os.getenv('EDD_ALLOWED_NUMA_NODES'))
        for noderange in os.getenv('EDD_ALLOWED_NUMA_NODES').split(','):
            noderange = noderange.split('-')
            allowed_nodes.update([str(n) for n in range(int(noderange[0]), int(noderange[-1]) + 1)])
        for node in allowed_nodes.difference(nodes):
            _log.warning("Node {} in EDD_ALLOWED_NUMA_NODES, but not available on host!".format(node))
        allowed_nodes.intersection_update(nodes)
        nodes = list(allowed_nodes)
    return nodes

def read_numa_from_path(path):
    """
    Read numa node from sys path. Assume 0 if not specified 
    """
    if os.path.isfile(path):
        with open(path, 'r') as f:
            node = f.read().strip()
            if int(node) < 0:
                # This is the same assumption as in hwloc
                _log.warning('Device not associated to node by bios/driver - assuming node 0')
                node = '0'
    else:
        _log.warning('Numa info not available for device - assuming node 0')
        node = '0'
    return node





def updateInfo():
    """
    Updates the info dictionary.
    """
    _log.debug("Update numa dictionary")
    global __numaInfo
    __numaInfo = {}

    nodes = get_valid_numa_nodes()
    with open('/sys/devices/system/cpu/isolated', 'r') as f:
        isolated_cpus = expandlistrange(f.read())

    for node in nodes:
        _log.debug("Preparing node {} of {}".format(node, len(nodes)))
        __numaInfo[node] = {"net_devices":{}}

        with open('/sys/devices/system/node/node' + node + '/cpulist', 'r') as f:
            cpulist = expandlistrange(f.read())

        __numaInfo[node]['cores'] = list(cpulist.difference(isolated_cpus))
        __numaInfo[node]['cores'].sort(key=lambda x: int(x))
        __numaInfo[node]['isolated_cores'] = list(isolated_cpus.intersection(cpulist))
        __numaInfo[node]['isolated_cores'].sort(key=lambda x: int(x))

        __numaInfo[node]['gpus'] = []
        __numaInfo[node]["net_devices"] = {}
        _log.debug("  found {} Cores.".format(len(__numaInfo[node]['cores'])))
    _log.debug("Available nodes: {}".format(__numaInfo.keys()))

    #_log.debug(__numaInfo)
    # check network devices
    for device in os.listdir("/sys/class/net/"):
        _log.debug("Associate network device %s to node", device)
        node = read_numa_from_path("/sys/class/net/" + device + "/device/numa_node")
        if node not in __numaInfo:
            _log.debug("Device on node %s, but node not in list of nodes. Possible node was deacitvated.", node)
            continue

        __numaInfo[node]["net_devices"][device] = {}
        _log.debug("  - found node {}".format(node))
        __numaInfo[node]["net_devices"][device]['ip'] = ""
        try:

            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                ip = socket.inet_ntoa(fcntl.ioctl(sock.fileno(),
                                              0x8915,  # SIOCGIFADDR
                                              struct.pack('256s', device[:15].encode('ascii')))[20:24])
            __numaInfo[node]["net_devices"][device]['ip'] = ip
        except IOError:
            _log.warning(" Cannot associate device %s to a node: %s", device, node)
        d = "/sys/class/net/" + device + "/speed"
        speed = 0
        if os.path.isfile(d):
            try:
                with open(d, 'r') as f:
                    speed = f.read()
            except Exception as E:
                _log.warning(" Cannot acess speed for device %s: %s", device, node)
                _log.exception(E)

        __numaInfo[node]["net_devices"][device]['speed'] = int(speed)
    # check CUDA devices:
    if __haspynvml:

        nGpus = pynvml.nvmlDeviceGetCount()
        _log.debug("Found %i GPUs", nGpus)

        for i in range(nGpus):
            handle = pynvml.nvmlDeviceGetHandleByIndex(i)
            pciInfo = pynvml.nvmlDeviceGetPciInfo(handle)

            dl = '/sys/bus/pci/devices/' + pciInfo.busId.lower()[4:] + "/numa_node"
            du = '/sys/bus/pci/devices/' + pciInfo.busId.upper()[4:] + "/numa_node"

            _log.debug('Checking paths %s %s', du, dl)
            if os.path.isfile(du):
                node = read_numa_from_path(du)
            else:
                node = read_numa_from_path(dl)

            if node not in __numaInfo:
                _log.debug("Device on node {}, but node not in list of nodes. Possible node was deactivated.".format(node))
                continue
            __numaInfo[node]['gpus'].append(str(i))


def getInfo():
    """
    Returns dict with info on NUMA configuration. For every NUMA node the dict
    contains a dict with the associated resources.
    """
    global __numaInfo
    if not __numaInfo:
        updateInfo()
    return __numaInfo


def getFastestNic(numa_node=None, restrict_network=True):
    """
    Returns (name, description) of the fastest NIC (on given numa_node).

    If restrict network is set to True a network provided via the environment
    variable EDD_ALLOWED_NETWORK will be used to restrict the selection a a
    specific subnet.
    """
    if numa_node is not None:
        all_nics = getInfo()[numa_node]["net_devices"]

        edd_allowed_network = ipaddress.ip_network("0.0.0.0/0")
        if restrict_network:
            v = os.getenv('EDD_ALLOWED_NETWORK')
            if v:
                edd_allowed_network = ipaddress.ip_network(v)
        _log.debug("Restrict network to: {}".format(edd_allowed_network))
        nics = {}
        _log.debug("Checking ips in allowed network")
        for n, i in all_nics.items():
            try:
                if ipaddress.IPv4Address(i['ip']) in edd_allowed_network:
                    _log.debug("%s in %s", i['ip'], edd_allowed_network)
                    nics[n] = i
                else:
                    _log.debug("%s not in %s", i['ip'], edd_allowed_network)
            except ipaddress.AddressValueError:
                _log.debug("Not an ip %s", i['ip'])

        if nics:
            fastest_nic = max(nics.keys(), key=lambda k: nics[k]['speed'])
            _log.debug("Returning %s, %s", fastest_nic, nics[fastest_nic])
            return fastest_nic, nics[fastest_nic]
        _log.warning("Using dummy nic - are you in debug mode?")
        return "dummy_nic", {"ip": "127.0.0.1", "speed": 1.2345}
    else:
        f = None
        d = None
        for node in getInfo():
            fn, fnd = getFastestNic(node)
            if f is not None:
                print(fn, fnd)
                if fnd['speed'] < d['speed']: # pylint: disable=unsubscriptable-object
                    continue
            f = fn
            d = fnd
            d['node'] = node
        if not f:
            _log.warning("Using dummy nic - are you in debug mode?")
            return "dummy_nic", {"ip": "127.0.0.1", "speed": 1.2345}
        _log.debug("Returning %s, %s", f, d)
        return f, d


def getCudaDeviceHighestComputeArch(node_id: int) -> int:
    """Queries the system's numa node for the GPU with the highest
    CUDA compute architecture.

    Args:
        node_id (int): The numa node ID

    Returns:
        int: The GPU id
    """
    dev_id = 0
    prev = 0
    desc = getInfo()[str(node_id)]
    for device in desc['gpus']:
        major, __ = pynvml.nvmlDeviceGetCudaComputeCapability(
            pynvml.nvmlDeviceGetHandleByIndex(int(device)))
        if major > prev:
            prev = major
            dev_id = device
    return dev_id

if __name__ == "__main__":
    print(getInfo())
    for node, res in getInfo().items():
        print("NUMA Node: {}".format(node))
        print("  CPU Cores: {}".format(", ".join(res['cores'])))
        print("  Isolated CPU Cores: {}".format(", ".join(res['isolated_cores'])))
        print("  GPUs: {}".format(", ".join(map(str, res['gpus']))))
        print("  Network interfaces:")
        for nic, info in res['net_devices'].items():
            print("     {nic}: ip = {ip}, speed = {speed} Mbit/s ".format(nic=nic, **info))

        nics = res['net_devices']
        if nics:
            fastest_nic = max(nics.keys(), key=lambda k: nics[k]['speed'])
            print('   -> Fastest interface: {}'.format(fastest_nic))

    print("Fastest nic over all: {}".format(getFastestNic()))
