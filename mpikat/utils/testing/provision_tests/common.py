import enum
import dataclasses

import aiokatcp

from mpikat.core import logger

_log = logger.getLogger('mpikat.utils.testing.provision_tests.common')


class Stage(enum.Enum):
    """
    Test stages. Every stage corresponds to a command sent to the
    master_controller. Tests corresponding to a stage are executed after the
    master controller has finished successfully.
    """
    provision = enum.auto()
    configure = enum.auto()
    capture_start = enum.auto()
    measurement_prepare = enum.auto()
    measurement_start = enum.auto()
    measurement_stop = enum.auto()
    # capture_stop = enum.auto()
    deconfigure = enum.auto()
    deprovision = enum.auto()


@dataclasses.dataclass(eq=True, unsafe_hash=True)
class EDDTest():
    """
    A single test.
    Args:
        stage: Stage in which the test will be scheduled.
        pipeline_name: Name of the pipeline under test.
        func: Test function to be executed
        name: Name of the test. If no name is provided, name of the function is used
        tbc: True if the test needs manual confirmation
    """
    stage: Stage
    func: callable
    pipeline_name: str = ""
    class_name: str = ""
    name: str = ""
    tbc: bool = False

    def __post_init__(self):
        if not self.name:
            self.name = self.func.__name__

        if not (self.pipeline_name or self.class_name):
            raise RuntimeError('Test requires either pipeline_name or class_name')


class TestState(enum.Enum):
    NOT_EXECUTED = enum.auto()
    OK = enum.auto()
    FAIL = enum.auto()
    ERROR = enum.auto()
    TBC = enum.auto()



@dataclasses.dataclass
class EDDTestResult:
    """
    A test result:

    Attributes:
        name:     Name of the test
        pipeline_name:     name of the pipeline
        output:   List of outputs, e.g. plots produced by the test
        state:  State of the test
        duration: Duration of the test in seconds.
    """
    name: str
    pipeline_name: str
    output: list = dataclasses.field(default_factory=lambda: [])
    state: TestState = TestState.NOT_EXECUTED
    duration: float = 0.


@dataclasses.dataclass
class EDDTestContext:
    """
    Context passed to the test function.

    output: List of outputs added by the test function
    pipeline. katcp client connection to the pipeline
    master_controller. katcp connection to the master controller
    """
    output: list = dataclasses.field(default_factory=lambda: [])
    master_controller: aiokatcp.Client = dataclasses.field(default_factory=lambda: None)
    pipeline: aiokatcp.Client = dataclasses.field(default_factory=lambda: [])
    status_server: aiokatcp.Client = dataclasses.field(default_factory=lambda: [])
    stage_response: dict = dataclasses.field(default_factory=lambda: [])

state_targets = {
        Stage.provision: ["idle"],
        Stage.configure: ['configured'],
        Stage.capture_start: ['ready', 'streaming'],
        Stage.measurement_prepare: ['set', 'streaming'],
        Stage.measurement_start: ['measuring', 'streaming'],
        Stage.measurement_stop: ['ready', 'streaming'],
        Stage.deconfigure: ['idle']
}


@dataclasses.dataclass
class StageSummary:
    """
    Summary results for a stage
    """
    stage: Stage
    n_tests: int = 0
    duration: float = 0
    results: dict = dataclasses.field(default_factory=lambda: {})
    pipelines: list = dataclasses.field(default_factory=lambda: [])
    stats: dict = dataclasses.field(default_factory=lambda: {t: 0 for t in TestState})

    def add(self, test_result):
        _log.debug(f'Adding test result {test_result} to existing set of {self.n_tests} results.')
        self.n_tests += 1
        _log.debug(f'Now set contains {self.n_tests} results.')
        self.stats[test_result.state] += 1
        if not test_result.pipeline_name in self.results:
            self.results[test_result.pipeline_name] = []

        self.results[test_result.pipeline_name].append(test_result)

