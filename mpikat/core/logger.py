"""
Unified logging initialization for mpikat 

- Use .getLogger(name) to obtain a logging logger
- Use environment variable LOG_LEVEL to set log LEVEL for all mpikat logger
  (defaults to INFO)
- Additional level logging.TRACE is created with corresponding
  logger.trace('Foo') for very verbose logging of repetitive function calls.
"""
import os
import logging as lg

import coloredlogs

# Add trace log level
# https://stackoverflow.com/questions/2183233/how-to-add-a-custom-loglevel-to-pythons-logging-facility
lg.TRACE = lg.DEBUG - 5
lg.addLevelName(lg.TRACE, "TRACE")


def trace(self, message, *args, **kws):
    """
    Log at very verbose level
    """
    if self.isEnabledFor(lg.TRACE):
        # Yes, logger takes its '*args' as 'args'.
        self._log(lg.TRACE, message, args, **kws)


lg.Logger.trace = trace

# Default configuration for logger
lg.getLogger().addHandler(lg.NullHandler())
logger = lg.getLogger("mpikat")
coloredlogs.install(
    fmt=("[ %(levelname)s - %(asctime)s - %(name)s "
         "- %(filename)s:%(lineno)s] %(message)s"),
    datefmt="%Y-%m-%dT%H:%M:%S%z",
    level=1,            # We manage the log level via the logger, not the handler
    logger=logger)

env_level = os.getenv("LOG_LEVEL")
if env_level:
    logger.setLevel(env_level.upper())
else:
    logger.setLevel("INFO")


def setLevel(level):
    """
    Set log level. Accepts  logging levels.
    """
    global logger
    logger.setLevel(level)


def getLogger(*args, **kwargs):
    """
    Return logger
    """
    log = lg.getLogger(*args, **kwargs)
    return log


def getLevelName():
    """
    Get name from level
    """
    global logger
    return lg.getLevelName(logger.level)
