import unittest
import time
from mpikat.utils.process_io import ProcessIO

def dummy_callable(item: any):
    return item
def dummy_slow_callable(item: any):
    time.sleep(1)
    return item

class Test_ProcessIO(unittest.TestCase):

    def setUp(self) -> None:
        self.tobj = ProcessIO(dummy_callable)
        return super().setUp()

    def tearDown(self) -> None:
        if self.tobj.is_alive():
            self.tobj.terminate()
            self.tobj.join()
        return super().tearDown()

    def test_add_not_alive(self):
        """Tests that no item is added when the process is not running
        """
        self.tobj.add("I'm a dummy item")
        self.assertEqual(self.tobj.isize, 0)

    def test_bad_callable(self):
        """
        """
        item = "I'm a dummy item"
        self.tobj._callback = None
        self.tobj.start()
        self.tobj.add(item)
        self.tobj.join()
        self.assertEqual(self.tobj.exitcode, 1)

    def test_is_stoppable(self):
        """
        """
        self.tobj.start()
        self.assertTrue(self.tobj.is_alive())
        self.tobj.stop()
        self.assertFalse(self.tobj.is_alive())

    def test_add_get_items(self):
        """
        """
        self.tobj.start()
        for __ in range(self.tobj._iqueue_size):
            item = time.time()
            self.tobj.add(item)
            self.assertEqual(self.tobj.get(), item)

    def test_flush_queue_when_to_slow(self):
        """
        """
        self.tobj._callback = dummy_slow_callable
        self.tobj.start()
        for __ in range(self.tobj._iqueue_size+2):
            item = time.time()
            self.tobj.add(item)

        start = time.time()
        while time.time() - start <= 2:
            if not self.tobj.is_alive() or self.tobj.isize == 0:
                break
            time.sleep(.1)
        else:
            raise TimeoutError('Job still running after 2 sec')

        self.assertEqual(self.tobj.isize, 0)

    def test_add_get_queue_full(self):
        """
        """
        self.tobj._oqueue_size = 16
        self.tobj.start()
        for i in range(17):
            self.tobj.add(i)
        while self.tobj.isize > 0:
            time.sleep(0.01)
        self.assertEqual(self.tobj.osize, self.tobj._oqueue_size)
        for i in range(self.tobj.osize):
            self.assertEqual(self.tobj.get(), i)
        self.assertEqual(self.tobj.osize, 0)

if __name__ == "__main__":
    unittest.main()
