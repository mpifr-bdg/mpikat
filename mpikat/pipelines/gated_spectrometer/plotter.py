import time
import numpy as np
import asyncio
from datetime import datetime as dt
from typing import List
from aiokatcp.sensor import Sensor

from mpikat.utils.spead_capture import SpeadPacket
from mpikat.utils.process_io import ProcessIO
import mpikat.core.logger


import matplotlib as mpl
mpl.use('Agg')
import pylab as plt
import io
import base64
mpl.rcParams.update(mpl.rcParamsDefault)
mpl.use('Agg')

_log = mpikat.core.logger.getLogger("mpikat.pipelines.gated_spectrometer.plotter")


def encodeb64_figure(fig: plt.figure) -> bytes:
    """Encodes the PNG of a plt.figure to base64 bytes

    Args:
        fig (plt.figure): the figure to encode

    Returns:
        bytes: the base 64 encoded bytes
    """
    fig_buffer = io.BytesIO()
    fig.savefig(fig_buffer, format='png')
    fig_buffer.seek(0)
    b64 = base64.b64encode(fig_buffer.read())
    plt.close(fig)
    return b64


def section_id(heap: SpeadPacket) -> str:
    """Constructs an ID for the corresponding SpeadPacket

    Args:
        heap (SpeadPacket): The SpeadPacket

    Returns:
        str: The section ID of the packet
    """
    prefix = {0: "P", 1: "S"}
    return f"{prefix[heap.type]}{heap.polarization}_ND{heap.noise_diode_status}"


class Plotter:
    def __init__(self, interval: int=10, channels: int=1024):
        """The Plotter class expects SpeadPackets of the GatedSpectrometerSpeadFormat.
        The Plotter creats an ProcessIO object providing in- and output queues. The SpeadPackets
        are put into the input queue and the Plotter processes the packets to plots in a
        mp.Process. If a plot is processed it gets put into the output queue.

        Args:
            interval (int, optional): The update interval. Defaults to 10.
            channels (int, optional): The number of channels in the plots. Defaults to 1024.
        """
        self._interval: int = interval
        self._channels: int = channels
        self._data: dict = {}
        self._ioproc: ProcessIO = ProcessIO(self, 1024, 2)
        self._start: int = time.time()

    def addSnapshot(self, item: SpeadPacket):
        """Adds a SpeadPacket to the input queue

        Args:
            item (SpeadPacket): the packet to put into the queue
        """
        if not self._ioproc.is_alive():
            _log.warning("Can not add snapshot when ProcessIO is not alive"); return
        self._ioproc.add(item)

    def getPlot(self) -> bytes:
        """Returns the first available item/plot from output queue

        Returns:
            bytes: The b64-encoded PNG plot, if no plot is avaiable it return None
        """
        if self._ioproc.oqueue.empty():
            return None
        return self._ioproc.oqueue.get()

    async def consume_plots(self, sensor: Sensor):
        """Asynchronous function consuming plots from the output queue.
        This function provides an async interface which assignes the plot
        to an aiokatcp.Sensor.

        Args:
            sensor (Sensor): An aiokatcp.Sensor which consumes the plots
        """
        self.start()
        try:
            while True:
                res = self.getPlot()
                if res is None:
                    await asyncio.sleep(1)
                else:
                    sensor.set_value(res)
        except asyncio.CancelledError:
            self.stop()
            _log.info("Plotting task was stopped")

    def __call__(self, item: SpeadPacket) -> bytes:
        """Callback function - usually called in a mp.Process.


        Args:
            item(SpeadPacket): The packet to attach

        Returns:
            bytes: A b64 encoded PNG image if ready, otherwise None
        """
        sid = section_id(item)
        if sid not in self._data:
            self._data[sid] = [item]
        else:
            self._data[sid].append(item)
        # If data collection is not finished, None is returned
        if time.time() - self._interval < self._start:
            return None
        self._start = time.time()
        _log.debug("Plotting data")
        res = self.plot()
        self._data = {}
        return res

    @property
    def plotting(self) -> bool:
        """Property, returns the alive state of the underlying mp.Process

        Returns:
            bool: returns True if the Plotter is plotting otherwise false.
        """
        if self._ioproc is None:
            return False
        return self._ioproc.is_alive()

    def start(self):
        """Starts the underlying mp.Process
        """
        if self._ioproc is None:
            self._ioproc = ProcessIO(self, 1024, 2)
        if self._ioproc.is_alive():
            _log.warning("Tried to start process which is already started")
            return
        self._ioproc.start()

    def stop(self):
        """Stops the underlying mp.Process
        """
        try:
            self._ioproc.stop(timeout=5)
        except Exception:
            _log.warning("Couldn't stop IO process, terminating now...")
            self._ioproc.terminate()
        self._ioproc = None

    def plot(self) -> bytes:
        """Plots a spectrum and encodes it to base 64

        Returns:
            bytes: base64 encoded byte-string containing the plot
        """
        pols = set([int(key[1]) for key in self._data.keys()])
        figsize = {2: (8, 4), 4: (8, 8)}
        labels = {2: ["PSD [dB]", 'PSD [dB]'], 4: ["S0 [dB]", "S1/S0", "S2/S0", "S3/S0"]}
        # Construct figure with subplots
        try:
            fig = plt.figure(len(pols), figsize=figsize[len(pols)])
        except:
            _log.warning("Not able to construct figure with %d plots", len(pols))
            return ""
        subplots: List[plt.Axes] = []
        for pol in pols:
            subplots.append(fig.add_subplot(len(pols)//2, 2, pol+1))
        self._data = dict(sorted(self._data.items()))
        # Iterate the data, process and plot it.
        acc = np.zeros((len(pols), 2, self._channels))
        for packet_list in self._data.values():
            tot_time = 0
            for packet in packet_list:
                nd = packet.noise_diode_status
                pol = packet.polarization
                spectrum = packet.data[1:].reshape([self._channels, (packet.data.size -1) // self._channels]).sum(axis=1)
                if np.isfinite(spectrum).all():
                    acc[pol, nd] += spectrum
                    tot_time += packet.integration_period
            label = f"POL: {pol} ND: {nd} (T = {tot_time:.2f} s)"
            if len(pols) == 4 and packet.polarization != 0:
                subplots[packet.polarization].plot(acc[pol, nd] / acc[0, nd] / tot_time, label=label)
            else:
                subplots[packet.polarization].plot(10. * np.log10(acc[pol, nd] / tot_time), label=label)
            subplots[packet.polarization].legend()
            subplots[packet.polarization].set_xlabel('Channel')
            subplots[packet.polarization].set_ylabel(f"{labels[len(pols)][pol]}")
        timestamp = packet.reference_time # last packet sets the timestamp
        fig.suptitle(f"{dt.fromtimestamp(timestamp).isoformat()}")
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        return encodeb64_figure(fig)
