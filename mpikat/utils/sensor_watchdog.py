import threading
import time
import aiokatcp.sensor

class SensorWatchdog(threading.Thread):
    """
    Watchdog thread that checks if the execution stalls without getting
    noticed.  If time between changes of the value of a sensor surpasses  the
    timeout value, the callback function is called.

    Usage example:
        wd = SensorWatchdog(self._polarization_sensors[k]["input-buffer-total-write"],
                10 * self._integration_time_status.value(),
                self.watchdog_error)

        # The input buffer should not silently  stall (was? an mkrecv problem).

    """
    def __init__(self, sensor, timeout, callback):
        threading.Thread.__init__(self)
        self.__timeout = timeout
        self.__sensor = sensor
        self.__callback = callback
        self.stop_event = threading.Event()

    def run(self):
        while not self.stop_event.wait(timeout=self.__timeout):

            if isinstance(self.__sensor, aiokatcp.sensor.Sensor):
                timestamp = self.__sensor.timestamp # read timestamp of aiokatcp sensor

            if (time.time() - timestamp) > self.__timeout:
                self.__callback()
                self.stop_event.set()



def conditional_update(sensor, value, status=None, timestamp=None):
    """
    Update a sensor if the value has changed
    """
    if sensor.value != value:
        if status is None:
            status = aiokatcp.sensor.Sensor.Status.NOMINAL
        sensor.set_value(value, status, timestamp=timestamp)
