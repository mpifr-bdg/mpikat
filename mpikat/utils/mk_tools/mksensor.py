import time
import json
import numpy as np

from aiokatcp.sensor import Sensor

import mpikat.core.logger

_log = mpikat.core.logger.getLogger('mpikat.mkrecv_stdout_parser')

MKRECV_STDOUT_KEYS = {"STAT": [("slot-nr", int), ("slot-nheaps", int), ("stc", int),
                               ("heaps-completed", int),
                               ("heaps-discarded", int), ("heaps-open", int),
                               ("payload-expected", int), ("payload-received", int),
                               ("global-heaps-completed", int),
                               ("global-heaps-discarded", int), ("global-heaps-open", int),
                               ("global-payload-expected", int), ("global-payload-received", int)],
                      "MCAST": [("group-idx", int), ("missing-heaps", int)],
                      "Epoch": [('dummy', str), ('epoch_difference', float)]
                     }

def mkrecv_stdout_parser(line):
    """
    Parser for lines of mkrecv output
    """

    _log.debug(line)

    line = line.replace('slot', '')
    line = line.replace('total', '')

    if line.split(" ")[0] == 'Epoch':
        _log.info(line)

    tokens = line.split()
    params = {}
    if tokens and (tokens[0] in MKRECV_STDOUT_KEYS):
        params = {}
        parser = MKRECV_STDOUT_KEYS[tokens[0]]
        for ii, (key, dtype) in enumerate(parser):
            params[key] = dtype(tokens[ii+1])
    else:
        _log.info(line)
    _log.debug("MKrecv params: %s", json.dumps(params, indent=4))
    return params


class MkrecvSensors:
    """
    List of sensors and handler to be connected to a mkrecv process
    """
    def __init__(self, name_suffix, nsum=100):
        """
        Args:
            name_suffix: Suffix appended to the name of the sensors to indicate differnent input streams
            nsum: Number of lines to sum over for received heaps fraction
        """
        self.sensors = {}

        self.sensors['global_payload_frac'] = Sensor(
            float, "global-payload-received-fraction-{}".format(name_suffix),
            description="Ratio of received and expected payload.",
            initial_status=Sensor.Status.UNKNOWN,
        )
        self.sensors['received_heaps_frac'] = Sensor(
            float, "received-heaps-frac-{}".format(name_suffix),
            description="Fraction of received heaps for last {} slots.".format(100),
            initial_status=Sensor.Status.UNKNOWN,
        )
        self.sensors['slot_received_heaps'] = Sensor(
            int, "slot_received-heaps-{}".format(name_suffix),
            description="Received heaps."
        )
        self.sensors['slot_expected_heaps'] = Sensor(
            int, "slot_expected-heaps-{}".format(name_suffix),
            description="Expected heaps.",
            initial_status=Sensor.Status.UNKNOWN,
        )
        self.sensors['missing_heaps'] = Sensor(
            str, "missing-heaps-{}".format(name_suffix),
            description="Missing heaps per multicast group.",
            initial_status=Sensor.Status.UNKNOWN,
        )
        self.sensors['epoch_difference'] = Sensor(
            str, "epoch_difference-{}".format(name_suffix),
            description="Difference of first heap to epoch.",
            initial_status=Sensor.Status.UNKNOWN,
        )

        self.sensors['global_completed_heaps'] = Sensor(int,
            "global-completed-heaps",
            description="Total received heaps",
            initial_status=Sensor.Status.UNKNOWN)

        self.sensors['global_missing_heaps'] = Sensor(int,
            "global-missing-heaps",
            description="Total missed heaps.",
            initial_status=Sensor.Status.UNKNOWN)

        self.sensors['global_expected_heaps'] = Sensor(int,
            "global-expected-heaps",
            description="Total expected heaps.",
            initial_status=Sensor.Status.UNKNOWN)

        self.__received_heaps = 0.
        self.__expected_heaps = 0.
        self.__total_expected_heaps = 0
        self.__idx = 0
        self.__nsum = nsum

        self.__missing_heaps = np.zeros(8)

    def stdout_handler(self, line: str):
        """
        Handle stdout output of mkrecv

        Args:
            line: line of mkrecv output
        """
        data = mkrecv_stdout_parser(line)
        #ToDo: Use best time in system here from synctime + number of samples
        timestamp = time.time()
        self.update_sensors(data, timestamp)

    def update_sensors(self, data: dict, timestamp: float):
        """
        Updates the sensors
        """

        if "global-payload-received" in data:
            try:
                self.sensors["global_payload_frac"].set_value(float(data["global-payload-received"]) / float(data["global-payload-expected"]), timestamp=timestamp)
            except ZeroDivisionError:
                _log.error("Zero division in sensor update. Received line:\n %s", str(data))

        if "heaps-completed" in data and "slot-nheaps" in data:
            self.__idx += 1
            self.__received_heaps += data["heaps-completed"]
            self.__expected_heaps += data["slot-nheaps"]
            self.__total_expected_heaps = data["slot-nheaps"] * data["slot-nr"]
            if self.__idx >= self.__nsum:
                self.sensors["received_heaps_frac"].set_value(self.__received_heaps / float(self.__expected_heaps + 1E-128), timestamp=timestamp)
                self.__idx = 0
                self.__received_heaps = 0.
                self.__expected_heaps = 0.

            self.sensors['slot_expected_heaps'].set_value(data["slot-nheaps"], timestamp=timestamp)
            self.sensors['slot_received_heaps'].set_value(data["heaps-completed"], timestamp=timestamp)
            self.sensors['missing_heaps'].set_value(np.array2string(self.__missing_heaps), timestamp=timestamp)
            self.sensors['global_expected_heaps'].set_value(self.__total_expected_heaps, timestamp=timestamp)


            # Zero out missing heaps data. Missing heaps is reported before
            # stat line!
            self.__missing_heaps -= self.__missing_heaps

        if 'global-heaps-completed' and 'global-heaps-discarded' in data:
            self.sensors['global_completed_heaps'].set_value(data['global-heaps-completed'], timestamp=timestamp)
            self.sensors['global_missing_heaps'].set_value(data['global-heaps-discarded'], timestamp=timestamp)

        if "group-idx" in data:
            if data['group-idx'] >= self.__missing_heaps.size:
                # Resize for next round
                self.__missing_heaps = np.zeros(data['group-idx'] + 1)
            else:
                self.__missing_heaps[data['group-idx']] = data['missing-heaps']

        if 'epoch_difference' in data:
            self.sensors['epoch_difference'].set_value(data['epoch_difference'])
