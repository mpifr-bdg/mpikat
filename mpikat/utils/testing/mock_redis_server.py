import socket
import redislite
from mpikat.core import logger
_log = logger.getLogger("mpikat.testEddPipeline")


def setup_redis(host: str="") -> redislite.Redis:
    """
    Set up a local Redis mock-server
    """

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, 0))
        addr = s.getsockname()
        port = addr[1]
        _log.debug('Creatting temporary redis server listening at port: %i', port)
    return redislite.Redis(serverconfig={'port': port})
