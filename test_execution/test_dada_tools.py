import unittest
import asyncio
import string
import math
import os
import numpy as np

import mpikat.utils.dada_tools as dada


class TEST_DadaKeyGeneration(unittest.IsolatedAsyncioTestCase):

    def test_hex_chars(self):
        k = dada.get_key()
        self.assertTrue(all([1 if l in string.hexdigits else 0 for l in k]))

    def test_key_generation(self):
        # test False when no keys are available
        self.assertFalse(dada.get_key(width=0, exclude={'dada'}))
        # test step width ('dada' still occupied)
        self.assertEqual('dade', dada.get_key(step=4, exclude={'dada'}))
        # test step width but also if exclude feature work with hex string
        self.assertEqual('daea', dada.get_key(step=16,exclude={'dada'}))
        # Test that the next free key is used (step=2)
        self.assertEqual('dae0', dada.get_key(step=2, exclude={'dada','dadc','dade'}))
        # Test correct start of key generation
        self.assertEqual('aaaa', dada.get_key(start='aaaa'))

    async def test_free_key(self):
        # Test with assigning the occupation list
        key = dada.get_key()
        self.assertTrue(dada.is_free(key))
        # Test with actual creation of a DadaBuffer
        k = dada.get_key()
        b = dada.DadaBuffer(1024*1024*1024, key=k)
        await b.create()
        self.assertFalse(dada.is_free(k))
        await b.destroy()

    def test_keys_generation(self):
        keys = dada.get_key(n_keys=16)
        self.assertTrue(len(keys) == 16)


class TEST_DadaBuffer(unittest.IsolatedAsyncioTestCase):

    async def test_creation(self):
        obj = dada.DadaBuffer(1024*1024)
        await obj.create()
        self.assertFalse(dada.is_free(obj.key))
        await obj.destroy()

    async def test_destroy(self):
        obj = dada.DadaBuffer(1024*1024)
        await obj.create()
        await obj.destroy()
        self.assertTrue(dada.is_free(obj.key))

    # Only works with psrdada_cpp version 07417574987e27008979a6e0a3783739d86947a1 or later
    # async def test_reset(self):
    #     obj = dada.DadaBuffer(1024*1024)
    #     await obj.create()
    #     await obj.reset()
    #     await obj.destroy()
    #     self.assertTrue(dada.is_free(obj.key))


    async def test_is_alive(self):
        obj = dada.DadaBuffer(1024*1024)
        self.assertFalse(obj.is_alive())
        await obj.create()
        self.assertTrue(obj.is_alive())
        await obj.destroy()
        self.assertFalse(obj.is_alive())

    async def test_get_dbmonitor(self):
        obj = dada.DadaBuffer(1024*1024)
        monitor = obj.get_monitor()
        self.assertEqual(None, monitor)

        await obj.create()
        monitor = obj.get_monitor()
        monitor.start()
        self.assertTrue(isinstance(monitor, dada.DbMonitor))
        await obj.destroy()

    async def test_monitor_stop(self):
        """ Monitor should be stoppable even if not started """

        obj = dada.DadaBuffer(1024*1024)
        await obj.create()
        monitor = obj.get_monitor()
        monitor.stop()
        await obj.destroy()

    async def test_race_condition(self):
        """
        This tests should ensure, that the DadaBuffer creation do not end in a race condition.
        """
        n_buffers = 16
        for __ in range(2):
            create_tasks = []
            destroy_tasks = []
            for __ in range(n_buffers):
                create_tasks.append(
                    asyncio.create_task(dada.DadaBuffer(1024*1024).create())
                )
            await asyncio.gather(*create_tasks)
            keys = dada.DadaBuffer.used()
            self.assertEqual(len(keys), n_buffers)
            for key in keys:
                destroy_tasks.append(
                    asyncio.create_task(dada.DadaBuffer(0, key=key).destroy())
                )
            await asyncio.gather(*destroy_tasks)
            self.assertEqual(len(dada.DadaBuffer.used()), 0)



class TEST_DbMonitor(unittest.TestCase):

    def test_stdout_parser_params(self):
        exp_params = {}
        def callback_func(row):
            self.assertEqual(row['key'], exp_params["key"])
            self.assertEqual(row['fraction-full'], exp_params["fraction-full"])
            self.assertEqual(row['written'], exp_params["written"])
            self.assertEqual(row['read'], exp_params["read"])
            self.assertEqual(row['clear'], exp_params["clear"])

        dbm = dada.DbMonitor('dummy', callback_func)
        for i in range(10):
            exp_params = {
                "key":"dummy",
                "fraction-full":math.inf,
                "written":i,
                "read":i,
                "clear":i
            }
            dbm._stdout_parser("8   0   0  0  0      0   0   {i}     {i}     {i}"\
                .format(i=i))

    def test_add_callback(self):
        def callback_func(params):
            pass
        # Add a single callback
        dbm = dada.DbMonitor('dummy')
        dbm.add_callback(callback_func)
        self.assertEqual([callback_func], dbm.get_callback())
        # Add a list of call backs
        dbm.add_callback([callback_func, callback_func])
        self.assertEqual([callback_func, callback_func, callback_func],
            dbm.get_callback())


class TEST_DadaCLI(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self):
        self.tobj = dada.DadaBuffer(1024*1024)
        await self.tobj.create()

    def setUp(self):
        np.arange(1024*1024).tofile("/tmp/test.dat")

    async def asyncTearDown(self):
        await self.tobj.destroy()

    def tearDown(self):
        os.remove("/tmp/test.dat")

    def test_DbNull(self):
        reader = dada.DbNull(self.tobj.key)
        reader.start()
        reader.stop()

    def test_DbDisk(self):
        reader = dada.DbDisk(self.tobj.key)
        reader.start()
        reader.stop()

    async def test_DbSplitDb(self):
        buffers = []
        for i in range(4):
            buffers.append(dada.DadaBuffer(1024*1024//4))
            await buffers[i].create()
        reader = dada.DbSplitDb(self.tobj.key, [buf.key for buf in buffers])
        reader.start()
        reader.stop()
        for buf in buffers:
            await buf.destroy()

    def test_DiskDb(self):
        writer = dada.DiskDb(self.tobj.key)
        writer.start()
        writer.stop()

    def test_SyncDb(self):
        writer = dada.SyncDb(self.tobj.key)
        writer.start()
        writer.stop()

    def test_JunkDb(self):
        writer = dada.JunkDb(self.tobj.key)
        writer.start()
        writer.stop()

if __name__ == '__main__':
    # dada.rm_buffers()
    unittest.main()
    # dada.rm_buffers()
