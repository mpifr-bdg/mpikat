
import asyncio
from argparse import ArgumentParser
import json
from mpikat.core.alveo_client import get_alveo_ids
from mpikat.core import logger


_log = logger.getLogger("mpikat.alvoe_tool")



def main():
    ids = get_alveo_ids() 
    print(json.dumps({'device_ids': ids}))


if __name__ == "__main__":
    main()


