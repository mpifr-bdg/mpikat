# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring
import unittest
from unittest import mock
import sys
import ipaddress
from mpikat.utils import ip_utils

sys.modules['pynq'] = mock.MagicMock()


unittest.mock.patch("mpikat.core.alveo_client.get_interface").start()
from mpikat.core.alveo_client import get_alveo_ids  # pylint: disable=wrong-import-position
from mpikat.pipelines.alveo_pfb_controller import AlveoPipeline # pylint: disable=wrong-import-position



example_json_pci_ids = '''{
    "schema_version": {
        "schema": "JSON"
    },
    "system": {
        "host": {
            "os": {
                "sysname": "Linux",
                "release": "6.5.0-41-generic",
                "version": "#41~22.04.2-Ubuntu SMP PREEMPT_DYNAMIC Mon Jun  3 11:32:55 UTC 2",
                "machine": "x86_64",
                "distribution": "Ubuntu 20.04.6 LTS",
                "model": "Standard PC (Q35 + ICH9, 2009)",
                "cores": "4",
                "memory_bytes": "0x1efca0000",
                "libraries": [
                    {
                        "name": "glibc",
                        "version": "2.31"
                    }
                ],
                "hostname": "a6ddf1aab138"
            },
            "xrt": {
                "version": "2.16.0",
                "branch": "HEAD",
                "hash": "fa4c0045003fed0acea4593788dce5ef6d0b66ee",
                "build_date": "2024-07-12 19:11:14",
                "drivers": [
                    {
                        "name": "xocl",
                        "version": "2.16.204",
                        "hash": "fa4c0045003fed0acea4593788dce5ef6d0b66ee"
                    },
                    {
                        "name": "xclmgmt",
                        "version": "2.16.204",
                        "hash": "fa4c0045003fed0acea4593788dce5ef6d0b66ee"
                    }
                ]
            },
            "devices": [
                {
                    "bdf": "0000:01:00.0",
                    "vbnv": "xilinx_u55c_gen3x16_xdma_base_3",
                    "id": "97088961-FEAE-DA91-52A2-1D9DFD63CCEF",
                    "instance": "mgmt(inst=256)",
                    "is_ready": "true"
                }
            ]
        }
    }
}'''

example_json_sn = '''
{
    "schema_version": {
        "schema": "JSON"
    },
    "devices": [
        {
            "interface_type": "pcie",
            "device_id": "0000:01:00.0",
            "device_status": "UNKNOWN",
            "platform": {
                "bdf": "0000:01:00.0",
                "flash_type": "spi",
                "hardware": {
                    "serial_num": "XFL1QZUGTSS4"
                },
                "device_properties": {
                    "board_type": "u55c",
                    "board_name": "ALVEO U55C PQ",
                    "config_mode": "7",
                    "max_power_watts": "75W"
                },
                "current_partitions": "",
                "current_shell": {
                    "vbnv": "xilinx_u55c_gen3x16_xdma_base_3",
                    "logic-uuid": "97088961-FEAE-DA91-52A2-1D9DFD63CCEF",
                    "interface-uuid": "B7AC1ABE-1E3E-1CB6-86D5-A81232452676",
                    "id": "0x97088961feaeda91",
                    "sc_version": "7.1.22"
                },
                "available_shells": "",
                "available_partitions": "",
                "macs": [
                    {
                        "address": "00:0A:35:0F:81:10"
                    },
                    {
                        "address": "00:0A:35:0F:81:11"
                    },
                    {
                        "address": "00:0A:35:0F:81:12"
                    },
                    {
                        "address": "00:0A:35:0F:81:13"
                    },
                    {
                        "address": "00:0A:35:0F:81:14"
                    },
                    {
                        "address": "00:0A:35:0F:81:15"
                    },
                    {
                        "address": "00:0A:35:0F:81:16"
                    },
                    {
                        "address": "00:0A:35:0F:81:17"
                    }
                ]
            }
        }
    ]
}'''

def mock_xbmgmt(args, **kwargs):  # pylint: disable=unused-argument
    """ Mock writing the JSON data to given file"""
    for i, a in enumerate(args):
        if a.strip() == '-o':
            filename = args[i + 1]

    with open(filename, 'w', encoding='utf8') as f:
        if '0000:01:00.0' in " ".join(args):
            f.write(example_json_sn)
        else:
            f.write(example_json_pci_ids)



class TestAlveoPipeline(unittest.IsolatedAsyncioTestCase):
    def test_config_after_construction(self):
        """After construction, all output addresses should be relative to given multicast base address"""

        device_netcfg = {'multicast_destinations': '111.111.111.111+64'}
        pipeline = AlveoPipeline('0.0.0.0', 0, device_netcfg, 0)

        ip0, _, _ = ip_utils.split_ipstring(device_netcfg['multicast_destinations'])
        ip0 = ipaddress.IPv4Address(ip0)

        offset = 0
        for ds in pipeline._config['output_data_streams'].values():
            ips = ip_utils.ipstring_to_list(ds['ip'])
            self.assertEqual(int(ip0) + offset, int(ipaddress.IPv4Address(ips[0])), msg=f'Expected {ipaddress.IPv4Address(int(ip0) + offset).compressed}, got {ips[0]}')
            offset += len(ips)

    async def test_no_shift(self):
        """No changes should be done if no base adress is specified."""

        pipeline = AlveoPipeline('0.0.0.0', 0, {}, 0)

        await pipeline.set({'output_data_streams': {'a': {'ip': '123.123.123.123+5'}, 'b': {'ip': '123.123.123.129+3'}}})

        self.assertEqual(pipeline._config['output_data_streams']['a']['ip'], '123.123.123.123+5')
        self.assertEqual(pipeline._config['output_data_streams']['b']['ip'], '123.123.123.129+3')




    async def test_config_after_set(self):
        """After set, all output addresses should be relative to given multicast base address"""

        device_netcfg = {'multicast_destinations': '111.111.111.111+64'}
        pipeline = AlveoPipeline('0.0.0.0', 0, device_netcfg, 0)

        await pipeline.set({'output_data_streams': {'a': {'ip': '123.123.123.123+5'}, 'b': {'ip': '123.123.123.129+3'}}})

        self.assertEqual(pipeline._config['output_data_streams']['a']['ip'], '111.111.111.111+5')
        self.assertEqual(pipeline._config['output_data_streams']['b']['ip'], '111.111.111.117+3')


    async def test_shift_with_auto(self):
        """Auto with base adress should jsut start at base address"""

        device_netcfg = {'multicast_destinations': '111.111.111.111+64'}
        pipeline = AlveoPipeline('0.0.0.0', 0, device_netcfg, 0)

        await pipeline.set({'output_data_streams': {'a': {'ip': 'auto+5'}, 'b': {'ip': 'auto+3'}}})

        self.assertEqual(pipeline._config['output_data_streams']['a']['ip'], '111.111.111.111+5')
        self.assertEqual(pipeline._config['output_data_streams']['b']['ip'], '111.111.111.117+3')

    async def test_noshift_after_set_with_auto(self):
        """Auto without base adress should be kept."""

        device_netcfg = {}
        pipeline = AlveoPipeline('0.0.0.0', 0, device_netcfg, 0)

        await pipeline.set({'output_data_streams': {'a': {'ip': 'auto+5'}, 'b': {'ip': 'auto+3'}}})

        self.assertEqual(pipeline._config['output_data_streams']['a']['ip'], 'auto+5')
        self.assertEqual(pipeline._config['output_data_streams']['b']['ip'], 'auto+3')









class Test_PipelineRegister(unittest.IsolatedAsyncioTestCase):
    @mock.patch("subprocess.call")
    def test_get_data(self, popen_patch):

        popen_patch.side_effect = mock_xbmgmt
        card_serials = get_alveo_ids()

        self.assertEqual(len(card_serials), 1)
        self.assertEqual(list(card_serials)[0], 'XFL1QZUGTSS4')  # hard coded serial obtained from patch which returns recoreded cbmgmt output

if __name__ == "__main__":
    unittest.main()
