"""
Utils to work with IP addresses
"""

import ipaddress
import netaddr
from typing import List

from mpikat.core import logger
_log = logger.getLogger('mpikat.utilsip_utils')

def split_ipstring(ips):
    """
    Split an string of form iprange:port, e.g. 123.0.0.4+7:1234 into baseip,
    total number of ips and port. Raise an exception on invalid ip, port or number.
    """
    _log.debug("Checking string: {}".format(ips))
    try:
        if ":" not in ips:
            R = ips
            port = None
        else:
            R, port = ips.split(':')
            port = int(port)
        if '+' not in R:
            ip = R
            N = 1
        else:
            ip, N = R.split('+')
            N = int(N) + 1
        ipaddress.IPv4Address(ip)
    except Exception as E:
        _log.error("Cannot parse string: {}".format(ips))
        raise E

    return ip, N, port


def is_valid_multicast(ip_str: str) -> bool:
    """Checks if passed ip_str is a valid multicast address

    Args:
        ip_str (str): The string

    Returns:
        bool: True if it is valid, otherwise false
    """
    try:
        ipaddress.ip_address(ip_str)
    except ValueError:
        return False
    return True


def is_valid_multicast_range(ip, N, port):
    """
    Validates a multicast ranges of format 225.0.0.4, 7, 1234.

        - The base ip has to be a valid multicast address.
        - The address range has to be a valid power of two.
        - The address has to be a multiple of the blocksize. A block of 8 can
          start at a.b.c.0, a.b.c.8, a.b.c.16, ...
          This is a limitation we inherited from the casperfpga skarab firmware(s).

    The function will return True if the string specifies a valid range and false otherwise. If the string cannot be interpreted an error is raised.
    """
    if (N & (N - 1)) != 0:
        _log.warning("{} is not a power of 2".format(N))
        return False
    try:
        network = ipN2network(ip, N)
    except ValueError:
        _log.warning("{} does not start a valid range of size {} ({})".format(ip, 2**(N.bit_length()-1), N))
        return False

    _log.debug(" Validating network {}".format(network))
    if not network.is_multicast:
        _log.warning("{} is not a multicsat address".format(ip))
        return False

    if not network[0] == ipaddress.ip_address(ip):
        _log.warning("{} does not start a valid range of size {} ({}) - please use {}".format(ip, 2**(N.bit_length()-1), N, network))
        return False
    return True

def is_valid_nic_mac(mac):
    """
    Test if the mac is valid for a nic, i.e. in particular not a multicast mac.
    """
    # https://stackoverflow.com/questions/27739889/is-there-a-library-function-to-check-if-a-mac-address-is-multicast

    try:
        mac = netaddr.EUI(mac)
    except netaddr.core.AddrFormatError:
        return False
    return not bool(mac.words[0] & 0b01)


def ipstring_to_list(ipstring):
    """
    Generate list of ips from string of form a.b.c.d+3
    """
    ip, N, _ = split_ipstring(ipstring)
    ips = [(ipaddress.IPv4Address(ip) + i).compressed for i in range(N)]
    return ips


def ipstring_from_list(ipstrings):
    """
    Creates a string of format a.b.c.d+x from a list of ips.
    """
    ips = [ipaddress.IPv4Address(ip) for ip in ipstrings]
    ips.sort()

    if ips[0] + len(ips) - 1 != ips[-1]:
        raise RuntimeError("IP list not continous")

    return "{}+{}".format(ips[0].compressed, len(ips) - 1)

def network2ipstring(network):
    """
    Generate katcp style network range string, e.g. 239.11.1.150+15 from ipaddress
    network
    """
    N = network.num_addresses - 1
    if N == 0:
        result = f'{network.network_address}'
    else:
        result = f'{network.network_address}+{N}'
    return result


def ipN2network(ip: str, N: int) -> ipaddress.IPv4Network:
    """Transforms the IP addresses into a ip_network address

    Args:
        ip (str): The base address
        N (int): The number of ips

    Returns:
        ipaddress.IPv4Network: The returned network
    """
    return ipaddress.ip_network(f'{ip}/{32-(N.bit_length() - 1)}', strict=False)

def ipstring2network(ipstring):
    """
    Create a ipadresses.ip_network from a kacpstyle ipstring
    """
    ip, N, _ = split_ipstring(ipstring)
    network = ipN2network(ip, N)
    return network



def concatenate_ips(ip_list: list) -> list:
    """Takes a list of list, strings and integers and concatenates valid ips to a single list

    Example:
        ip_list = [['225.0.0.1','225.0.0.2','I.m.a.bad.address'], '226.0.0.0+4']
        would result in
        [225.0.0.1, 225.0.0.2, 226.0.0.0, 226.0.0.1, 226.0.0.2, 226.0.0.3]
    Args:
        ip_list (list): List of arbiatary ips

    Returns:
        list: The concatenated list
    """
    res = []
    if not isinstance(ip_list, list):
        ip = [ip]
    for ips in ip_list:
        if isinstance(ips, list):
            res.extend(ips)
        elif isinstance(ips, str):
            try:
                res.extend(ipstring_to_list(ips))
            except ValueError as e:
                _log.error("Failed to extend ip list with %s and ip: %s", e, ips)
        elif isinstance(ips, int):
            try:
                res.append(str(ipaddress.ip_address(ips)))
            except ValueError as e:
                _log.error("Failed to create ip address from %d with error %s", ips, e)
    for ip in res:
        if not is_valid_multicast(ip):
            res.remove(ip)
    return sorted(list(set(res)), key=lambda ip: tuple(map(int, ip.split('.'))))

def ips_from_stream_description(streams: List[dict]) -> list:
    """Returns IPs in a list from given stream descriptions

    Args:
        streams (List[dict]): The stream descriptions

    Returns:
        list: IPs of the streams
    """
    return concatenate_ips([stream["ip"] for stream in streams])


def shift_ip_ranges(ip_ranges, start_ip):
    """
    Shift list of IP ranges relative to start with a given IP.

    Example:
        R = shift_ip_ranges([1.2.3.1+3, 1.2.3.5+2], 2.1.1.12 )
        print(R)
        >>> print(R)
        ['2.1.1.12+3', '2.1.1.16+2']

    IP ranges can contain also ports and do not need to be sorted.
    """
    _log.debug('Shift %i IP ranges to base %s', len(ip_ranges), start_ip)

    min_ip = ipaddress.IPv4Address('255.255.255.255')
    # find min IP first
    for ipr in ip_ranges:
        ips, _, _ = split_ipstring(ipr)
        ip = ipaddress.IPv4Address(ips)
        if ip < min_ip:
            min_ip = ip
    _log.debug('Min IP: %s', min_ip)

    offset = int(ipaddress.IPv4Address(start_ip)) - int(min_ip)
    _log.debug('Shifting by %i', offset)

    result = []
    for ipr in ip_ranges:
        ips, N, port = split_ipstring(ipr)
        ip = ipaddress.IPv4Address(ips) + offset
        R = ip.compressed
        if N > 1:
            R += f'+{N-1}'
        if port:
            R += f':{port}'
        result.append(R)
        _log.debug('Shifting %s -> %s', ipr, R)
    return result
