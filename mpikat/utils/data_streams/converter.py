import numpy as np
import base64

def deserialize_array(serialized: str, dtype: str="float32", encoding: str="utf-8"):
    """Decodes a b64 string into a numpy array

    Args:
        serialized (str): the b64 encoded string
        dtype (str, optional): The data type of the numpy array. Defaults to "float32".
        encoding (str, optional): The encoding type. Defaults to "utf-8".

    Returns:
        np.ndarray: The decoded numpy array
    """
    return np.frombuffer(base64.b64decode(serialized.encode(encoding)), dtype=dtype)

def serialize_array(array: np.ndarray, encoding: str="utf-8") -> str:
    """Encodes a numpy array into b64 encoded string

    Args:
        array (np.ndarray): The numpy array to encode
        encoding (str, optional): The encoding type. Defaults to "utf-8".

    Returns:
        str: the encoded string
    """
    return base64.b64encode(array.tobytes()).decode(encoding)