import json
import os

import asyncio
from aiokatcp import Sensor, FailReply

from mpikat.core import cfgjson2dict
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change, getArgumentParser
from mpikat.core.alveo_client import get_interface
import mpikat.utils.ip_utils as ip_utils
import mpikat.pipelines.digpack_controller  # import needed to register packetizer data stream  pylint: disable=unused-import
from mpikat.core.data_stream import DataStreamRegistry
from mpikat.core import logger as logging

_log = logging.getLogger("mpikat.pipelines.AlveoPipeline")

DataStreamRegistry.register({"type": "AlveoPFB:1",
                             "description": "Channelized complex voltage ouptut.",
                             "ip": None,
                             "port": None,
                             "sample_rate": None,
                             "central_freq": None,
                             "sync_time": None,
                             "predecimation_factor": None,
                             })


DEFAULT_CONFIG = {
    # default name for master controller.
    # from Ansible
    "id": "alveo_pipeline",
    "type": "AlveoPipeline",
    # supported input formats name:version
    "supported_input_formats": {"MPIFR_EDD_Packetizer": [1]},
    "input_data_streams": {
        "polarization_0":
        {
            # Format has version separated via colon
            "format": "MPIFR_EDD_Packetizer:1",
            "ip": "225.1.90.192+3",
            "port": "7148",
            "bit_depth": 8,
        },
        "polarization_1":
        {
            "format": "MPIFR_EDD_Packetizer:1",
            "ip": "225.1.90.196+3",
            "port": "7148",
            "bit_depth": 8,
        }
    },
    "output_data_streams":                              # Filled programmatically, see below
    {                                                   # The output can be split into an arbitrary sequence of streams. The board streams to the lowest specified stream + 8 groups
        "band0": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.120+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 1268.0
        },
        "band1": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.128+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 1396.0
        },
        "band2": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.136+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 1524.0
        },
        "band3": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.144+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 1652.0
        },
        "band4": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.152+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 1780.0
        },
        "band5": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.160+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 1908.0
        },
        "band6": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.168+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 2036.0
        },
        "band7": {
            "format": "AlveoPFB:1",
            "ip": "239.2.2.176+7",
            "port": "60002",
            "sample_rate": 4000000000.0,
            "central_freq": 2164.0
        }

    },

    "log_level": "debug",
    # Force reprogramming of with new firmware version
    "force_program": True,
    # Skips the FPGA config always. Overrides force.
    "skip_device_config": False,
    "firmware_directory": os.path.join(os.path.dirname(os.path.realpath(__file__)), "alveo_firmwares"),
    "firmware": "",  # If defined, overrides default firmware
    # Channels per multicast group in the FPGA output
    # Id to add to the SPEAD headers of the FPGA output
    "board_id": 23,
    "channels_per_group": 1,
    # initial value for the quantization factor. Can be changed per
    # measurement
    "initial_quantization_factor": 8388608,
    # initial value for the FFT shift. Can be changed per measurement
    "initial_fft_shift": 127,
    "relock_cnts": 3000,
    "mcnt_diff": 65,
    "read_cnts": 128, #old firmware default
    "packet_size": 8000,
}



class AlveoPipeline(EDDPipeline):
    """ Alveo controlling Pipeline
    """
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    def __init__(self, ip, port, device_netcfg, index):
        """initialize the pipeline.
        Args:
            ip:  Network to accept connections from
            port: Port to listen on
            device_netcfg: dictionary of IPs and macs for the FPGA NIC
            index: Index of the device to use
        """
        self._device_netcfg = device_netcfg

        EDDPipeline.__init__(self, ip, port, DEFAULT_CONFIG)

        self.shift_output_streams()
        _log.info('Connecting to alveo hosting @ {}:{}'.format(ip, port))


        self._client = get_interface(index)

    def shift_output_streams(self):
        """Shift output streams to given multicast destination"""
        if not (mc:=self._device_netcfg.get('multicast_destinations', None)):  # pylint: disable=superfluous-parens
            _log.debug('Not shifting output data streams')
            return

        ip_counter = 0
        for stream in self._config['output_data_streams'].values():
            if 'auto' in stream['ip']:
                _log.debug('Found auto chosen ip in stream: %s', stream['ip'])
                stream['ip'] = stream['ip'].replace('auto', f'0.0.0.{ip_counter}')
                _, N, _ = ip_utils.split_ipstring(stream['ip'])
                ip_counter += N
                _log.debug('Updated to: %s', stream['ip'])

        _log.debug('Shifting output data streams')
        start_ip, _, _ = ip_utils.split_ipstring(mc)
        R = ip_utils.shift_ip_ranges([d['ip'] for d in self._config['output_data_streams'].values()], start_ip)

        for (k, v), uip in zip(self._config['output_data_streams'].items(), R):
            _log.debug('Updating ip for %s: %s -> %s', k, v['ip'], uip)
            v['ip'] = uip


    def setup_sensors(self):
        EDDPipeline.setup_sensors(self)
        self._fpga_clock = Sensor(float, "fpga-clock",
                                  description="FPGA Clock estimate",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fpga_clock)

    async def set(self, config_json):
        cfg = cfgjson2dict(config_json)
        if 'output_data_streams' in cfg:
            _log.debug("Stripping outputs from cfg before check")
            # Do not check output data streams, as the only relevant thing is here
            # that they are consecutive
            outputs = cfg.pop('output_data_streams')

            _log.debug("Pipeline set")
            await EDDPipeline.set(self, cfg)
            _log.debug("Re-adding outputs")
            self._config['output_data_streams'] = outputs
            self.shift_output_streams()
            self._configUpdated()
        else:
            await EDDPipeline.set(self, cfg)

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the Alveo PFB Pipeline
        """
        _log.info("Configuring EDD backend for processing")
        _log.info("Initial configuration:\n%s", json.dumps(self._config, indent=4))
        # Convert arbitrary output parts to input list
        iplist = []
        for l in self._config["output_data_streams"].values():
            iplist.extend(ip_utils.ipstring_to_list(l["ip"]))
            channels_per_band = len(ip_utils.ipstring_to_list(l["ip"]))

        output_string = ip_utils.ipstring_from_list(iplist)
        output_ip, Noutput_streams, port = ip_utils.split_ipstring(
            output_string)

        _log.info("Noutput_streams ={}".format(Noutput_streams))
        _log.info("output_ip ={}".format(output_ip))
        _log.info("channels_per_band ={}".format(channels_per_band))
        port = {l["port"] for l in self._config["output_data_streams"].values()}
        if len(port) != 1:
            raise FailReply("Output data streams have to stream to same port")
        freq_per_channel_MHz = self._config["input_data_streams"]["polarization_0"]["sample_rate"] / 1e6 / Noutput_streams / 2
        freq_per_band_MHz = freq_per_channel_MHz * channels_per_band
        sky_freq_MHz = self._config["input_data_streams"]["polarization_0"]["central_freq"]
        bandwidth_MHz = freq_per_channel_MHz * Noutput_streams
        freq_offset_MHz = bandwidth_MHz / Noutput_streams / 2
        central_freq_MHz_bottom = sky_freq_MHz - bandwidth_MHz / 2 + bandwidth_MHz / channels_per_band / 2 - freq_offset_MHz
        _log.info("central_freq_MHz_bottom: %s, freq_per_band_MHz: %s, Noutput_streams: %s, channels_per_band: %s",
          central_freq_MHz_bottom, freq_per_band_MHz, Noutput_streams, channels_per_band)
        band_central_freq = [central_freq_MHz_bottom + freq_per_band_MHz * i for i in range(int(Noutput_streams/channels_per_band))]

        # update sync time based on input
        for stream in self._config["output_data_streams"].values():
            # Copy some attributes from the input streams to the output streams
            stream["sync_time"] = self._config["input_data_streams"]["polarization_0"]["sync_time"]
            stream["sample_rate"] = self._config["input_data_streams"]["polarization_0"]["sample_rate"]
            stream["bit_depth"] = self._config["input_data_streams"]["polarization_0"]["bit_depth"]
            stream["bandwidth"] = float(self._config["input_data_streams"]["polarization_0"]["sample_rate"])/1e6/(Noutput_streams/channels_per_band)/2
            stream["central_freq"] = band_central_freq.pop(0)
            stream["channels_per_band"] = channels_per_band
            # Set some fixed values for other attributes
            stream["ndim"] = 2
            stream["npol"] = 2
            stream["tsamps"] = Noutput_streams / (float(self._config["input_data_streams"]["polarization_0"]["sample_rate"])/1e6/2)
            stream["samples_per_heap"] = self._config["packet_size"]
            stream["time_step"] = int(32 * stream["samples_per_heap"])

        self._configUpdated()

        _log.info("Final configuration:\n%s", json.dumps(self._config, indent=4))

        if self._config["skip_device_config"]:
            _log.warning(
                "Skipping device configuration because debug mode is active!")
            return

        if self._config['firmware']:
            firmwarefile = os.path.join(self._config["firmware_directory"], self._config['firmware'])
            _log.debug("Setting firmware file %s", firmwarefile)
            self._client.setFirmware(firmwarefile)
        _log.debug("Connecting to client")
        await self._client.connect()

        await self._client.initialize()
        if self._config['force_program']:
            _log.debug("Forcing reprogramming")
            await self._client.program()

        await self._client.configure(**self._device_netcfg)

        await self._client.lock_timestamp(int(32* self._config["packet_size"]), self._config["mcnt_diff"], self._config["relock_cnts"], self._config["read_cnts"])
        await self._client.set_input_bits(int(self._config["input_data_streams"]["polarization_0"]["bit_depth"]))
        await self._client.configure_inputs(self._config["input_data_streams"]["polarization_0"]["ip"], self._config["input_data_streams"]["polarization_1"]["ip"], port=int(self._config["input_data_streams"]["polarization_0"]["port"]))

        await self._client.configure_output(output_ip, int(port.pop()), number_of_groups=Noutput_streams, packet_size=self._config["packet_size"])

        await self._client.configure_quantization_factor(self._config["initial_quantization_factor"])
        await self._client.configure_fft_shift(self._config["initial_fft_shift"])

    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        await self._client.capture_start()

    async def measurement_prepare(self, config=None):
        """Set quantization factor and fft_shift parameter"""
        if config is None:
            config = {}
        if "fft_shift" in config:
            await self._client.configure_fft_shift(config["fft_shift"])
        if "quantization_factor" in config:
            await self._client.configure_quantization_factor(config["quantization_factor"])

    @state_change(target="idle", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        await self._client.capture_stop()


async def wrapper():
    """Async wrap pipeline launch"""
    parser = getArgumentParser()
    parser.add_argument('--alveo-index', dest='alveo_index', type=int,
                        default=0, help='Index number of the Alveo card')

    parser.add_argument('--device-netcfg', dest='device_netcfg', type=str, nargs="*",
                        help='Settings for the alveo Interfaces mac0=XX:XX:XX:XX ip0=a.b.c.d, one per device of the card and (optionally) a multicast destination range to which output streams are shifted multicast_destinations=aa.bb.cc.d+63', required=True)

    args = parser.parse_args()

    logging.setLevel(args.log_level.upper())

    device_netcfg = {}
    for i in args.device_netcfg:
        k, v = i.split('=')
        device_netcfg[k] = v

    server = AlveoPipeline(args.host, args.port, device_netcfg, args.alveo_index)
    await launchPipelineServer(server, args)


if __name__ == "__main__":
    asyncio.run(wrapper())
