import unittest
import numpy as np
import uuid
import h5py
import os

from mpikat.core.hdf5_file_writer import HDF5FileWriter


def gated_spectrometer_format(nchannels):
    """
    Create a format dictionary for the spectrometer format. This has to go somewhere else.
    """
    dformat = {}
    dformat['timestamp'] = {'dtype': float, 'shape': (1,)}
    dformat['spectrum'] = {'dtype': float, 'shape': (nchannels,)}
    dformat['integration_time'] = {'dtype': float, 'shape': (1,)}
    dformat['saturated_samples'] = {'dtype': np.int64, 'shape': (1,)}

    return dformat

class TestHDF5FileWriter(unittest.TestCase):
    def test_auto_filename(self):
        f = HDF5FileWriter()

        self.assertTrue(f.getFileSize())
        self.addCleanup(os.remove, f.filename)
        f.close()
        self.assertTrue(os.path.exists(f.filename))


    def test_manual_filename(self):
        fn = '/tmp/{}'.format(uuid.uuid4())
        f = HDF5FileWriter(fn)
        self.addCleanup(os.remove, f.filename)
        f.close()
        self.assertTrue(os.path.exists(f.filename))

    def test_creation_of_subscans(self):
        fn = '/tmp/{}'.format(uuid.uuid4())
        f = HDF5FileWriter(fn)
        self.addCleanup(os.remove, f.filename)

        f.newSubscan()
        f.newSubscan()
        f.close()
        infile = h5py.File(f.filename, "r")
        self.assertEqual(len(infile['scan'].keys()), 2)


    def test_gated_spectrometer_data_insert(self):
        f = HDF5FileWriter()
        self.addCleanup(os.remove, f.filename)

        nchannels = 64 * 1024

        data = {}
        for n,d in gated_spectrometer_format(nchannels).items():
            data[n] = np.empty(**d)

        f.newSubscan()
        attr = {'foo':'bar', 'nu': 3}
        f.addData('mysection', data, attr)
        f.close()

        infile = h5py.File(f.filename, "r")

        self.assertTrue("scan" in infile)
        self.assertTrue("scan/000" in infile)

        dataset = infile["scan/000/mysection"]
        for k in data:
            # Strip NaNs from test
            idx = data[k] == data[k]
            self.assertTrue((data[k] == dataset[k][0])[idx].all())

        self.assertEqual(dataset.attrs['foo'], 'bar')
        self.assertEqual(dataset.attrs['nu'], 3)


    def test_insertion_of_history(self):
        fn = '/tmp/{}'.format(uuid.uuid4())
        f = HDF5FileWriter(fn)
        self.addCleanup(os.remove, f.filename)
        self.assertTrue('history' in f._file.keys())
        self.assertEqual(f._file['history'][0][1], b'mpikat.HDF5Writer')
        f.close()

    def test_insertion_of_format_version(self):
        fn = '/tmp/{}'.format(uuid.uuid4())
        f = HDF5FileWriter(fn)
        self.addCleanup(os.remove, f.filename)
        self.assertTrue('FORMAT_VERSION' in f._file.attrs.keys())
        f.close()

    def test_file_reopen(self):
        fn = '/tmp/{}'.format(uuid.uuid4())
        f = HDF5FileWriter(fn)

        f.newSubscan()
        self.addCleanup(os.remove, f.filename)
        f.addData('testdata1', {'foo': np.zeros(5)})
        f.close()

        with h5py.File(f.filename, "r") as infile:
            self.assertTrue("scan/000/testdata1" in infile)

        with self.assertRaises(Exception) as context:
            f.addData('testdata2', np.zeros(5))
        f.open()
        f.addData('testdata2', {'foo': np.zeros(5)})
        f.close()
        with h5py.File(f.filename, "r") as infile:
            self.assertTrue("scan/000/testdata1" in infile)
            self.assertTrue("scan/000/testdata2" in infile)

    def test_file_isopen(self):
        fn = '/tmp/{}'.format(uuid.uuid4())
        f = HDF5FileWriter(fn)
        self.assertTrue(f.is_open())
        f.flush()
        self.assertTrue(f.is_open())
        f.close()
        self.assertFalse(f.is_open())


if __name__ == '__main__':
    unittest.main()
