# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
from unittest import  mock
import tempfile
import shutil
import os
import inspect
import copy
import json

import aiokatcp
import asyncio

from mpikat.core import logger, updateConfig
from mpikat.core import datastore 
from mpikat.utils import testing, get_port, ip_manager, ip_utils
import mpikat.core.edd_pipeline_aio

from mpikat.pipelines.pulsar_processing import EddPulsarPipeline, EddPulsarTimingPipeline , EddPulsarBasebandPipeline, EddPulsarSearchPipeline, EddPulsarLeapPipeline, _DEFAULT_CONFIG, PulsarBase, ArchiveAdder

_log = logger.getLogger("mpikat.tests.test_pulsar_processing")

data_dir = None

def mock_numa_info():
    """Mock numa info"""
    return {'0': {'net_devices': {'ibp5s0': {'ip': '10.42.0.207', 'speed': 40000},
   'ens1d1': {'ip': '', 'speed': 0},
   'ens1': {'ip': '10.10.1.87', 'speed': -1}},
  'cores': ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
  'isolated_cores': [],
  'gpus': ['0']},
 '1': {'net_devices': {'enp129s0f0': {'ip': '134.104.70.97', 'speed': 1000},
   'enp129s0f1': {'ip': '', 'speed': 0},
   'ens5d1': {'ip': '', 'speed': 0},
   'ens5': {'ip': '10.10.1.97', 'speed': 10000}},
  'cores': ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19'],
  'isolated_cores': [],
  'gpus': ['1']}}


def setUpModule():
    global data_dir
    data_dir = tempfile.mkdtemp()
    unittest.mock.patch("mpikat.utils.numa.getInfo", wraps=mock_numa_info).start()
def tearDownModule():
    shutil.rmtree(data_dir)



def read_mkrecv_header(filename):
    data = {}
    with open(filename) as f:
        for line in f:
            p = line.split('#')[0].strip().split()
            if len(p) == 2:
                data[p[0]] = p[1]

    return data



class Test_EDDPulsarPipeline(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.port = get_port()
        self.thread = testing.launch_server_in_thread(
            EddPulsarPipeline, '0.0.0.0', self.port,
        )
        self.pipeline = self.thread.server
        self.implementation = mock.AsyncMock()
        self.implementation.sensors = aiokatcp.sensor.SensorSet()
        self.implementation.check_cal_active = mock.Mock(return_value=True)

        self.pipeline.pipeline_classes['mock'] = lambda cfg: self.implementation 
        self.client = None
        self.redis_db = testing.setup_redis()
        self.pipeline._config["data_store"] = {'ip': "localhost", 'port': self.redis_db.server_config['port']}


    async def asyncTearDown(self) -> None:
        """
        asyncTearDown
        """
        self.thread.stop().result()
        self.thread.join(timeout=5)
        self.redis_db.shutdown()


    async def test_raise_invalid_mode(self):
        """Pipeline should raise an value error if an invalid mode is requested"""
        await self.pipeline.set({"mode": "THIS_MODE_IS_INVALID"})
        with self.assertRaises(ValueError):
            await self.pipeline.configure()


    async def test_pass_of_requests_to_implementation(self):
        """
        All EDD state machine methods should be passed from the pipeline to the
        requests
        """
        self.assertIsNone(self.pipeline._implementation)
        await self.pipeline.set({"mode": "mock"})
        await self.pipeline.configure()
        self.pipeline._implementation.configure.assert_awaited_once()

        await self.pipeline.capture_start()
        self.pipeline._implementation.capture_start.assert_awaited_once()

        await self.pipeline.measurement_prepare()
        self.pipeline._implementation.measurement_prepare.assert_awaited_once()

        await self.pipeline.measurement_start()
        self.pipeline._implementation.measurement_start.assert_awaited_once()

        await self.pipeline.measurement_stop()
        self.pipeline._implementation.measurement_stop.assert_awaited_once()
        await self.pipeline.deconfigure()
        self.pipeline._implementation.deconfigure.assert_awaited_once()


    async def test_skip_of_inactive_pipelines(self):
        """
        If pipeline is inactive, the measurement prep/start/stop calls should be skipped
        """
        await self.pipeline.set({"mode": "mock", "active": 0})
        await self.pipeline.configure()
        self.pipeline._implementation.configure.assert_awaited_once()

        await self.pipeline.capture_start()
        self.pipeline._implementation.capture_start.assert_awaited_once()

        await self.pipeline.measurement_prepare()
        self.pipeline._implementation.measurement_prepare.assert_not_awaited()

        await self.pipeline.measurement_start()
        self.pipeline._implementation.measurement_start.assert_not_awaited()

        await self.pipeline.measurement_stop()
        self.pipeline._implementation.measurement_stop.assert_not_awaited()
        await self.pipeline.deconfigure()
        self.pipeline._implementation.deconfigure.assert_awaited_once()


    async def test_return_files_in_data_dir_on_measurement_stop(self):
        """
        Should return all files in data dir on measurement stop
        """
        with tempfile.TemporaryDirectory(dir='/tmp') as d:
            await self.pipeline.set({"mode": "mock", "active": 1, 'data_root_directory': d})
            await self.pipeline.configure()
            await self.pipeline.capture_start()
            await self.pipeline.measurement_prepare()
            await self.pipeline.measurement_start()

            self.pipeline._implementation.data_directory = d

            with open(os.path.join(d, '1'), 'w'):
                pass
            os.makedirs(os.path.join(d, 'a', 'b'))

            with open(os.path.join(d, 'a', 'b', '2'), 'w'):
                pass
            await self.pipeline.measurement_stop()

            output_files = self.pipeline.get_output_files()

            self.assertIn('a/b/2', output_files)
            self.assertIn('1', output_files)

            for meta in output_files.values():
                self.assertEqual(meta['pipeline_mode'], 'mock')
                self.assertEqual(meta['cal_active'], True)


    async def test_not_return_old_files(self):
        """
        Should not return files created in data directory before emasurement start 
        """
        with tempfile.TemporaryDirectory(dir='/tmp') as d:
            await self.pipeline.set({"mode": "mock", "active": 1, 'data_root_directory': d})
            await self.pipeline.configure()
            self.pipeline._implementation.data_directory = d
            self.pipeline._implementation.old_files = set()
            self.pipeline._implementation.old_files.add('old')

            with open(os.path.join(d, 'old'), 'w'):
                pass

            await self.pipeline.capture_start()
            await self.pipeline.measurement_prepare()
            await self.pipeline.measurement_start()

            with open(os.path.join(d, '1'), 'w'):
                pass
            os.makedirs(os.path.join(d, 'a', 'b'))

            with open(os.path.join(d, 'a', 'b', '2'), 'w'):
                pass
            await self.pipeline.measurement_stop()

            output_files = self.pipeline.get_output_files()

            self.assertIn('a/b/2', output_files)
            self.assertIn('1', output_files)
            self.assertNotIn('old', output_files)




# mocks for implementation tzpar access
def mock_check_tzpar_repo(*args):
    tzpar_dir = os.path.join(data_dir, 'tz_pardir')
    return tzpar_dir

def mock_par_file_check(*args):
    tzpar_file = os.path.join(data_dir, 'tz_pardir', 'tzparfile')
    pulsar_flag = True
    return tzpar_file, pulsar_flag


unittest.mock.patch("mpikat.pipelines.pulsar_processing.PulsarBase._check_tzpar_repo", wraps=mock_check_tzpar_repo).start()
unittest.mock.patch("mpikat.pipelines.pulsar_processing.EddPulsarTimingPipeline._par_file_check", wraps=mock_par_file_check).start()


class Test_MKRH_Regression(unittest.IsolatedAsyncioTestCase):
    """
    Run a regression test on the creation of the mkrecv header. The saved
    versions are from the data as specified in the UTC_START field.
    """

    def setUp(self):
        self.cwd = os.getcwd()
        self.redis_db = testing.setup_redis()
        self.data_dir = tempfile.TemporaryDirectory()

        self.edd_data_store = datastore.EDDDataStore(port=self.redis_db.server_config['port'], host='127.0.0.1')
        _DEFAULT_CONFIG["data_store"] = {"port": self.redis_db.server_config['port'], 'ip': '127.0.0.1'}
        self.edd_data_store.setTelescopeDataItem("source-name", "B1937+21")
        self.edd_data_store.setTelescopeDataItem("ra", 123)
        self.edd_data_store.setTelescopeDataItem("dec", -45)
        self.edd_data_store.setTelescopeDataItem("observation-id",  "0000_0")
        self.edd_data_store.setTelescopeDataItem("receiver", "P217")
        self.edd_data_store.setTelescopeDataItem("project", "TEST")
        self.edd_data_store.setTelescopeDataItem("noise_diode_pattern", '{"period":0.5,"percentage":0.0}')

    def tearDown(self):
        self.redis_db.shutdown()
        self.data_dir.cleanup()
        os.chdir(self.cwd)

    async def _run_mkr_test(self, implementation, config):
        implementation._create_ring_buffer = mock.AsyncMock()
        current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        fname = os.path.join(current_dir, 'pulsar_mkr_header', config['mode'] + '.txt')
        expected_dada_header = read_mkrecv_header(fname)

        class KeyMock:
            def __init__(self, v):
                self.key = v
            async def destroy(self):
                pass

        implementation._dada_buffers = [KeyMock('dada'), KeyMock('dadc')]

        await implementation.configure()
        await implementation.measurement_prepare({})
        await PulsarBase.measurement_start(implementation)
        try:
            await implementation.measurement_stop()
        except mpikat.core.edd_pipeline_aio.StateChange:
            pass


        created_dada_header = read_mkrecv_header(implementation.dada_header_file.name)
        created_dada_header.pop("UTC_START")
        created_dada_header.pop("MJD_START")

        expected_dada_header.pop("UTC_START")
        expected_dada_header.pop("MJD_START")

        failed_keys = []
        for k, v in expected_dada_header.items():
            if created_dada_header[k] != v:
                failed_keys.append(f" Failed key: {k} - expected {v}, got {created_dada_header[k]}")

        self.assertFalse(failed_keys)

        # After emasurement start, also some sensors should be set
        self.assertEqual(implementation.sensors['target_name'].value, "B1937+21")

        self.assertEqual(implementation.sensors['_nbins'].value, config["nbins"])

        self.assertEqual(implementation.sensors['_central_freq'].value, str(config["input_data_streams"][0]['central_freq']))

        self.assertEqual(implementation.sensors['_nchannels'].value, str(config['nchannels']))

        await implementation.deconfigure()
            #self.assertEqual(created_dada_header[k], v, msg=)
        #self.assertEqual(created_dada_header, expected_dada_header)


    async def test_timing(self):
        config = updateConfig(_DEFAULT_CONFIG, {"mode":"Timing","dummy_input":True,

            "data_root_directory":  self.data_dir.name,
            "input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )

        await self._run_mkr_test(EddPulsarTimingPipeline(config), config)

    async def test_baseband(self):
        config = updateConfig(_DEFAULT_CONFIG, {"mode":"Baseband","dummy_input":True,
            "data_root_directory":  self.data_dir.name,
            "input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        await self._run_mkr_test(EddPulsarBasebandPipeline(config), config)

    async def test_search(self):
        config = updateConfig(_DEFAULT_CONFIG, {"mode":"Searching",
            "data_root_directory":  self.data_dir.name,
            "dummy_input":True, "input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        config['cdd_dm'] = 123
        implementation = EddPulsarSearchPipeline(config)
        await self._run_mkr_test(implementation, config)

    async def test_leap(self):
        config = updateConfig(_DEFAULT_CONFIG, {"mode":"Leap","dummy_input":True,
            "data_root_directory":  self.data_dir.name,
            "input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        await self._run_mkr_test(EddPulsarLeapPipeline(config), config)



class TestPulsarBase(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.data_dir = tempfile.TemporaryDirectory()
        self.redis_db = testing.setup_redis()
        cfg = copy.deepcopy(_DEFAULT_CONFIG)
        cfg.update( {"data_root_directory": self.data_dir.name,
            "data_store": {"port": self.redis_db.server_config['port'], "ip": '127.0.0.1'}
            }
                )
        self.implementation = PulsarBase(cfg)
        self.implementation._create_ring_buffer = mock.AsyncMock()
        self.cwd = os.getcwd()

    async def asyncSetUp(self):
        await self.implementation.configure()
    def tearDown(self):
        self.data_dir.cleanup()
        self.redis_db.shutdown()
        os.chdir(self.cwd)

    async def test_creation_of_default_data_directory(self):
        """If not specified, a default directory for output data should be created on measurement prepare"""
        await self.implementation.measurement_prepare({})
        await self.implementation.set_data_directory()
        self.assertTrue(os.path.isdir(self.implementation.data_directory))

    async def test_specifing_data_directory(self):
        """If specified, directory for output data should be created and used on measurement prepare"""
        await self.implementation.measurement_prepare({"data_directory": os.path.join(self.data_dir.name, "my", "data")})
        await self.implementation.set_data_directory()
        self.assertTrue(os.path.isdir(self.implementation.data_directory))
        self.assertTrue(self.implementation.data_directory.endswith('my/data'))

    async def test_existing_data_directory_with_files(self):
        """Already existing files should be listed in old_files"""

        with tempfile.TemporaryDirectory(dir=self.data_dir.name) as d:
            with open(os.path.join(d, '1'), 'w'):
                pass
            os.makedirs(os.path.join(d, 'a', 'b'))

            with open(os.path.join(d, 'a', 'b', '2'), 'w'):
                pass

            await self.implementation.measurement_prepare({"data_directory": d})
            await self.implementation.set_data_directory()
            self.assertIn(os.path.join(d, '1'), self.implementation.old_files)
            self.assertIn(os.path.join(d, 'a', 'b', '2'), self.implementation.old_files)


    async def test_keywords_data_directory(self):
        """If data directory contains keywords in {}, the variables should be
        retrieved from the data store and a path build accordingly"""

        self.edd_data_store = datastore.EDDDataStore(port=self.redis_db.server_config['port'], host='127.0.0.1')
        self.edd_data_store.setTelescopeDataItem("foo", "spam")
        self.edd_data_store.setTelescopeDataItem("bar", "eggs")
        self.edd_data_store.setTelescopeDataItem("date", "2022-11-09")

        await self.implementation.measurement_prepare({"data_directory": "{foo}/{bar}"})
        await self.implementation.set_data_directory()
        self.assertTrue(os.path.isdir(self.implementation.data_directory))
        self.assertTrue(self.implementation.data_directory.endswith('spam/eggs'))

        await self.implementation.measurement_prepare({"data_directory": "{year}/{month}/{day}"})
        await self.implementation.set_data_directory()
        self.assertTrue(os.path.isdir(os.path.join(self.data_dir.name, '2022', '11', '09')))


    async def test_unknown_keywords(self):
        """If a keyword is not found in the datastore, the string will be
        repalced by random characters to avoid loss of data"""

        await self.implementation.measurement_prepare({"data_directory": "{UNKNOWN_KEYWORD}"})
        await self.implementation.set_data_directory()
        self.assertTrue(os.path.isdir(self.implementation.data_directory))
        first_dir = self.implementation.data_directory

        # Second call should produce different dir 
        await self.implementation.set_data_directory()
        self.assertTrue(os.path.isdir(self.implementation.data_directory))
        self.assertNotEqual(first_dir, self.implementation.data_directory)


    async def test_header_keys(self):
        """keywords in the header should be also allowed"""

        await self.implementation.measurement_prepare({"data_directory": "{frequency_mhz}"})
        await self.implementation.set_data_directory({"frequency_mhz": 123})
        self.assertTrue(os.path.isdir(self.implementation.data_directory))
        self.assertTrue(self.implementation.data_directory.endswith('123'))

    async def test_header_key_precedence(self):
        """keywords in the header should take precedence over keywords in the data store"""

        self.edd_data_store = datastore.EDDDataStore(port=self.redis_db.server_config['port'], host='127.0.0.1')
        self.edd_data_store.setTelescopeDataItem("frequency_mhz", "spam")
        await self.implementation.measurement_prepare({"data_directory": "{frequency_mhz}"})
        await self.implementation.set_data_directory({"frequency_mhz": 123})
        self.assertTrue(os.path.isdir(self.implementation.data_directory))
        self.assertTrue(self.implementation.data_directory.endswith('123'))


    async def test_cal_active_check(self):
        """Cal is active if percentage is non zero, inactive otherwise"""
        edd_data_store = datastore.EDDDataStore(port=self.redis_db.server_config['port'], host='127.0.0.1')
        edd_data_store.setTelescopeDataItem("noise_diode_pattern", json.dumps({"percentage": 0.5}))
        self.assertTrue(self.implementation.check_cal_active())

        edd_data_store.setTelescopeDataItem("noise_diode_pattern", json.dumps({"percentage": 0.0}))
        self.assertFalse(self.implementation.check_cal_active())


class TestArchiveAdder(unittest.TestCase):
    def test_invalid_timezone(self):
        a = ArchiveAdder(None, 'NonExistingTimezone', "", 0)
        self.assertEqual(a.desired_timezone.tzname(0), 'UTC')


    def test_timezones(self):
        """
        Should not fail on different timezone calls
        """
        for tz in ['UTC', 'Europe/Berlin', 'Australia/Sydney']:
            a = ArchiveAdder(None, tz, "", 0)

    def test_conversion_to_datetime(self):
        """syscall should not fail with timezone object"""
        a = ArchiveAdder(None, 'UTC', "", 0)
        a._syscall = lambda x: x
        a._scrunch(None, None)


if __name__ == '__main__':
    unittest.main()
