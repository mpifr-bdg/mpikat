"""
"""
#Requires python 3.7+ and spead 3+

import asyncio
import copy
import json
import multiprocessing

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.core import logger
from mpikat.utils.spead_capture import SpeadCapture
from mpikat.pipelines.gated_spectrometer import GatedSpectrometerSpeadHandler, convert_to_redis_spectrum
import mpikat.utils.data_streams as ds
import mpikat.utils.numa as numa

_log = logger.getLogger("mpikat.pipelines.gs_plotter")

_DEFAULT_CONFIG = {
    "id":"gs_plotter",
    "type":"gs_plotter",
    "input_data_streams": [
        {
            "source": "gated_spectrometer_0:polarization_0_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.173",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.172",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.175",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.174",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.176",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.177",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_0",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.178",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_1",
            "hdf5_group_prefix": "S",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.179",
            "port": "7152"
        }
    ],
    "spead_config":copy.deepcopy(SpeadCapture.DEFAULT_CONFIG)
}


class GSPlotter(EDDPipeline):
    """
    Write EDD Data Streams to HDF5 Files.
    """
    def __init__(self, ip, port, loop=None):
        """
        Args:
            ip:   IP accepting katcp control connections from.
            port: Port number to serve on
        """
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)
        self._capture_thread = None
        self._data_plotter = None

    def setup_sensors(self):
        """
        Setup monitoring sensors
        """
        super().setup_sensors()


    # --------------------- #
    # State change routines #
    # --------------------- #
    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """Configures the HDF5Writer pipeline

        - Retrieves the address of the network interface card
        - Instantiates a Plotter object
        """
        _log.info("Configuring HDF5 Writer")
        nic_name, nic_description = numa.getFastestNic()
        _log.info("Capturing on interface %s, ip: %s, speed: %d Mbit/s",
                  nic_name, nic_description['ip'], nic_description['speed'])
        self._config['spead_config']['interface_address'] = nic_description['ip']
        self._config['spead_config']['numa_affinity'] = numa.getInfo()[nic_description['node']]['cores']

        qmanager = multiprocessing.Manager()
        self.plotting_queue = qmanager.Queue()

        self.redis_sender = ds.RedisJSONSender(self._config["data_store"]["ip"],
            self._config["data_store"]["port"], 2, self._config["id"],
            self.plotting_queue, callback=convert_to_redis_spectrum)
        self.redis_sender.connect()
        self.redis_sender.start()

        self._capture_thread = SpeadCapture(self._config["input_data_streams"],
            GatedSpectrometerSpeadHandler(),
            self._package_writer,
            self._config['spead_config'])

        _log.info("Final configuration:\n%s", json.dumps(self._config, indent=4))

    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """Starts the capturing of data
        """
        _log.info("Starting capture")
        self._capture_thread.start()


    @state_change(target="idle", allowed=["ready", "streaming", "deconfiguring"], intermediate="capture_stopping")
    async def capture_stop(self):
        """Stop the data/network capturing
        """
        self._stop_capturing()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """Deconfigures the pipeline
        """
        self._stop_capturing()

    def _stop_capturing(self):
        _log.debug("Stopping capturing")
        if self._capture_thread:
            self._capture_thread.stop()
            self._capture_thread.join(10.0)
            self._capture_thread = None
        _log.debug("Capture thread cleaned")


    def _package_writer(self, packet):
        """
        Writes a received heap to the writer queue and the data plotter
        Args:
            packet (dict): packet containing a heap
        """
        _log.trace("Writing package")
        self.plotting_queue.put(packet)



if __name__ == "__main__":
    asyncio.run(launchPipelineServer(GSPlotter))
