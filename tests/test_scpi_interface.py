# pylint:
# disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
import socket
import asyncio
import time

from mpikat.core import logger
from mpikat.utils.testing.mock_katcp_server import MockEDDMasterController
from mpikat.core.scpi_interface import EddScpiInterface
import mpikat.utils.async_util # pylint: disable=unused-import

from mpikat.utils import testing, get_port


_log = logger.getLogger("mpikat.tests.test_scpi_server")

host = '127.0.0.1'

class TestSCPIInterface(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.mock_edd_mc_thread = testing.launch_server_in_thread(
            MockEDDMasterController)
        self.mock_EDD = self.mock_edd_mc_thread.server

    def tearDown(self):
        self.scpi_server_task.cancel()
        self._osock.close()
        self.mock_edd_mc_thread.stop().result()
        self.mock_edd_mc_thread.join(timeout=5)

    async def asyncSetUp(self):  # pylint: disable=attribute-defined-outside-init

        self.port = get_port()
        self.scpi_interface = EddScpiInterface(
            "0.0.0.0", self.port, "127.0.0.1",
            self.mock_EDD.port, "0.0.0.0", 5000)
        self.scpi_server_task = asyncio.create_task(
            self.scpi_interface.start())

        # Set up socket for talking to the SCPI interface
        self._osock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._osock.connect((host, self.port))
        self._osock.settimeout(1.)

        # Wait until server is ready
        counter = 0
        while True:
            _log.debug("Trying SCPI connection %i", counter)
            try:
                r = self._osock.recv(8192)
            except ConnectionRefusedError:
                counter += 1
                if counter < 10:
                    await asyncio.sleep(.5)
                else:
                    break
                    # raise ConnectionRefusedError
            except socket.timeout:
                _log.debug("Successfully connected to scpi interface")
                break
        self._osock.settimeout(None)

    async def send_scpi(self, cmd, expect_fail=False):
        """ Send SCPI command and return reply. validates that the reply is valid SCPI"""
        cmde = cmd.encode('ascii')

        await asyncio.to_thread(self._osock.send, cmde)

        result = await asyncio.to_thread(self._osock.recv, 8192)
        result = result.decode('ascii')

        self.assertTrue(
            result.startswith(cmd),
            msg=f'Invalid SCPI by {cmd}: {result}')
        return result

    async def scpi_assert_ok(self, cmd):
        result = await self.send_scpi(cmd)
        self.assertEqual(cmd, result[:-27], msg=f'Error in {cmd}: {result}')

    async def scpi_assert_fail(self, cmd):
        result = await self.send_scpi(cmd)
        self.assertIn(
            'ERROR',
            result,
            msg=f'Error not reported in{cmd}: {result}')

    async def test_unknown_provision(self):
        """Unknown provision should fail"""
        await self.scpi_assert_fail('EDD:PROVISION')

    async def test_command_sequence(self):
        await self.scpi_assert_ok('EDD:PROVISION FOO')
        await self.scpi_assert_ok('EDD:CONFIGURE')
        # scpi interface waits, mock edd does not have complete state model
        self.mock_EDD.sensors['pipeline-status'].value = 'ready'
        await self.scpi_assert_ok('EDD:MEASUREMENTPREPARE "{}"')
        self.mock_EDD.sensors['pipeline-status'].value = 'set'
        await self.scpi_assert_ok('EDD:START')
        await self.scpi_assert_ok('EDD:STOP')
        await self.scpi_assert_ok('EDD:DECONFIGURE')
        await self.scpi_assert_ok('EDD:DEPROVISION')
        self.mock_EDD.mock.configure.assert_called_once()
        self.mock_EDD.mock.measurement_prepare.assert_called_once()
        self.mock_EDD.mock.measurement_start.assert_called_once()
        self.mock_EDD.mock.measurement_stop.assert_called_once()
        self.mock_EDD.mock.deconfigure.assert_called_once()
        self.mock_EDD.mock.deprovision.assert_called_once()

    async def test_command_fails(self):
        """Failing katcp requests should result in SCPI errors"""
        self.mock_EDD.fail = True
        await self.scpi_assert_fail('EDD:PROVISION FOO')
        await self.scpi_assert_fail('EDD:CONFIGURE')
        # scpi interface waits, mock edd does not have complete state model
        self.mock_EDD.sensors['pipeline-status'].value = 'ready'
        await self.scpi_assert_fail('EDD:MEASUREMENTPREPARE "{}"')
        await self.scpi_assert_fail('EDD:START')
        await self.scpi_assert_fail('EDD:STOP')
        await self.scpi_assert_fail('EDD:DECONFIGURE')
        # await self.scpi_assert_fail('EDD:DEPROVISION')

    async def test_missing_prepare(self):
        """ Measurement preapre should be automatically added"""
        self.mock_EDD.sensors['pipeline-status'].value = 'ready'
        await self.scpi_assert_ok('EDD:START')
        self.mock_EDD.mock.measurement_prepare.assert_called_once()

    async def test_rejection_of_invalid_json(self):
        self.mock_EDD.sensors['pipeline-status'].value = 'ready'
        await self.scpi_assert_fail('EDD:MEASUREMENTPREPARE ""')
        self.mock_EDD.mock.measurement_prepare.assert_not_called()

    async def test_wait_for_stop_finish_on_start(self):
        self.mock_EDD.sensors['pipeline-status'].value = 'stopping'
        task = asyncio.create_task(self.scpi_assert_ok('EDD:START'))
        await asyncio.sleep(1)
        self.mock_EDD.mock.measurement_start.assert_not_called()
        self.mock_EDD.sensors['pipeline-status'].value = 'ready'
        await task
        self.mock_EDD.mock.measurement_start.assert_called_once()

    async def test_min_delay_between_prepare_and_start(self):
        # There should be a minimum delay betweens tart and prepare, as for
        # a too quick start, the source name is not updated in time.
        self.mock_EDD.sensors['pipeline-status'].value = 'ready'
        t0 = time.time()
        await self.scpi_assert_ok('EDD:MEASUREMENTPREPARE "{}"')
        await self.scpi_assert_ok('EDD:START')
        t1 = time.time()
        self.assertGreater(t1 - t0, 3)


if __name__ == "__main__":
    unittest.main()
