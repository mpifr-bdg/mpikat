import spead2
import tempfile
import io
from typing import List
from dataclasses import asdict

from mpikat.core.logger import getLogger
from mpikat.utils.process_tools import ManagedProcess
from mpikat.utils.dada_tools import DadaClient, DadaHeader, DadaBaseHeader
from mpikat.core.data_stream import DataStreamRegistry, DataStream
from mpikat.utils.ip_utils import ips_from_stream_description
from mpikat.utils.process_tools import ManagedProcess

from .mkheader import MkItem, MkSendHeader, validate, get_item

_LOG = getLogger("mpikat.utils.mk_tools.mksend")

mksend_mapping = {
    "sample_rate":"sample_clock",
    "nic":"ibv_if",
    "bit_depth":"nbit",
}

def create_mksend_items(items: List[spead2.Item]) -> List[MkItem]:
    """Creates a list of MkSendItems

    Args:
        items (List[spead2.Item]): List of spead2.Items

    Returns:
        List[MkItem]: List of MksendItems
    """
    res: List[MkItem] = []
    for item in items:
        res.append(MkItem(name=item.name, addr=item.id))
    return res

def write_send_item(fid: io.TextIOWrapper, item: MkItem, num: int):
    """Writes an item to a opend file as it is expected by Mksend

    Args:
        fid (io.TextIOWrapper): The file pointer to the opened file
        item (MkItem): The item to write to the file
    """
    for key, val in asdict(item).items():
        if val is None:
            continue
        if key == "name":
            continue
        if key == "addr":
            key = "ID"
            val = f"{val} # {item.name}"
        fid.write(f"ITEM{num}_{key.upper()} {val}\n")
    fid.write("\n")


def write_send_header(header: DadaHeader, items: List[MkItem]=[], ignore_unset=True) -> str:
    """Creates a temporary file containing Mk header information

    Returns:
        str: The path of the file
    """
    fname = header.write(ignore_unset)
    with open(fname, mode="a") as f:
        if items:
            f.write("# Spead Items\n")
            for i, item in enumerate(items):
                write_send_item(f, item, i+1)
    return f.name


class Mksend(DadaClient):

    def __init__(self, streams: List[dict], key: str, physcpu: str="",
                 ibv: bool=True, mapping=mksend_mapping):
        """The Mkrecv wrapper object.

        Args:
            streams (List[dict]): The data streams from which information are taken
            key (str): The dada key to connect to
            physcpu (str, optional): The physical CPU to run the CLI on. Defaults to "".
            ibv (bool, optional): Use ibv or udp -> ibv for maximum performance. Defaults to True.
            mapping (dict, optional): A mapping dictionary allowing to translate streams
                to mkheader. Defaults to None.
        """
        super().__init__(key, physcpu=physcpu)
        self.mapping = mapping
        self.header = DadaHeader()
        self.header.add(DadaBaseHeader())
        self.header.add(MkSendHeader())

        if not isinstance(streams, list):
            streams = [streams]

        if not ibv:
            self.mapping["nic"] = "udp_if"

        self.header.update(streams[0], self.mapping)
        self.header.set("dada_key", key)

        self.formats: List[DataStream] = list(set(
            [DataStreamRegistry.get_class(stream["format"]) for stream in streams]))

        self.items: List[MkItem] = create_mksend_items(
            [item for sublist in [f.data_items for f in self.formats] for item in sublist])

        self.ip_list: List[str] = ips_from_stream_description(streams)


    def start(self, env: dict=None) -> ManagedProcess:
        """Start and return the ManagedProcess

        Args:
            env (dict, optional): Environmental variables to run the command with. Defaults to None.

        Returns:
            ManagedProcess: The running process
        """
        # Prepare MkItems
        self.items.sort(key=lambda x: x.addr)
        self.header.set("nitems", len(self.items))
        self.header.set("nsci", len([item for item in self.items if item.sci is not None]))
        if self.header.get("rate") == 0 and self.header.get("sample_clock") != "unset":
            self.header.set("rate", self.header.get("sample_clock") * 1.1)
        validate(self)
        self.header_file = write_send_header(self.header, self.items)
        self.cmd += f"mksend --quiet --header {self.header_file} {','.join(self.ip_list)}"

        _LOG.info("Starting Mksend with cmd: %s", self.cmd)
        return super().start(env)

    def set_item(self, name: str, key: str, val: str):
        """Sets the a property of an item by its name and key
            The property must exists in the Mkitem-dataclass

        Args:
            name (str): The item's name
            key (str): The key/property to set
            val (any): The value of the property
        """
        if key in ["mask", "modulo"]:
            _LOG.warning("Item property %s not used for mksend", key)
            return
        setattr(get_item(self.items, name), key, val)

    def set_header(self, key: str, val: any):
        """Sets an header parameter by given key-value pair

        Args:
            key (str): The name of the parameter to set
            val (any): The value to set
        """
        self.header.set(key, val)

