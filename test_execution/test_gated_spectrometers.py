from __future__ import print_function, division, unicode_literals
import logging
import asyncio

import unittest

from mpikat.pipelines.gated_dual_pol_spectrometer import GatedSpectrometerPipeline
from mpikat.pipelines.gated_full_stokes_spectrometer import GatedFullStokesSpectrometerPipeline

class TestGatedSpectrometer(unittest.IsolatedAsyncioTestCase):


    async def __test_sequence(self, pipeline):
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.set('{"nonfatal_numacheck":true,"dummy_input":true,"output_type":"null","samples_per_block":1048576,"fft_length":32768}')
        await pipeline.configure()
        self.assertEqual(pipeline.state, 'configured')

        await pipeline.capture_start()
        self.assertEqual(pipeline.state, 'streaming')

        # Ignore mesaurement start, stop prepare
        await pipeline.measurement_start()
        self.assertEqual(pipeline.state, 'streaming')

        await pipeline.measurement_stop()
        self.assertEqual(pipeline.state, 'streaming')

        await pipeline.measurement_prepare()
        self.assertEqual(pipeline.state, 'streaming')

        #await pipeline.capture_stop()
        #self.assertEqual(pipeline.state, 'idle')

        # This test needs to be available,as otherwise on successfull test
        # pycoverage wont exit
        await pipeline.deconfigure()
        self.assertEqual(pipeline.state, 'idle')

    async def test_FullStokes_sequence(self):
        pipeline = GatedFullStokesSpectrometerPipeline("localhost", 1234)
        await pipeline.set({"input_data_streams":{
            "polarization_0": {
                "format": "MPIFR_EDD_Packetizer:1", 
                "ip": "225.0.0.156+2", 
            }, "polarization_1": {
                "format": "MPIFR_EDD_Packetizer:1", 
                "ip": "225.0.0.156+2"
            }
        }})
        await self.__test_sequence(pipeline)

    #async def test_2Pol_sequence(self):
    #    pipeline = GatedSpectrometerPipeline("localhost", 1234)
    #    await self.__test_sequence(pipeline)


if __name__ == '__main__':
    logging.basicConfig(filename='debug.log',
        format=("[ %(levelname)s - %(asctime)s - %(name)s "
             "- %(filename)s:%(lineno)s] %(message)s"),
            level=logging.DEBUG)
    unittest.main()
