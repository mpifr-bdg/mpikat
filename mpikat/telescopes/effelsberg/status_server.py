import json
import asyncio
import time

from aiokatcp.sensor import Sensor
from multicastreceivers.AsyncZMQReceiver import AsyncZMQReceiver

from mpikat.core.telescope_meta_information_server import TMIServer

from mpikat.core.edd_pipeline_aio import getArgumentParser,\
    launchPipelineServer
from mpikat.core.datastore import EDDDataStore
from mpikat.utils.sensor_watchdog import conditional_update

import mpikat.core.logger

_log = mpikat.core.logger.getLogger('mpikat.telescopes.effelsberg.status_server')

sensor_map = {}
sensor_map['EffMeasurementInfo'] = {
        'source-name': lambda d: d['MEASUREMENT']['TARGET']['SOURCE_NAME']['VALUE'],
        'timestamp': lambda d: list(d['TIMESTAMPS'].values())[0],
        'focus': lambda d: d['MEASUREMENT']['TARGET']['ACTIVE_FOCUS']['VALUE'],
        'observing': lambda d: int(d['MEASUREMENT']['STATUS']['MEASUREMENT_IN_PROGRESS']['VALUE']),
        'timetostart': lambda d: int(d['MEASUREMENT']['SCHEDULE']['TIME_TO_START']['VALUE']),
        'time-remaining': lambda d: int(d['MEASUREMENT']['SCHEDULE']['TIME_TO_END']['VALUE']),
        'ra': lambda d: d['MEASUREMENT']['TARGET']['POSITION']['MEAN_EQ_2000']['LON']['VALUE'],
        'dec': lambda d: d['MEASUREMENT']['TARGET']['POSITION']['MEAN_EQ_2000']['LAT']['VALUE'],
        'project': lambda d: d['MEASUREMENT']['GENERAL']['PROJECT_ID']['VALUE'],
        'scannum': lambda d: d['MEASUREMENT']['CURRENT_SCAN_ID']['VALUE'],
        'subscannum': lambda d: d['MEASUREMENT']['CURRENT_SUBSCAN_ID']['VALUE'],
        'numsubscans': lambda d: d['MEASUREMENT']['SUBSCAN_COUNT']['VALUE'],
        'observation-id': lambda d: f"{d['MEASUREMENT']['CURRENT_SCAN_ID']['VALUE']}_{d['MEASUREMENT']['CURRENT_SUBSCAN_ID']['VALUE']}",
}

sensor_map['EffIntercomInfo'] = {
        'receiver': lambda d: d['INTERCOM']['ACTIVE_RECEIVER']['VALUE'],
        }

sensor_map['EffWeatherInfo'] = {
        'wind-speed': lambda d: d['WEATHER_INFORMATION']['AMBIENT']['WIND_SPEED']['VALUE'],
        'wind-direction': lambda d: d['WEATHER_INFORMATION']['AMBIENT']['WIND_DIRECTION']['VALUE'],
        'air-pressure': lambda d: d['WEATHER_INFORMATION']['AMBIENT']['AIR_PRESSURE']['VALUE'],
        'dew-point': lambda d: d['WEATHER_INFORMATION']['AMBIENT']['DEW_POINT']['VALUE'],
        'air-temperature': lambda d: d['WEATHER_INFORMATION']['AMBIENT']['TEMPERATURE']['TELESCOPE']['VALUE'],
        'humidity': lambda d: d['WEATHER_INFORMATION']['AMBIENT']['HUMIDITY']['TELESCOPE']['VALUE'],
        }



class EffelsbergStatusServer(TMIServer):
    """Handle telescope meta data information for Effelsberg by listening to the ZMQ stream"""
    VERSION_INFO = 'effelsberg-status-server-2.0'
    BUILD_INFO = 'effelsberg-status-server-2.0'

    def __init__(self, *args, **kwargs):

        self.receiver = AsyncZMQReceiver(topics=sensor_map.keys())
        self.receiver.MulticastAddress = '134.104.64.134'
        self.receiver.MulticastPort = 16052
        self.receiver._Coding = 'utf-8'

        self.update_events = {sg: asyncio.Event() for sg in sensor_map}

        super().__init__(*args, **kwargs)

        self.add_sensor(
            float, name="dew-point",
            description="On site dew point [deg C]",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            str, name="focus",
            description="Is the reciever at the primary or secondary focus?",
            default="",
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            str, name="timestamp",
            description="Timestamp associated with the telescope meta data",
            default="0000-00-00-00:00:00",
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            int, name="observing",
            description="Measurement in progress",
            default=42,
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            int, name="scannum",
            description="Number of the current scan",
            default=-1,
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            int, name="subscannum",
            description="Number of the current subscan",
            default=-1,
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            int, name="numsubscans",
            description="Number of the subscans in current scan",
            default=-1,
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            int, name="timetostart",
            description="",
            default=-1,
            initial_status=Sensor.Status.UNKNOWN)

        self.add_sensor(
            int, name="time-remaining",
            description="Time untile the scan ends",
            default=-1,
            initial_status=Sensor.Status.UNKNOWN)

        self.sensors.add(Sensor(bool, name="always_ensure_update",
            description="If true, ensure update will always return immediatly",
            default=False,
            initial_status=Sensor.Status.NOMINAL))


    async def request_toggle_always_ensure_update(self, _):
        """Toggles the ensure update behavior"""
        self.sensors['always_ensure_update'].value =  not self.sensors['always_ensure_update'].value


    async def sensor_update(self):
        """Task to Process data from he ZMQ receiver and update the sensors"""
        while True:
            _log.debug('Waiting for data ...')

            data = await self.receiver.Data
            timestamp = time.time()

            for k, v in data.items():
                _log.debug('Got data for %s', k)
                d = json.loads(v)
                _log.trace(json.dumps(v, indent=4))
                self.update_events[k].set()
                for sensor_name, coll in sensor_map[k].items():
                    try:
                        value = coll(d)
                        conditional_update(self.sensors[sensor_name], value, timestamp=timestamp)
                    except KeyError as E:
                        _log.error('Error processing sensor: %s', sensor_name)
                        _log.exception(E)


    async def ensure_update(self, cfg=None):
        # reset all events, then wait for all
        if self.sensors['always_ensure_update'].value:
          return

        starttime = time.time()
        _log.debug('Waiting for updates in all subscribed streams')
        for event in self.update_events.values():
            event.clear()
        # Wait at most 10 seconds for an update
        await asyncio.wait_for(asyncio.gather(*[event.wait() for event in self.update_events.values()]), 10.)
        _log.debug('Received all updates after %.2f s', time.time() - starttime)


    async def start(self):
        _log.debug("Starting server")
        await super().start()
        self.add_service_task(asyncio.create_task(self.receiver.run()))
        self.add_service_task(asyncio.create_task(self.sensor_update()))





async def main():
    """Wrapper to start the server"""
    parser = getArgumentParser()
    args = parser.parse_args()
    mpikat.core.logger.setLevel(args.log_level.upper())

    try:
        datastore = EDDDataStore(args.redis_ip, args.redis_port)
        datastore.ping()
    except ConnectionError:
        _log.warning('Datastore not avaialble at %s:%i - not updating redis!', args.redis_ip, args.redis_port)
        datastore = None
    server = EffelsbergStatusServer(
        args.host, args.port,
        datastore)
    await launchPipelineServer(server, args)



if __name__ == "__main__":
    asyncio.run(main())
