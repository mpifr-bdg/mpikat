"""
Pipeline to send spectrometer data to the Effelsberg fits writer (APEX Fits writer).
"""
import asyncio
import time
import copy
import json
from datetime import datetime

import numpy as np

from aiokatcp.sensor import Sensor

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.core import logger
from mpikat.utils.spead_capture import SpeadCapture, SpeadPacket
from mpikat.pipelines.gated_spectrometer import GatedSpectrometerSpeadHandler, Plotter
from mpikat.utils.sensor_watchdog import conditional_update
from mpikat.utils import fits, numa, get_host


_log = logger.getLogger("mpikat.pipelines.fits_interface")


# Artificial time delta between noise diode on / off status

# Timeout for socket operations [s]
_SOCKET_TIMEOUT = 12 # Ensure that we tmeout only after the Effelsberg Fits Writer

_DEFAULT_CONFIG = {
    "id":"fits_interface",
    "type":"fits_interface",
    "drop_nans": True,
    "input_data_streams":
    [
        {
            "source": "gated_spectrometer_0:polarization_0_0",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.173",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_1",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.172",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_0",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.175",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_1",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.174",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_0",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.176",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_0:polarization_0_1",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.177",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_0",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.178",
            "port": "7152"
        },
        {
            "source": "gated_spectrometer_1:polarization_1_1",
            "format": "GatedSpectrometer:1",
            "ip": "225.0.1.179",
            "port": "7152"
        }
    ],
    "output_data_streams":{
        "fits_spectrum" :
        {                                           # Dual info on polarization, as stream naming is arbitrary string, but pol field carries the information
            "source": "fits_interface:fits_spectrum",
            "format": "FitsGatedSpectrum:1",
            "ip":"0.0.0.0",
            "port":5002, 
        }
    },

    "output_bands": [           # output bands in the fits writer
        (1, None ),     # Full band except DC
#       (0, 128),    # First 128 channels
#       (100, 200),  # channels 100:200
#       (-128, None),  # last 128 channels
                ],

    "enable_plotting":True,
    "spead_config":copy.deepcopy(SpeadCapture.DEFAULT_CONFIG),
    "max_package_age":10,
    "keep_socket_alive":False,
    "fits_writer_ip":"0.0.0.0",
    "fits_writer_port":5002,
    "noise_time_delta":0.001  # Time added to noise diode on data for FitsWriter.
}



class FitsInterfaceServer(EDDPipeline):
    """
    Interface of EDD pipeliens to the Effelsberg FITS writer.

    Configuration
    -------------

    fits_writer_ip: "0.0.0.0"   - IP to accept Fitw Writer Connections from.
    fits_writer_port: 5002      - Port to listen for fits writer connections.
    drop_nans: True             - If true, any spectra containing a  NaM or Inf value will be dropped.


    """
    VERSION_INFO = ("spead-edd-fi-server-api", 1, 0)
    BUILD_INFO = ("spead-edd-fi-server-implementation", 0, 1, "")

    DATA_FORMATS = [fits.FitsGatedSpectrumDataStreamFormat.stream_meta]

    def __init__(self, ip, port, loop=None):
        """
        Args:
            ip:   IP accepting katcp control connections from.
            port: Port number to serve on
        """
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)
        self._fits_sender = None
        self._capture_thread = None
        self.data_plotter = Plotter()


    def setup_sensors(self):
        """
        Setup monitoring sensors
        """
        super().setup_sensors()

        self._fw_connection_status = Sensor(str,
            "fits-writer-connection-status",
            description="Status of the fits writer conenction",
            default="Unmanaged",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fw_connection_status)

        self._fw_packages_sent = Sensor(int,
            "fw-sent-packages",
            description="Number of packages sent to fits writer in this measurement",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fw_packages_sent)

        self._fw_packages_dropped = Sensor(int,
            "fw-dropped-packages",
            description="Number of packages dropped by fits writer in this measurement",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fw_packages_dropped)

        self._incomplete_heaps = Sensor(int,
            "incomplete-heaps",
            description="Incomplete heaps received.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._incomplete_heaps)

        self._complete_heaps = Sensor(int,
            "complete-heaps",
            description="Complete heaps received.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._complete_heaps)

        self._invalid_packages = Sensor(int,
            "invalid-packages",
            description="Number of invalid packages dropped.",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._invalid_packages)

        self._bandpass =  Sensor(str,
            "bandpass",
            description="band-pass data (base64 encoded)",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._bandpass)



    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """Configures the FitsInterface
        - Request and assign NIC
        - Start the connection manager
        - Instantiate the Plotter
        """
        _log.info("Configuring Fits interface")

        nic_name, nic_desc = numa.getFastestNic()
        _log.info("Capturing on interface %s, ip: %s, speed: %s Mbit/s",
                  nic_name, nic_desc['ip'], nic_desc['speed'])
        self._config['spead_config']['interface_address'] = nic_desc['ip']
        self._config['spead_config']['numa_affinity'] = numa.getInfo()[nic_desc['node']]['cores']

        cfs = json.dumps(self._config, indent=4)
        _log.info("Final configuration:\n" + cfs)

        if self._fits_sender is not None:
            _log.warning("Replacing fw_connection manager")
            self._fits_sender.stop()
            self._fits_sender.join()

        self._fits_sender = fits.Sender(
            self._config["fits_writer_ip"],
            self._config["fits_writer_port"],
            keep_socket_alive=self._config["keep_socket_alive"])
        self._config["output_data_streams"]["fits_spectrum"]["ip"] = get_host()
        self._config["output_data_streams"]["fits_spectrum"]["port"] = self._config["fits_writer_port"]
        self._fits_sender.start()


        if self._config["enable_plotting"]:
            self._plot_task = asyncio.create_task(self.data_plotter.consume_plots(self._bandpass))
        self._configUpdated()


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """Start the data capturing
        - Starts capturing data with the GS spead handler
        - Starts the plotting if enabled
        """
        _log.debug("Starting FITS interface capture")

        merger = FitsPacketMerger(self._fits_sender,
            self.data_plotter,
            spectra_per_package=len(self._config["input_data_streams"]) // 2,
            output_bands=self._config["output_bands"],
            noise_time_delta=self._config["noise_time_delta"],
            max_age=self._config['max_package_age'], 
            drop_invalid_packages=self._config['drop_nans'])
        self._capture_thread = SpeadCapture(self._config["input_data_streams"],
            GatedSpectrometerSpeadHandler(), merger, self._config['spead_config'])

        self._capture_thread.start()

        _log.debug("Done capture starting!")


    @state_change(target="measuring", allowed=['set'], intermediate="measurement_starting")
    async def measurement_start(self):
        """Starts the measurement by sending data to the fits writer
        """
        _log.info("Starting FITS interface data transfer as soon as connection is established ...")
        self._fits_sender.clear_queue()
        self._fits_sender.is_measuring.set()


    @state_change(target="ready", allowed=["measuring", "set"], ignored=['ready'], intermediate="measurement_stopping")
    async def measurement_stop(self):
        """Stops the measurement by dropping the connection to the FITS writer
        """
        _log.info("Stopping FITS interface data transfer")
        self._fits_sender.is_measuring.clear()
        counter = 0
        _log.info("Waiting for empting output queue")
        while not self._fits_sender.queue_is_empty():
            if counter > 15 * _SOCKET_TIMEOUT:
                _log.warning("Queue not empty after flush!")
                self._fits_sender.clear_queue()
                break
            await asyncio.sleep(0.1)
            counter += 1
        await asyncio.sleep(0.1)
        _log.info("Dropping connection.")
        self._fits_sender.disconnect()


    @state_change(target="idle", intermediate="capture_stopping")
    async def capture_stop(self):
        """Stops the capturing and plotting if enabled
        """
        _log.info("Capture stopping FitsInterface")
        self._stop_capturing()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """Deconfigures the pipeline
        """
        _log.info("Deconfiguring FitsInterface")
        if self.data_plotter.plotting:
            self._plot_task.cancel()
        self._stop_capturing()


    async def periodic_sensor_update(self):
        """
        Updates the katcp sensors.

        This is a periodic update as the connection are managed using threads and not coroutines.
        """
        while True:
            timestamp = time.time()
            if not self._fits_sender:
                conditional_update(self._fw_connection_status, "Unmanaged", timestamp=timestamp)
            elif not self._fits_sender.is_connected():
                conditional_update(self._fw_connection_status, "Unconnected", timestamp=timestamp)
            else:
                conditional_update(self._fw_connection_status, "Connected", timestamp=timestamp)
                conditional_update(self._fw_packages_sent, self._fits_sender.send_items, timestamp=timestamp)
                conditional_update(self._fw_packages_dropped, self._fits_sender.dropped_items, timestamp=timestamp)
            if self._capture_thread:
                conditional_update(self._incomplete_heaps, self._capture_thread.incomplete_heaps, timestamp=timestamp)
                conditional_update(self._complete_heaps, self._capture_thread.complete_heaps, timestamp=timestamp)
                conditional_update(self._invalid_packages, self._capture_thread._package_handler.invalidPackages, timestamp=timestamp)
            await asyncio.sleep(1)


    def _stop_capturing(self):
        if self.data_plotter.plotting:
            self.data_plotter.stop()
        if self._capture_thread:
            _log.debug("Cleaning up capture thread")
            self._capture_thread.stop()
            self._capture_thread.join()
            self._capture_thread = None
            _log.debug("Capture thread cleaned")
        if self._fits_sender:
            self._fits_sender.stop()
            self._fits_sender.join()
            self._fits_sender = None
            _log.debug("Connection manager thread cleaned")


    async def start(self):
        await super().start()
        task = asyncio.create_task(self.periodic_sensor_update())
        self.add_service_task(task)


    async def stop(self):
        """
        Handle server stop. Stop all threads
        """
        try:
            if self._fits_sender:
                self._fits_sender.stop()
                self._fits_sender.join(3.0)
            if self._capture_thread:
                self._capture_thread.stop()
                self._capture_thread.join(3.0)
        except Exception as E:
            _log.error("Exception during stop! {}".format(E))
        await super().stop()


class PrepPack: # pylint: disable=too-few-public-methods
    """
    Package in preparation.
    """
    def __init__(self, number_of_spectra, output_bands, reference_time):
        """

        """
        self._nspectra = number_of_spectra
        self.fits_packet = fits.FitsPacket()
        self.payload = fits.FitsPayload()

        self.counter = 0    # Counts spectra set in fw_pkg
        self.valid = True   # Invalid package will not be send to fits writer
        self.reference_time = reference_time
        self.output_bands = output_bands

    def addSpectrum(self, packet):
        """
        Includes packet with spead spectrum into the fw output package
        """
        self.counter += 1
        _log.debug("   This is {} / {} parts for reference_time: {:.3f}".format(self.counter, self._nspectra, packet.reference_time))

        # Copy data and drop DC channel - Direct numpy copy behaves weired as memory alignment is expected
        # but ctypes may not be aligned

        _log.debug("   Data (first 5 ch., including DC): {}".format(packet.data[:5]))
        packet.data /= (packet.number_of_input_samples + 1E-30)
        _log.debug("                After normalization: {}".format(packet.data[:5]))
        if np.isnan(packet.data).any():
            _log.debug("Invalidate package due to NaN detected")
            self.valid = False


        for bid, band in enumerate(self.output_bands):
            section = fits.FitsSection(data=packet.data[band[0]:band[1]])
            section.section_id = int(packet.polarization) * len(self.output_bands) + bid
            self.payload.add(section)


        if self.counter > self._nspectra:
            raise RuntimeError(f'Too many spectra added to preparation package. Expected only {self._nspectra}')

        if self.counter == self._nspectra:
            # Fill header fields + output data
            self.fits_packet.payload = self.payload

            def local_to_utc(t):
                """Convert timestamp to datetimestring in UTC"""
                secs = time.mktime(t)
                return time.gmtime(secs)

            dto = datetime.fromtimestamp(packet.reference_time)
            timestamp = time.strftime('%Y-%m-%dT%H:%M:%S', local_to_utc(dto.timetuple()))

            # Blank should be at the end according to specification
            timestamp += ".{:04d}UTC ".format(int((float(packet.reference_time) - int(packet.reference_time)) * 10000))
            self.fits_packet.header.timestamp = timestamp.encode('ascii')  # pylint: disable=attribute-defined-outside-init

            integration_time = packet.number_of_input_samples / float(packet.sampling_rate)

            self.fits_packet.header.backend_name = b"EDDSPEAD"  # pylint: disable=attribute-defined-outside-init
            # As the data is normalized to the actual nyumber of samples, the
            # integration time for the fits writer corresponds to the nominal
            # time.
            self.fits_packet.header.integration_time = int(packet.integration_period * 1000000) # in us pylint: disable=attribute-defined-outside-init

            self.fits_packet.header.blank_phases = int(2 - packet.noise_diode_status)  # pylint: disable=attribute-defined-outside-init
            _log.debug("Got all parts for reference_time {:.3f} - Finalizing".format(packet.reference_time))
            _log.debug("   Calculated timestamp for fits: %s", self.fits_packet.header.timestamp)
            _log.debug("   Integration period: %d s", packet.integration_period)
            _log.debug("   Received samples in period: %d", packet.number_of_input_samples)
            _log.debug("   Integration time: %d s", integration_time)
            _log.debug("   Noise diode status: %s, Blank phase: %d", packet.noise_diode_status, self.fits_packet.header.blank_phases)


class FitsPacketMerger:
    """
    Merge spectra to a package expected by the fits writer and pass completed
    packages to the outptu queue. Inclompelte pacakges above a max age will be dropped.
    """
    def __init__(self, fits_interface: fits.Sender, data_plotter: Plotter,
            spectra_per_package: int, output_bands: list, noise_time_delta: float, max_age: float=10.0,
                 drop_invalid_packages: bool=True):
        """_summary_

        Args:
            fits_interface (fits.Sender): _description_
            data_plotter (Plotter): _description_
            spectra_per_package (int): _description_
            noise_time_delta (float): _description_
            max_age (int, optional): _description_. Defaults to 10.
            drop_invalid_packages (bool, optional): _description_. Defaults to True.
        """
        self.__fits_interface = fits_interface
        self.__data_plotter = data_plotter
        self.__drop_invalid_packages = drop_invalid_packages
        self.invalidPackages = 0        # count invalid packages

        # Now is the latest checked package
        self.now = 0
        self.__max_age = max_age
        self._noise_time_delta = noise_time_delta

        self.__spectra_per_package = spectra_per_package
        self._output_bands = output_bands
        self.packages_in_preparation = {}


    def __call__(self, packet: SpeadPacket):
        """callback function handling spead heaps

        Args:
            packet (_type_): _description_
        """
        _log.debug("Merging data to FITS package")
        if packet.noise_diode_status != 1:  # pylint: disable=no-member
            packet.reference_time += self._noise_time_delta

        if self.__data_plotter.plotting:
            self.__data_plotter.addSnapshot(packet)

        # Update local time
        self.now = max(packet.reference_time, self.now)
        pp = self.add2prep_package(packet)
        if pp.counter == self.__spectra_per_package:
            self.finalize_prep_package(pp)
        else:
            # Package not done, (re-)add to preparation stash
            self.packages_in_preparation[packet.reference_time] = pp
            self.clean_queue()


    def add2prep_package(self, packet):
        """
        Adds the packet to the correct preparation package corresponding to received packet
        """

        if packet.reference_time not in self.packages_in_preparation:
            _log.trace("Creating new preparation packet")
            pp = PrepPack(self.__spectra_per_package, self._output_bands, packet.reference_time)
        else:
            _log.trace("Adding to existing preparation packet")
            pp = self.packages_in_preparation.pop(packet.reference_time)
        pp.addSpectrum(packet)
        return pp


    def finalize_prep_package(self, pp):
        """Handle the final package
        """
        # package is done. Send or drop
        if self.__drop_invalid_packages and not pp.valid:
            _log.warning("Package for reference time {} dropped because it contains NaN)!".format(pp.reference_time))
            self.invalidPackages += 1
        else:
            self.__fits_interface.put(pp.fits_packet)



    def clean_queue(self):
        """
        Delete pacakges that are too old
        """
        # Cleanup old packages
        tooold_packages = []
        _log.debug('Checking %d packages for age restriction', len(self.packages_in_preparation))
        for p in self.packages_in_preparation:
            age = self.now - p
            if age > self.__max_age:
                _log.warning("   Age of package {} exceeded maximum age {} - Incomplete package will be dropped.".format(age, self.__max_age))
                tooold_packages.append(p)
        for p in tooold_packages:
            self.packages_in_preparation.pop(p)


if __name__ == "__main__":
    asyncio.run(launchPipelineServer(FitsInterfaceServer))
