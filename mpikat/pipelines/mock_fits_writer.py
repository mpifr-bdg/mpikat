import asyncio

from mpikat.core.edd_pipeline_aio import EDDPipeline, state_change, launchPipelineServer, getArgumentParser
from mpikat.utils import fits
from mpikat.core import logger

_log = logger.getLogger("mpikat.pipelines.mock_fits_writer")

_DEFAULT_CONFIG = {
    "id": "MockFitsWriter",
    "type": "MockFitsWriter",
    "input_data_streams":[
        {
            "source": "fits_interface:fits_spectrum",
            "format": "FitsGatedSpectrum:1",
            "ip":"0.0.0.0",
            "port":5002
        }
    ],
    "fits_ip": "localhost",
    "fits_port": 5002,
    "output_directory":"/mnt"
}

class MockFitsWriter(EDDPipeline):
    def __init__(self, ip, port, loop=None):
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)
        self._clients = []

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Default method for configuration.
        """
        _log.info("Running configure.")

    @state_change(target="measuring", allowed=["set"], ignored=["streaming"], intermediate="measurement_starting")
    async def measurement_start(self):
        _log.info("MockFitsWriter starts measuring")
        for istream in self._config["input_data_streams"]:
            _log.info("Connecting fits.Reciever to %s:%d", istream["ip"], istream["port"])
            self._clients.append(fits.Receiver((istream["ip"], istream["port"]),
                                               self._config["output_directory"]))
            self._clients[-1].start()

    @state_change(target="ready", allowed=["measuring", "set"], ignored=['streaming', 'ready'], intermediate="measurement_stopping")
    async def measurement_stop(self):
        _log.info("MockFitsWriter stops measuring")
        for client in self._clients:
            client.stop()
            client.join(3)

    @property
    def client(self) -> fits.Receiver:
        return self._clients


if __name__ == "__main__":
    asyncio.run(launchPipelineServer(MockFitsWriter))
