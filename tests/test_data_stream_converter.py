import unittest
import numpy as np
import mpikat.utils.data_streams.converter as cv
from mpikat.core.logger import getLogger


LOG = getLogger("mpikat.dbbc.utils.spectrum_stream")

class Test_HelperFunctions(unittest.TestCase):
    """
    Test all helper functions implemented in dbbc.utils.redis_sender.py
    """

    def test_serialize_deserialze(self):
        """
        Should test if the serialization and deserialization
            of a np.ndarray to base64 works.
        """
        arr = np.random.rand(2048)
        enc = cv.serialize_array(arr)
        res = cv.deserialize_array(enc, dtype=arr.dtype)
        self.assertTrue(isinstance(enc, str))
        self.assertTrue(np.all(arr == res))


if __name__ == '__main__':
    unittest.main()
