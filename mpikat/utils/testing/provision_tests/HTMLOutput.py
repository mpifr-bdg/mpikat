
import time
from xml.etree import ElementTree as ET

from mpikat.core import logger
from . OutputProcessor import OutputProcessor
from . common import TestState, Stage

_log = logger.getLogger('mpikat.utils.testing.provision_tests.HTMLOutput')

class HTMLOutput(OutputProcessor):
    """
    Output test results to HTML document
    """
    def __init__(self):
        self.document = ET.Element('html')
        head = ET.Element('head')
        self.document.append(head)
        style = ET.Element('style')
        head.append(style)
        style.text = """
        .FAIL {color: red;}
        .ERROR {color: red;}
        .TBC {color: blue;}
        .OK   {color: green;}
        .table_sub_heading   {font-weight: bold;}
        .list_key {font-family: sans-serif;}
        .list_val {font-family: mono;}
        """

        self.body = ET.Element('body')
        self.document.append(self.body)

        self.stage_div = None

    def _text_el(self, eltype, eltext, elclass=None):
        """
        Create an element of a given type and add text to it.
        """
        if elclass:
            el = ET.Element(eltype, attrib={'class': elclass})
        else:
            el = ET.Element(eltype)
        el.text = eltext
        return el

    def to_string(self):
        """
        Return string representation of HTMl document
        """
        return ET.tostring(self.document).decode('utf-8')

    def print_head(self, summary):
        inventory_data = summary['inventory_data']
        stage_parameters = summary['stage_parameters']
        h1 = ET.Element('h1')
        h1.text = f"EDD Provisioning Test Result for: {stage_parameters[Stage.provision]}"
        self.body.append(h1)

        table = ET.Element('table')
        self.body.append(table)
        def add_row(*args):
            row = ET.Element('tr')
            for i, a in enumerate(args):
                row.append(self._text_el('td', a, ['list_key', 'list_val'][i % 2]))
            return row

        table.append(add_row('Time of report:', f'{time.ctime()}', 'Total duration of test execution:', f"{summary['duration']:.1f} s"))
        table.append(add_row('Inventory:', f"{inventory_data['edd_inventory_folder']}", 'Version tag:', f"{inventory_data['version_tag']}"))

        table.append(add_row('Pipeline Versions:'))
        for k,v  in summary['pipeline_versions'].items():
            table.append(add_row(k, v))

        table.append(add_row('Configuration Graph:'))
        ET.register_namespace("","http://www.w3.org/2000/svg")
        try:
            svg = ET.fromstring(summary['configuration_graph'])
        except ET.ParseError:
            svg = ET.Element('div')
            svg.append(self._text_el('span', f'No Graph produced!'))

        self.body.append(svg)




    def print_test_result(self, test):
        res = []
        res.append(self._text_el('td', test.state.name, test.state.name))

        res.extend([self._text_el('td', f'{test.name}'), self._text_el('td', f'{test.duration:.1f}')])
        return res

    def print_test_output(self, test):
        res = []

        for o in test.output:
            _log.debug('Adding output')
            row = ET.Element('tr')
            row.append(self._text_el('td', ''))

            td = ET.Element('td')
            if o.startswith('iVBOR'):
                _log.debug('Output is an image')
                img = ET.Element('img', attrib={'src': f"data:image/png;base64,{o}"})
                td.append(img)
            else:
                _log.debug('Output is not an image')
                pre = ET.Element('pre')
                pre.text = f'{o}'
                td.append(pre)
            row.extend([td, self._text_el('td', f'')])
            res.append(row)
        return res

    def print_stage(self, summary):
        _log.debug('HTMLOutput: Printing stage')
        h2 = ET.Element('h2')
        self.body.append(h2)
        h2.text = f"Stage: {summary.stage.name}"

        div = ET.Element('div')
        self.body.append(div)
        div.append(self._text_el('span', f' {summary.stats[TestState.FAIL] + summary.stats[TestState.ERROR]} / {summary.n_tests} failed. {summary.stats[TestState.TBC]} require manual check.'))
        div.append(self._text_el('span', f' Total duration for stage: {summary.duration:6.1f} s'))

        table = ET.Element('table')
        div.append(table)
        row = ET.Element('tr')
        row.append(self._text_el('th', 'Result'))
        row.append(self._text_el('th', 'Test Name'))
        row.append(self._text_el('th', 'Test Duration [s]'))
        table.append(row)

        for pipeline in summary.results:
            row = ET.Element('tr')
            row.append(self._text_el('td', pipeline, 'table_sub_heading'))
            table.append(row)
            for test in summary.results[pipeline]:
                row = ET.Element('tr')
                row.extend(self.print_test_result(test))
                table.append(row)
                table.extend(self.print_test_output(test))
