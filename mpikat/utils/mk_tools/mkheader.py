from dataclasses import dataclass
from typing import List
from mpikat.utils import numa
from mpikat.core.logger import getLogger

_LOG = getLogger("mpikat.utils.mk_tools.mkheader")

@dataclass
class MkItem():
    """A dataclass representing the properties of a Mkitem
    """
    # The item's name
    name: str = ""
    # The item's address - Only used for Mksend
    addr: int = None
    # The item's index for the placement
    index: int = None
    # The item list (range or string)
    list: str = None
    # The masking of the item allows filtering (hex string)
    mask: str = None
    # Modulo allows further filtering (Only valid for IDX1 - first Mkrecv item)
    modulo: int = None
    # The step size of the item (Only valid for IDX1 - first Mkrecv item)
    step: int = None
    # The side-channel index - Only used for Mksend
    sci: int = None

# pylint: disable=too-many-instance-attributes
@dataclass
class MkBaseHeader:
    """A data class representing the properties of a MkHeader
    """
    # ----------------- #
    # MK CLI parameters #
    # ----------------- #
    # dada mode (0 = artificial data, 1 = data from dada ringbuffer)
    dada_mode: int = "unset"
    # Maximum packet size to use for UDP
    packet_size: int = 8400
    # Network buffer size. System settings can be tuned with "sysctl net.core.wmem_max=NUMBER" and "sysctl net.core.rmem_max="
    buffer_size: int = 16777216
    # The heap size. Usually the byte size of the payload data (n samples * bytes size)
    # heap_size: int = 0
    # Number of heap groups (heaps with the same timestamp) going into the data space. (optional)
    ngroups_data: int = "unset"
    # The ADC sync epoch - used for time stamping of the data
    sync_time: int = "unset"
    # virtual sample clock used for calculations
    sample_clock: int = "unset"
    # The start of the sample counter, is set by mkrecv. The first received dictates the START
    sample_clock_start: int = "unset"
    # Interface address for ibverbs
    ibv_if: str = "unset"
    # Interface address for UDP
    udp_if: str = "unset"
    # Interrupt vector (-1 for polled)
    ibv_vector: int = -1
    # Maximum number of times to poll in a row
    ibv_max_poll: int = 10
    # The port on which to capture data
    port: int = "unset"
    # Number of worker threads
    nthreads: int = 1
    # Maximum number #of active heaps
    nheaps: int = 64

# pylint: disable=too-many-instance-attributes
@dataclass
class MkSendHeader(MkBaseHeader):
    # ---------------- #
    # Mksend specifics #
    # ---------------- #

    # Use non_temporal memcpy
    memcpy_nt: any = "unset"
    # Number of slots send via network during simulation, 0  means infinite.
    nslots: int = 0
    # network mode (0 = no network, 1 = full network support)
    network_mode: int = 1
    # Maximum number #of hops
    nhops: int = 1
    # Network use #rate
    rate: float = .0
    # Size of a network burst [Bytes]
    burst_size: int = 65536
    # ?
    burst_ratio: float = 1.05
    # The heap size. Usually the byte size of the payload data (n samples * bytes size)
    heap_size: int = 0
    # First used heap id
    heap_id_start: int = 0
    # offset of heap id (different streams)
    heap_id_offset: int = 0
    # difference between two heap ids (same stream)
    heap_id_step: int = 1
    # number of consecutive heaps going into the same stream
    heap_group: int = 1
    # Number of #item pointers in the side_channel
    nsci: int = 0
    # Number of #item pointers in a SPEAD heap
    nitems: int = 0



# pylint: disable=too-many-instance-attributes
@dataclass
class MkRecvHeader(MkBaseHeader):
    # ---------------- #
    # Mkrecv specifics #
    # ---------------- #

    # NUmber of open dada slots to put heaps in
    dada_nslots: int = 3
    # Number of dada slots to skip at the beginning
    slots_skip: int = 0
    # Number of items used for computing the position of the heap in the dada slot
    nindices: int = 0
    # Byte size of a single dada slot
    slot_nbytes: int = "unset"
    # Payload size of a heap
    heap_nbytes: int = "unset"
    # ?
    level_data: any = "unset"
    # Number of SCI items to put into the dada slot. Dada buffer must be sized accordingly
    sci_list: any = "unset"
    # Number of multicast groups per stream
    multi_stream_threads: any = "unset"
    # Auxiliary thread core (main, header creation)
    aux_thread_core: any = "unset"

def get_item(items: List[MkItem], name: str) -> MkItem:
    """Returns an item from a list that matches the name

    Args:
        name (str): The name to search for

    Returns:
        MkItem: The item to return
    """
    for item in items:
        if item.name == name:
            return item
    _LOG.error("No item exists with name %s", name)
    return None

def validate(mk_object):
    """_summary_

    Args:
        mk_object (Mkrecv or Mksend): The mk object to validate
    """
    d = mk_object.header.asdict()
    nics = []
    for node in numa.getInfo().keys():
        __, n = numa.getFastestNic(node)
        nics.append(n["ip"])

    if mk_object._physcpu == "" or d["nthreads"] == 1:
        _LOG.warning("Mk process runs on single core, may lead to performance issues")
    if d[mk_object.mapping["nic"]] not in nics:
        _LOG.warning("NIC %s specified for %s, you are not using the fastest NIC",
                        d[mk_object.mapping["nic"]], mk_object.mapping["nic"])
    if len(mk_object.ip_list) == 0:
        _LOG.error("Multicast address list is empty")
    if len(mk_object.items) < 1:
        _LOG.error("At least one index item is required")
    if d["dada_key"] == "unset":
        _LOG.error("No DADA key specified")
