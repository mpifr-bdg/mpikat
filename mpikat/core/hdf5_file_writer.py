import datetime
import os
import string
from threading import Lock
import multiprocessing as mp
import queue

import h5py
import numpy as np
import mpikat.core.version
import mpikat.core.logger
import mpikat.utils.output

_log = mpikat.core.logger.getLogger("mpikat.core.HDF5FileWriter")


def WriterProcess(fname: str, stop_event: mp.Event(), write_queue: mp.JoinableQueue, new_subscan: bool, chunksize: any='auto'):
    """
    Handle actual file write + conversion to HDF5 Layout. new items are put to an internal queue.

    Args:
        fname:          Name of the HDF5 File to write to
        stop_event:     Event used to stop the process
        write_queue:    Queue of data
        attributes:     Attributes added to the file
        new_subscan:    Create a new subscan if True
    """
    _log = mpikat.core.logger.getLogger("mpikat.hdf5_file_writer.WriterProcess")
    _log.debug("Starting writing data to queue")
    output_file = HDF5FileWriter(filename=fname, chunksize=chunksize)

    if new_subscan:
        output_file.newSubscan()

    #output_file.writeAttributes(config["attributes"])

    _log = mpikat.core.logger.getLogger("mpikat.hdf5pipeline.WriterSubprocess")
    _log.debug('Starting writing process')
    while True:
        try:
            data = write_queue.get(timeout=1)
        except queue.Empty:
            if not stop_event.is_set():
                continue
            break
        output_file.addData(data[0], data[1], )
        write_queue.task_done()

    output_file.flush()
    output_file.close()
    _log.debug('Finished writing process')


class HDF5FileWriter:
    """
    A HDF file writer for EDD backend output.
    """
    _file_format_version = 1      # Version of the format (key naming schema) written to the HDF5 files

    def __init__(self, filename=None, path=None, file_id_no=None, mode='a', chunksize='auto'):
        """
        Args;
            filename:
                If specified this filename will be used. Otherwise a unique
                filename will be generated automatically based on the current
                time.
            path:
                path to use to create the file. Current working directory if None of filename is specified.
            file_id_no:
                If specified this will be used in the automatic file name.
                Otherwise this will be a random string.
            mode:
                w        Create file, truncate if exists
                w- or x  Create file, fail if exists
                a        write if exists, create otherwise (default)
            chunksize:
                Number of data blocks that are hold in memory and written to
                file at once. This also defines the chunksize of the actual HDF
                file.
                Auto uses hdf5 auto chunk size selection.
                Preliminary test showed ~ 25% performance in write spead with
                manual chunk size set to 8 spectra compared to auto.

        ToDo:
            Performance: Tune chunk cache size
            Performance: Check memory offsets + alignment? Likely auto tuned and already good
        """

        if path is None:
            path = os.getcwd()

        # Use full path for file
        if not filename:
            filename = mpikat.utils.output.create_file_name(path, 'hdf5', file_id_no)

        if path is None:
            path = os.path.dirname(filename)
        self.__filename = os.path.join(path, filename)    # join does NOT duplicate the path.

        _log.debug('Using file: {}'.format(self.__filename))
        self.__chunksize = chunksize

        self._file = h5py.File(self.__filename, mode)
        self._file.attrs['FORMAT_VERSION'] = self._file_format_version

        now = datetime.datetime.now(datetime.UTC).strftime("%Y-%m-%dT%H:%M:%S.%f")

        # Version string need to accommodate sh256 has + data + comment on
        # modifications.
        a = np.array((now, "mpikat.HDF5Writer", mpikat.core.version.VERSION, ""),
                     dtype=[('processingtime', '|S23'),
                            ('software', '|S256'),
                            ('version', '|S128'),
                            ('comment', '|S256')])
        if 'history' in self._file:
            history = self._file['history']
            shape = list(history.shape)
            shape[0] += 1
            history.resize(tuple(shape))
        else:
            history = self._file.create_dataset("history", dtype=a.dtype, shape=(1,) + a.shape, maxshape=(None,) + a.shape)
        history[-1] = a

        self.__subscan = None

        self.__items = []

        self._lock = Lock()

    def __del__(self):
        self._file.close()

    @property
    def filename(self):
        """
        Return full file name of the HDF5 file
        """
        return self.__filename

    def getFileSize(self):
        """
        Returns current size of file in bytes.
        """
        return os.path.getsize(self.__filename)

    def newSubscan(self):
        """
        Starts a new subscan.
        """
        with self._lock:
            if "scan" in self._file:
                scannum = len(self._file['scan'].keys())
            else:
                scannum = 0

            scanid = "scan/{:03}".format(scannum)
            _log.debug('Starting new subscan: {}'.format(scanid))
            self._file.create_group(scanid)


    def getCurrentScanId(self):
        """
        Return current scan ID string
        """
        return  "scan/{:03}".format(len(self._file['scan'].keys()) - 1)


    def addData(self, section, data, attributes=None):
        """
        Add data block to a section of the current subscan.

        Args:
            section (str): Name of the section
            data (dict):
                data[did] needs to return the data for did in the selected format.
            attributes (dict):
                First occurrence of any key will be added as attribute to the dataset.

        It is assumed that the first data set is complete. Subsequent datasets with missing items are ignored.

            ToDo:
               Format management
        """
        if attributes is None:
            attributes = {}

        with self._lock:
            __subscan = self._file.get(self.getCurrentScanId())
            if section not in __subscan:
                _log.debug('Creating new section {} for subscan: {}'.format(section, __subscan.name))
                __subscan.create_group(section)
                for k, c in data.items():
                    if self.__chunksize == 'auto':
                        __subscan[section].create_dataset(k, dtype=c.dtype, shape=(0,) + c.shape, maxshape=(None,)+ c.shape, chunks=True)
                    else:
                        __subscan[section].create_dataset(k, dtype=c.dtype, shape=(0,) + c.shape, maxshape=(None,)+ c.shape, chunks=(self.__chunksize, )+ c.shape, )
                self.__items = set(data.keys())

            if set(data.keys()) != self.__items:
                _log.warning("Missing keys in dataset: {} - Ignoring dataset!".format(",".join(self.__items.difference(data.keys()))))
                return

            for did, dataset in __subscan[section].items():
                shape = list(dataset.shape)
                shape[0] += 1
                _log.debug('Resizing {}: {} -> {}'.format(dataset.name, dataset.shape, tuple(shape)))
                dataset.resize(tuple(shape))
                dataset[-1] = data[did]

            for key, value in attributes.items():
                if key not in __subscan[section].attrs.keys():
                    _log.debug("Adding attribute: {} = {} to section {}".format(key, value, section))
                    __subscan[section].attrs[key] = value

    def open(self, mode='a'):
        """
        Re-Opens the HDF file.
        """
        _log.debug('Opening : {}'.format(self.filename))
        self._file.close()
        self._file = h5py.File(self.__filename, mode)

    def is_open(self):
        """
        Returns True if the file is open, False otherwise
        """
        return bool(self._file)


    def close(self):
        """
        Closes the HDF File
        """
        _log.debug('Closing: {}'.format(self.filename))
        self._file.close()

    def flush(self):
        """
        Flush the HDF Buffers.
        """
        with self._lock:
            self._file.flush()
