import asyncio
import aiokatcp
from mpikat.core.logger import getLogger

_log = getLogger('mpikat.core.KATCPclient')

class KATCPClient(aiokatcp.Client):
    """
    KATCP client with optional timeout argument for connect and request.
    """
    async def request(self, *args, **kwargs):
        timeout = kwargs.pop('timeout', None)
        _log.trace("Sending reqeust %s, %s with timeout %s s", args, kwargs, timeout)

        req_task = super().request(*args, **kwargs)
        if timeout:
            result = await asyncio.wait_for(req_task, timeout=timeout)
        else:
            result = await req_task
        _log.trace("Received result %s from request %s %s", result, args, kwargs)
        return result

    @classmethod
    async def connect(cls, *args, **kwargs):
        timeout = kwargs.pop('timeout', None)

        task = super().connect(*args, **kwargs)
        if timeout:
            return await asyncio.wait_for(task, timeout=timeout)
        return await task
