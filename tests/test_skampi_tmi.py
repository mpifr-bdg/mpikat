# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest

import asyncio

import aiokatcp
from aiokatcp.client import Client

from mpikat.utils import testing, get_port
from mpikat.core.datastore import EDDDataStore
from mpikat.telescopes.skampi.ska_protodish_status_server import  SKAMPIStatusServer

HOST = "127.0.0.1"




class Test_SkampiTMI(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.redis = testing.setup_redis()
        self.datastore = EDDDataStore(host=HOST, port=self.redis.server_config['port'])

    async def test_ensure_update(self):
        tmi = SKAMPIStatusServer(HOST, 0)

        cfg= {
            "observation_config": {
                "band": "foo",
                "scans": [{"ra": 123, "dec": 456, "source_name": "arakis"}, {}]
                },
            "scannumber": 0
            }

        await tmi.ensure_update(cfg)

        self.assertEqual(tmi.sensors['receiver'].value, 'foo')
        self.assertEqual(tmi.sensors['ra'].value, 123)
        self.assertEqual(tmi.sensors['dec'].value, 456)
        self.assertEqual(tmi.sensors['source-name'].value, "arakis")

        cfg['scannumber'] = 1

        await tmi.ensure_update(cfg)

        self.assertEqual(tmi.sensors['receiver'].value, 'foo')
        self.assertNotEqual(tmi.sensors['ra'].value, 123)
        self.assertNotEqual(tmi.sensors['dec'].value, 456)
        self.assertNotEqual(tmi.sensors['source-name'].value, "arakis")

    async def test_ensure_update_missing_obs_config(self):
        """
        ensure update should not fail when observation config is missing.
        """
        tmi = SKAMPIStatusServer(HOST, 0)

        cfg= {
            "observation_config": {
                "band": "foo",
                "scans": [{"ra": 123, "dec": 456, "source_name": "arakis"}, {}]
                },
            "scannumber": 0
            }

        await tmi.ensure_update(cfg)

        self.assertEqual(tmi.sensors['receiver'].value, 'foo')
        self.assertEqual(tmi.sensors['ra'].value, 123)
        self.assertEqual(tmi.sensors['dec'].value, 456)
        self.assertEqual(tmi.sensors['source-name'].value, "arakis")

        await tmi.ensure_update({})
        self.assertNotEqual(tmi.sensors['receiver'].value, 'foo')
        self.assertNotEqual(tmi.sensors['ra'].value, 123)
        self.assertNotEqual(tmi.sensors['dec'].value, 456)
        self.assertNotEqual(tmi.sensors['source-name'].value, "arakis")

        await tmi.ensure_update(None)
        self.assertNotEqual(tmi.sensors['receiver'].value, 'foo')
        self.assertNotEqual(tmi.sensors['ra'].value, 123)
        self.assertNotEqual(tmi.sensors['dec'].value, 456)
        self.assertNotEqual(tmi.sensors['source-name'].value, "arakis")



async def test_ensure_update_missing_scan(self):
        """
        ensure update should not fail when scan is missing.
        """
        tmi = SKAMPIStatusServer(HOST, 0)

        cfg= {
            "observation_config": {
                "band": "foo",
                },
            }

        await tmi.ensure_update(cfg)

        self.assertNotEqual(tmi.sensors['receiver'].value, 'foo')
        self.assertNotEqual(tmi.sensors['ra'].value, 123)
        self.assertNotEqual(tmi.sensors['dec'].value, 456)
        self.assertNotEqual(tmi.sensors['source-name'].value, "arakis")




if __name__ == '__main__':
    unittest.main()
