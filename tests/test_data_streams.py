# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest

from mpikat.core.data_stream import DataStream, DataStreamRegistry, edd_spead, convert48_64, convert64_48


class TestStream(DataStream):
    """
    test data stream definition
    """
    data_items = [
        edd_spead(123, 'foo'),
        edd_spead(456, 'bar'),
        edd_spead(23, 'dynamic', shape=())
            ]

    stream_meta = {"type": "TEST_STREAM:1", "foo": 23}


    @classmethod
    def ig_update(cls, items, ig):
        """
        Update first time dynamic shape with value from foo
        """
        if 'dynamic' in ig:
            return False

        i = cls.get_data_item(name='dynamic')
        i.shape = (items['foo'].value, )

        cls.add_item2group(i, ig)
        return True



class TestDataStream(unittest.TestCase):
    def test_get_data_item(self):
        self.assertEqual(TestStream.get_data_item(id=123).id, 123)
        self.assertEqual(TestStream.get_data_item(id=123).name, 'foo')
        self.assertEqual(TestStream.get_data_item(id=456).id, 456)
        self.assertEqual(TestStream.get_data_item(id=456).name, 'bar')

        with self.assertRaises(ValueError):
            TestStream.get_data_item(id=789)

        with self.assertRaises(ValueError):
            TestStream.get_data_item(description='wtf')

    def test_create_item_group(self):
        # spead2 item group should contain all items with shape
        ig = TestStream.create_item_group()
        self.assertEqual(len(ig.ids()), 2)

        # spead2 item group should be updated dynamically on first call
        ig['foo'].value = 3
        self.assertTrue(TestStream.ig_update(ig, ig))
        self.assertEqual(TestStream.get_data_item(name='dynamic').shape, (3,))
        self.assertEqual(len(ig.ids()), 3)
        self.assertFalse(TestStream.ig_update(ig, ig))

    def test_validate_items(self):
        items = {d.name: d for d in TestStream.data_items}

        self.assertTrue(TestStream.validate_items(items))
        items.pop('bar')
        self.assertFalse(TestStream.validate_items(items))




class TestDataStreamRegistry(unittest.TestCase):
    def tearDown(self):
        DataStreamRegistry.clear()

    def test_register_by_class(self):
        DataStreamRegistry.register(TestStream)
        f = DataStreamRegistry.get("TEST_STREAM:1")
        self.assertEqual(f['foo'], 23)

    def test_register_by_meta(self):
        DataStreamRegistry.register(TestStream.stream_meta)
        f = DataStreamRegistry.get("TEST_STREAM:1")
        self.assertEqual(f['foo'], 23)

    def test_format_exceptions(self):
        with self.assertRaises(RuntimeError):
            DataStreamRegistry.register("foo")


class TestConverter(unittest.TestCase):
    def testconvs(self):
        N = 1108152157446
        self.assertEqual(convert48_64(convert64_48(N)), N)


if __name__ == '__main__':
    unittest.main()
