#!/usr/bin/python3
"""
Command-line application to control digitizer/packetizers via katcp.
"""

import asyncio
import aiokatcp
from argparse import ArgumentParser

from mpikat.core.digpack_client import DigitiserPacketiserClient
from mpikat.core import logger

# Convenience settings of known packetizers. Should probably go to a config
# file
__known_packetizers = {"faraday_room": {"ip": "134.104.73.132", "port": 7147},
                       "focus_cabin": {"ip": "134.104.70.65", "port": 7147}}

_log = logger.getLogger("mpikat.edd_digpack_cli")


async def main():
    parser = ArgumentParser(
        description="Configures edd digitizer. By default, send synchronize and capture start along with the given options.")
    parser.add_argument('host', type=str,
                        help='Digitizer to bind to, either ip or one of [{}]'.format(
                            ", ".join(__known_packetizers)))
    parser.add_argument('-p', '--port', dest='port', type=int,
                        help='Port number to bind to', default=7147)
    parser.add_argument('--nbits', dest='nbits', type=int,
                        help='The number of bits per output sample')
    parser.add_argument('--sampling-rate', dest='sampling_rate', type=float,
                        help='The digitizer sampling rate (Hz)')
    parser.add_argument('--v-destinations', dest='v_destinations', type=str,
                        help='V polarisation destinations')
    parser.add_argument('--h-destinations', dest='h_destinations', type=str,
                        help='H polarisation destinations')

    parser.add_argument('--interface-addresses', nargs=2, dest='interface_addresses', type=str,
                        help='Set IP of the sender nics')
    parser.add_argument('--mac-addresses', nargs=2, dest='mac_addresses', type=str,
                        help='Set MAC of the sender nics')

    parser.add_argument('--log-level', dest='log_level', type=str,
                        help='Logging level', default="INFO")
    parser.add_argument('--predecimation-factor', dest='predecimation_factor', type=int,
                        help='Predecimation factor')
    parser.add_argument('--sync', dest='synchronize', action='store_true',
                        help='Send sync command.')
    parser.add_argument('--no-capture-start', dest='capture_start', action='store_false',
                        help='Do not send capture start command.')
    parser.add_argument('--sync-time', dest='sync_time', type=int,
                        help='Use specified synctime, otherwise use current time')
    parser.add_argument('--noise-diode-frequency', dest='noise_diode_frequency', type=float,
                        help='Set the noise diode frequency', default=-1)
    parser.add_argument('--noise-diode-pattern', dest='noise_diode_pattern', type=float, nargs=2,
                        help='Set the noise diode pattern, percentage [0...1], period [s]')

    parser.add_argument('--flip-spectrum', action="store_true", default=False, help="Flip the spectrum")
    args = parser.parse_args()

    if args.host in __known_packetizers:
        print("Found {} in known packetizers, use stored lookup ip and port.".format(args.host))
        args.port = __known_packetizers[args.host]['port']
        args.host = __known_packetizers[args.host]['ip']
    print("Configuring paketizer {}:{}".format(args.host, args.port))

    client_connection = aiokatcp.Client(args.host, args.port)
    client = DigitiserPacketiserClient(client_connection) 
    await client.query_packer_command_prefix()

    actions = [(client.check_packetizer_state, {})]
    if args.sampling_rate:
        actions.append((client.set_sampling_rate, dict(rate=args.sampling_rate)))
    if args.nbits:
        actions.append((client.set_bit_width, dict(nbits=args.nbits)))
    if args.v_destinations:
        actions.append((client.set_destinations, dict(v_dest=args.v_destinations, h_dest=args.h_destinations)))
    if args.interface_addresses:
        actions.append((client.set_interface_address, dict(intf=0, ip=args.interface_addresses[0])))
        actions.append((client.set_interface_address, dict(intf=1, ip=args.interface_addresses[1])))

    if args.mac_addresses:
        actions.append((client.set_mac_address, dict(intf=0, mac=args.mac_addresses[0])))
        actions.append((client.set_mac_address, dict(intf=1, mac=args.mac_addresses[1])))

    if args.predecimation_factor:
        actions.append((client.set_predecimation, dict(factor=args.predecimation_factor)))
    # Always flip spectrum to either on or off
    actions.append((client.flip_spectrum, dict(flip=args.flip_spectrum)))
    if args.noise_diode_frequency >= 0.:
        actions.append((client.set_noise_diode_frequency, dict(frequency=args.noise_diode_frequency)))
    if args.noise_diode_pattern:
        actions.append(
            (client.set_noise_diode_firing_pattern, dict(percentage=args.noise_diode_pattern[0],
                                                        period=args.noise_diode_pattern[1])))

    # Sync + capture start should come last
    if args.synchronize:
        actions.append((client.synchronize, dict(unix_time=args.sync_time)))
    if args.capture_start:
        actions.append((client.capture_start, {}))

    async def perform_actions():
        """Run all actions"""
        for action, params in actions:
            await action(**params)

    await perform_actions()


def main_cli():
    asyncio.run(main())


if __name__ == "__main__":
    main_cli()
