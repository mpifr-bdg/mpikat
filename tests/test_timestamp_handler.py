from __future__ import print_function, division, unicode_literals
import unittest
import time

from mpikat.utils.timestamp_handler import TimestampHandler

class Test_timestamp_handler(unittest.TestCase):

    def test_start_epoch_estimate(self):
        now = time.time()
        freq = 4E9

        # Epoch should be 0 if sync_time is just before now
        handler = TimestampHandler(now - 10, freq)
        self.assertEqual(handler._epoch, 0)
        self.assertEqual(handler._active_third, 1)

        # Test initial active third close to rollover
        handler = TimestampHandler(now + 10 - 2**48 / freq, 4E9)
        self.assertEqual(handler._epoch, 0)
        self.assertEqual(handler._active_third, 3)

        # Test epochs with different distances to now
        for i in range(1, 5):
            handler = TimestampHandler(now - 10 - i * 2**48 / freq, freq)
            self.assertEqual(handler._epoch, i)
            self.assertEqual(handler._active_third, 1)

        # Should also work for shorter timestamps
        for i in range(1, 5):
            handler = TimestampHandler(now - 1E-3 - i * 2.**32 / freq, freq, 32)
            self.assertEqual(handler._epoch, i)
            self.assertEqual(handler._active_third, 1)


    def test_active_thrid_update(self):
        freq = 3.2E9
        now = time.time()
        handler = TimestampHandler(now - 10, freq)
        self.assertEqual(handler._active_third, 1)

        # a timestamp close to rollover should not change the active third
        handler._updateEpoch(2**48 - 1)
        self.assertEqual(handler._active_third, 1)

        # a timestamp in the first thrid should not trigger an update
        handler._updateEpoch(int(2**48 *  1./6 ))
        self.assertEqual(handler._active_third, 1)

        # consequetive tiemstamps should update
        handler._updateEpoch(int(2**48 *  3./6 ))
        self.assertEqual(handler._active_third, 2)

        handler._updateEpoch(int(2**48 *  1./6 ))
        self.assertEqual(handler._active_third, 2)

        handler._updateEpoch(int(2**48 *  5./6 ))
        self.assertEqual(handler._active_third, 3)
        handler._updateEpoch(int(2**48 *  3./6 ))
        self.assertEqual(handler._active_third, 3)

        handler._updateEpoch(int(2**48 *  1./6 ))
        self.assertEqual(handler._active_third, 1)


    def test_effective_epoch(self):
        freq = 3.2E9
        now = time.time()
        handler = TimestampHandler(now + 10 - 2**48 / freq , freq)

        # No rollover happend yet, still in epoch 0
        E = handler._updateEpoch(2**48 - 2)
        self.assertEqual(E, 0)

        E = handler._updateEpoch(2**48 - 1)
        self.assertEqual(E, 0)

        # Rollover happens, new epoch
        E = handler._updateEpoch(2)
        self.assertEqual(E, 1)

        # Stray from previous epoch
        E = handler._updateEpoch(2**48 - 5)
        self.assertEqual(E, 0)

        E = handler._updateEpoch(1)
        self.assertEqual(E, 1)


    def test_timestampUpdate(self):
        freq = 6.2E9
        now = time.time()
        handler = TimestampHandler(now + 10 - 2**48 / freq , freq)

        # No rollover happend yet, still in epoch 0
        T = handler.updateTimestamp(2**48 - 2)
        self.assertEqual(T, 2**48 - 2)

        # Rollover happens, new epoch
        T = handler.updateTimestamp(2)
        self.assertEqual(T, 2**48 + 2)


if __name__ == '__main__':
    unittest.main()
