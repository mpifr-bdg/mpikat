import socket
from mpikat.core import logger

_log = logger.getLogger('mpikat.utils')

def get_port() -> int:
    """
    Return a free port from the kernel port range
    """
    with socket.socket() as sock:
        sock.bind(('localhost', 0))
        port = sock.getsockname()[1]
    _log.debug("Got port %d", port)
    return port

def get_host() -> str:
    host = socket.gethostname()
    _log.debug("Hostname is %s", host)
    return host