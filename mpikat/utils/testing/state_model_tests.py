"""
Author: Tobias Winchen / Niclas Esser
Mail: twinchen@mpifr-bonn.mpg.de / nesser@mpifr-bonn-mpg.de

State engine tests for EDDPipeline implementations. The tests are only
viable for piplines which inherits from mpikat.core.edd_pipleine_aio.EDDPipeline.
The deprecated mpikat.core.edd_pipleine.EDDPipeline is not supported.

Note: The actual function of the pipelines under tests are not executed and patched
    with a dummy function. The goal of these tests is just to validate the state engine!
"""

import asyncio
import unittest

from aiokatcp.client import Client
from aiokatcp.connection import FailReply

from mpikat.core.edd_pipeline_aio import EDDPipeline, state_change
from mpikat.core import logger
from mpikat.utils.testing import launch_server_in_thread
from mpikat.utils import get_port

_LOG = logger.getLogger("mpikat.utils.testing.state_model_tests")

host = "127.0.0.1"

def state_change_patch(**kwargs: dict) -> callable:
    """Decorator to patch the inital state_change decorator.
    All functions of individual pipelines are replaced by a callable dummy. This allows the
    validation of the state engine without having pipeline specific dependencies installed
    (e.g. dspsr for the pulsar pipeline).

    Args:
        kwargs (dict): arguments of the initial state_change decorator the pipeline

    Returns:
        callable: a dummy callable using the state_change arguments of the inital pipeline function
    """
    @state_change(**kwargs)
    async def dummy(*args: EDDPipeline, **kwargs: dict) -> None:
        """Callable dummy function replacing EDDPipeline function

        Args:
            args (EDDPipeline): The EDDPipeline object under test
            kwargs (dict): The arguments to pass to the function
        """
        pass

    def wrapper(func: callable) -> callable:
        """Wrapper of the dummy

        Args:
            func (callable): The function under test

        Returns:
            callable: The dummy callable
        """
        return dummy
    return wrapper

class Test_StateModelOnRequests(unittest.IsolatedAsyncioTestCase):
    """
    Asynchronous test class to validate states of the pipeline under test by sending KATCP-
        commands. In contrast to the Test_StateModel-class here the request-functions are
        tested, too.
    """
    async def asyncSetUp(self) -> None:
        """Default asyncSetUp using the EDDPipeline base class. Must be reimplemented by
            sub-classes.
        """
        await super().asyncSetUp()
        await self.setUpBase(EDDPipeline)

    async def setUpBase(self, pipeline: EDDPipeline, streaming: bool=False) -> None:
        """Interface for sub-classes. Usually called in the asyncSetUp-function of the child.

        Args:
            pipeline (EDDPipeline): The pipeline under test. Should be a sub-class of EDDPipeline
            streaming (bool, optional): If true, the pipeline enters a streaming state and ignores
                measurement-prepare / -start / -stop
        """
        _LOG.info("Setting up statemodel test for %s on requests", pipeline.__class__.__name__)
        self.streaming = streaming
        self.port = get_port()
        unittest.IsolatedAsyncioTestCase.setUp(self)
        # launch pipeline server
        self.server_thread = launch_server_in_thread(pipeline, host, self.port)
        self.server = self.server_thread.server
        await unittest.IsolatedAsyncioTestCase.asyncSetUp(self)
        # launch KATCP client
        self.client = await asyncio.wait_for(Client.connect(host, self.port), timeout=5.0)

    def tearDown(self):
        """Clean up after each test. Closes the client and stops the server thread.
        """
        unittest.IsolatedAsyncioTestCase.tearDown(self)
        self.client.close()
        self.server_thread.stop().result()
        self.server_thread.join(timeout=5)

    async def trigger_transition(self, cmds: dict, current_state: str) -> None:
        """Common helper function used for trigger state transisitions

        Args:
            cmds (dict): The commands to execute. Should have the form {"command":"target_state"}
            current_state (str): The state of the pipeline on start up
        """
        # Generalized command palette
        commands = ["configure",
                    "capture-start",
                    "measurement-prepare",
                    "measurement-start",
                    "measurement-stop",
                    "deconfigure"
                   ]
        # Iterate over all commands
        for c in commands:
            self.server.state = current_state
            self.assertEqual(self.server.state, current_state)
            if c not in cmds:
                with self.assertRaises(FailReply):
                    reply, __ = await self.client.request(c)
                self.assertEqual(
                    self.server.state, current_state, msg="Expected: " + current_state+ " by " + c
                )
            else:
                reply, __ = await self.client.request(c)
                self.assertEqual(reply, [], msg="Expected ok reply from " + c)
                self.assertEqual(
                    self.server.state, cmds[c], msg="Expected: " + cmds[c] + " by " + c
                )

    async def test_idle_on_request(self) -> None:
        """Should test sequence configure -> deconfigure
        """
        await self.trigger_transition({
            "configure": "configured",
            "deconfigure": "idle"
        }, 'idle')

    async def test_configured_on_request(self) -> None:
        """Should test sequence capture_start -> deconfigure
        """
        if self.streaming:
            await self.trigger_transition({
                "capture-start": 'streaming',
                'deconfigure': 'idle'
            }, 'configured')
        else:
            await self.trigger_transition({
                "capture-start": 'ready',
                'deconfigure': 'idle'
            }, 'configured')

    async def test_ready_on_request(self) -> None:
        """Should test sequence measurement-prepare -> deconfigure -> measurement-stop
        """
        if not self.streaming:
            await self.trigger_transition({
                "measurement-prepare": 'set',
                "deconfigure": "idle",
                "measurement-stop": 'ready'
            }, 'ready')

    async def test_set_on_request(self):
        """Should test sequence measurement-start -> measurement-stop -> deconfigure
        """
        if not self.streaming:
            await self.trigger_transition({
                "measurement-start": 'measuring',
                "measurement-stop": 'ready',
                "deconfigure": "idle"
            },'set')

    async def test_measuring_on_request(self):
        """Should test sequence measurement-stop -> deconfigure
        """
        if not self.streaming:
            await self.trigger_transition({
                "measurement-stop": 'ready',
                "deconfigure": "idle"
            }, 'measuring')

class Test_StateModel(unittest.IsolatedAsyncioTestCase):
    """
    Also base class for state model tests of other pipelines.
    """
    def setUp(self) -> None:
        super().setUp()
        self.setUpBase(EDDPipeline(host, get_port()))

    def setUpBase(self, pipeline: EDDPipeline, streaming: bool=False):
        """Interface for sub-classes. Usually called in the asyncSetUp-function of the child.

        Args:
            pipeline (EDDPipeline): The pipeline under test. Should be a sub-class of EDDPipeline
            streaming (bool, optional): If true, the pipeline enters a streaming state and ignores
                measurement-prepare / -start / -stop
        """
        _LOG.info("Setting up state model test for %s", pipeline.__class__.__name__)
        unittest.IsolatedAsyncioTestCase.setUp(self)
        self.pipeline = pipeline
        self.streaming = streaming

    async def trigger_transition(self, cmds, current_state):
        """Common helper function used for trigger state transisitions

        Args:
            cmds (dict): The commands to execute. Should have the form {"command":"target_state"}
            current_state (str): The state of the pipeline on start up
        """
        commands = [self.pipeline.configure,
                    self.pipeline.capture_start,
                    self.pipeline.measurement_prepare,
                    self.pipeline.measurement_start,
                    self.pipeline.measurement_stop,
                    self.pipeline.deconfigure
                   ]
        for c in commands:
            self.pipeline.state = current_state
            if c not in cmds:
                self.assertEqual(self.pipeline.state, current_state)
                msg = f"State change {c.__name__} not failing as expected. current state: {self.pipeline.state}"
                with self.assertRaises(FailReply, msg=msg):
                    await c()
                msg = f"Expected: {current_state} by {c.__name__}"
                self.assertEqual(self.pipeline.state, current_state, msg=msg)
            else:
                msg = f"Expected: {cmds[c]} by {c.__name__}"
                await c()
                self.assertEqual(self.pipeline.state, cmds[c], msg=msg)


    async def test_idle(self):

        await self.trigger_transition({
            self.pipeline.configure: "configured",
            self.pipeline.deconfigure: "idle"
        }, 'idle')


    async def test_configured(self):

        if self.streaming:
            await self.trigger_transition({
                self.pipeline.capture_start: 'streaming',
                self.pipeline.deconfigure: "idle"
            }, 'configured')
        else:
            await self.trigger_transition({
                self.pipeline.capture_start: 'ready',
                self.pipeline.deconfigure: "idle"
            }, 'configured')

    async def test_ready(self):

        if not self.streaming:
            await self.trigger_transition({
                self.pipeline.measurement_prepare: 'set',
                self.pipeline.deconfigure: "idle",
                self.pipeline.measurement_stop: 'ready'
            }, 'ready')

    async def test_set(self):

        if not self.streaming:
            await self.trigger_transition({
                self.pipeline.measurement_start: 'measuring',
                self.pipeline.measurement_stop: 'ready',
                self.pipeline.deconfigure: "idle"
            }, 'set')

    async def test_measuring(self):

        if not self.streaming:
            await self.trigger_transition({
                self.pipeline.measurement_stop: 'ready',
                self.pipeline.deconfigure: "idle"
            }, 'measuring')

    async def test_streaming(self):
        """Should test if streaming pipelines stay in streaming state after measurement-commands
        """
        if self.streaming:
            await self.trigger_transition({
                self.pipeline.deconfigure: "idle",
                self.pipeline.measurement_prepare: "streaming",
                self.pipeline.measurement_start: "streaming",
                self.pipeline.measurement_stop: "streaming",
            }, 'streaming')