import select
from threading import Thread, Event

import mpikat.core.logger

_log = mpikat.core.logger.getLogger('mpikat.pipe_monitor')


class PipeMonitor(Thread):
    """
    Class for parsing output of subprocess pipes
    """

    def __init__(self, pipe, handler, sentinel=b'', timeout=1):
        """
        Args:
            pipe:    An OS pipe
            handler: handler to be called on the line.
        """
        Thread.__init__(self, daemon=True)
        self._handler = handler
        self._pipe = pipe
        self._sentinel = sentinel
        self._timeout = timeout
        self._poll = select.poll()
        self._poll.register(self._pipe)
        self._stop_event = Event()

    def run(self):
        """
        Starts the monitor thread. Will stop on pipe error or pipe close.
        """
        while not self._stop_event.is_set():
            pev = self._poll.poll(self._timeout)
            if not pev:
                continue
            else:
                pev = pev[0]
            if pev[1] in [select.POLLIN, select.POLLPRI]:
                for line in iter(self._pipe.readline, self._sentinel):
                    line = line.decode('utf-8')
                    try:
                        self._handler(line)
                    except Exception as error: # pylint: disable=broad-except
                        _log.warning("Exception raised in pipe handler: '{}' with line '{}'".format(
                            str(error), line))
            elif pev[1] in [select.POLLHUP]:
                self.stop()
            elif pev[1] == select.POLLOUT:
                pass
            elif pev[1] == select.POLLNVAL:
                _log.error("Invalid request descriptor not open")
                self.stop()
            elif pev[1] == select.POLLERR:
                _log.error("Error reading data from pipe!")
                self.stop()
            else:
                _log.error("Unknown error code")
                self.stop()

    def stop(self):
        """
        Stop the thread
        """
        self._stop_event.set()


if __name__ == "__main__":
    import subprocess

    def stdout_handler(line):
        """Print line"""
        print(line)

    proc = subprocess.Popen("echo Foo && sleep 2 && echo bar", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, close_fds=True)
    pm = PipeMonitor(proc.stdout, stdout_handler)
    pm.start()
    pm.join()
