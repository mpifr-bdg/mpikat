import os
import datetime
import string
import random

import mpikat.core.logger

_log = mpikat.core.logger.getLogger('mpikat.utils.ip_manager')



def get_random_string(N):
    """
    Returns tring of N random characters
    """
    return "".join([string.ascii_letters[random.randint(0, len(string.ascii_letters) - 1)] for _ in range(N)])



def create_datebased_subdirs(root, date=datetime.datetime.now(datetime.UTC)):
    """
    Create a folder hirarchy based on a given timestsamp
    """

    path = os.path.join(root, str(date.year), f'{date.month:02}', f'{date.day:02}')
    os.makedirs(path, exist_ok=True)
    _log.debug('Creating date based subdirectory in %s', path)

    return path


def create_file_name(path, suffix, file_id_no=None):
    """
    Creates a unique non-existing filename.

    Args:
        path:           directory in which to create the file
        file_id_no:     Identifier to be included in the file. If None
                        (default) a random string will be used. pass an empty sting to
                        disable.
    """
    _log.debug("Creating unique file name in %s with suffix %s and id %s ", path, suffix, file_id_no)

    while True:
        now = datetime.datetime.now(datetime.UTC).strftime("%Y-%m-%dT%H:%M:%S.%f")
        if file_id_no is None:
            uid = get_random_string(5)
        else:
            uid = str(file_id_no)
        filename = f"EDD_{now}UTC_{uid}.{suffix}"
        ofile = os.path.join(path, filename)
        if not os.path.exists(ofile):
            break
    return ofile



