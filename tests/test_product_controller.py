# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring

import unittest
import unittest.mock
import asyncio

import aiokatcp

from mpikat.utils import testing, get_port
from mpikat.core.katcp_client import KATCPClient
from mpikat.core.product_controller import ProductController
from mpikat.core.edd_pipeline_aio import EDDPipeline


class TestPipeline(EDDPipeline):
    """
    A test pipeline
    """


class TestProductController(unittest.IsolatedAsyncioTestCase):
    """
    Unittests for the class ProductController
    """
    async def asyncSetUp(self) -> None:

        self.port = get_port()

        self.thread = testing.server_thread.ServerThread(
            testing.FailPipeline, '0.0.0.0', self.port)

        self.controller = ProductController(
            "FailPipeline", "127.0.0.1", self.port)


    async def asyncTearDown(self) -> None:
        self.thread.stop().result()
        self.thread.join(timeout=5)


    async def test_fails(self) -> None:
        """
        All executed requests should raise an FailReply
        """
        self.thread.start()
        self.thread.server_created.wait()

        with self.assertRaises(aiokatcp.FailReply):
            await self.controller.set("")
        with self.assertRaises(aiokatcp.FailReply):
            await self.controller.configure()
        with self.assertRaises(aiokatcp.FailReply):
            await self.controller.capture_start()
        with self.assertRaises(aiokatcp.FailReply):
            await self.controller.capture_stop()

    async def test_fail_overrides(self) -> None:
        """
        All executed requests should raise an FailReply but ignore them
        """
        self.controller.ignore_errors = True
        self.thread.start()
        self.thread.server_created.wait()

        await self.controller.set("")
        await self.controller.configure()
        await self.controller.capture_start()
        await self.controller.capture_stop()


    async def test_fail_overrideserver_not_responding(self) -> None:
        """
        All executed requests should raise an FailReply but ignore them
        """
        self.controller.ignore_errors = True
        self.thread.start()
        self.thread.server_created.wait()

        async def dummy_ping(self):
            return False

        self.controller.ping = dummy_ping

        await self.controller.set("")
        await self.controller.configure()
        await self.controller.capture_start()
        await self.controller.capture_stop()


    async def test_ping(self) -> None:
        """
        Test ping: ping should only work after pipeline start
        """
        p = await self.controller.ping(timeout=0.1)
        self.assertFalse(p)

        self.thread.start()
        self.thread.server_created.wait()
        counter = 0
        while counter < 50:
            counter += 1
            p = await self.controller.ping()
            if p:
                break
            await asyncio.sleep(0.1)

        self.assertTrue(p)


    async def test_reconnect_on_ping(self) -> None:
        """
        Ping should reconnect to server when first attempt times out
        """
        self.thread.start()
        self.thread.server_created.wait()

        mock_object = KATCPClient(0, 0)
        async def dummy(*args, **kwargs): raise TimeoutError
        mock_object.wait_connected = dummy
        self.controller._client = mock_object

        p = await self.controller.ping()

        self.assertTrue(p)

        self.assertNotEqual(self.controller._client, mock_object)


    async def test_get_config(self) -> None:
        """
        Test getConfig: config should contain expected values
        """
        self.thread.start()
        self.thread.server_created.wait()
        cfg = await self.controller.getConfig()
        self.assertEqual(cfg['port'], self.controller.port)


    async def test_get_state(self) -> None:
        """
        Test getState: state should have the prevoiously set state
        """
        self.thread.start()
        self.thread.server_created.wait()

        state = await self.controller.getState()
        self.assertEqual(state, "idle")

        self.thread.server.state = 'panic'

        state = await self.controller.getState()
        self.assertEqual(state, "panic")


    async def test_measurement_stop(self):

        self.thread = testing.server_thread.ServerThread(
            TestPipeline, '0.0.0.0', self.port)

        self.thread.start()
        self.thread.server_created.wait()
        self.thread.server.state = "measuring"
        self.thread.server.add_output_file('foo')

        res = await self.controller.measurement_stop(True, timeout=2)
        self.assertTrue('foo' in res)

        self.thread.server.state = "measuring"

        res = await self.controller.measurement_stop(False, timeout=2)
        self.assertIsNone(res)

if __name__ == '__main__':
    unittest.main()
