import signal
from argparse import ArgumentParser
import json
import functools
import socket
import asyncio
import copy
import os
from typing import Dict

from aiokatcp.connection import FailReply
from aiokatcp.server import DeviceServer, RequestContext
from aiokatcp.sensor import Sensor

from mpikat.core.version import VERSION
from mpikat.core import datastore, cfgjson2dict
from mpikat.core.data_stream import DataStreamRegistry
from mpikat.core import updateConfig, value_list
from mpikat.core import logger as logging
from mpikat.utils import get_port

_log = logging.getLogger("mpikat.core.edd_pipeline_aio")


class StateChange(Exception):
    """
    Special exception that causes a state change to a state other than the
    defined error states.
    """

class StateChangeFail(Exception):
    """
    Special exception that causes a state change to a state other than the
    defined error states. However, the second argument is returned as a fail reply to the katcp server
    """




PIPELINE_STATES = ["idle", "configuring", "configured",
                   "capture_starting", "streaming", "ready",
                   "measurement_preparing", "set",
                   "measurement_starting", "measuring", "running",
                   "measurement_stopping",
                   "capture_stopping", "deconfiguring", "error", "panic",
                   "unprovisioned", "provisioning", "deprovisioning"]




def pipeline_task(timeout=None):
    """
    Decorator to add a method of a EDDPipeline to the pipeline task list, allowing cancellation via request
    """
    def decorator_pipeline_task(func):
        @functools.wraps(func)
        async def wrapper(self, *args, **kwargs):
            """
            Wraps the method so that it is added as a task to the pipeline's task list on start, removed on finish and that the result is None if
            """
            task = self.add_pipeline_task(func(self, *args, **kwargs), timeout=timeout)
            result = await task
            _log.debug('Task %s finsihed', task)
            return result
        return wrapper
    return decorator_pipeline_task






def state_change(target, allowed=PIPELINE_STATES, waitfor=None,
                 abortwaitfor=['deconfiguring', 'error', 'panic'],
                 intermediate=None, ignored=None, error='error', timeout=120):
    """
    Decorator to perform a state change in a method.

    Args:
       target (str):          Target state.
       allowed (list):        Allowed source states.
       intermediate (str):    Intermediate state assumed while executing.
       error (str):           State assumed if an exception reached the decorator.
       waitfor (str):         Wait with the state changes until the current state set.
       abortwaitfor (list):   States that result in aborting the wait (with Failure).
       ignored (list):        States that are ignored - will not raise a failure.
       timeout (int):         If state change is not completed after [timeout] seconds, error state is assumed. Timeout can be None to wait indefinitely.

    Example:
       State transition from idle->configure (or error) via configuring.::

         @state_change(target="configured", allowed=["idle"], intermediate="configuring")
         async def configure(self, config_json):
             pass

    Note:
        If more than one valid target or error state is possible, the final
        state has to be indicated by throwing a StateChange exception.
    """
    def decorator_state_change(func: callable):
        @functools.wraps(func)
        async def wrapper(self, *args, **kwargs):
            _log.debug("Decorator managed state change %s -> %s via intermediate state: %s", self.state, target, intermediate)
            if ignored and self.state in ignored:
                _log.debug('State change ignored because state %s is in ignored list: %s!', self.state, ignored)
                return
            if self.state not in allowed:
                msg = "State change to {} requested, but state {} not in allowed states! Doing nothing.".format(target, self.state)
                raise FailReply(msg)
            if waitfor:
                waiting_since = 0
                while self.state != waitfor:
                    _log.warning("Waiting since {}s for state {} to start state change to {}, current state {}. Timeout: {}s".format(waiting_since, waitfor, target, self.state, timeout))
                    if waiting_since > timeout:
                        raise RuntimeError("Waiting since {}s to assume state {} in preparation to change to {}. Aborting.".format(waiting_since, waitfor, target))
                    await asyncio.sleep(1)
                    waiting_since += 1

                    if self.state in abortwaitfor:
                        raise FailReply("Aborting waiting for state: {} due to state: {}".format(waitfor, self.state))

            if intermediate:
                self.state = intermediate
            try:
                result = None
                task = self.add_pipeline_task(func(self, *args, **kwargs), timeout=timeout)
                result = await task
            except StateChange as E:
                self.state = str(E)
            except StateChangeFail as E:
                self.state = str(E.args[0])
                raise FailReply(E.args[1])
            except Exception as E:
                _log.error('Pipeline task %s failed, with %s: %s', func.__name__ , E.__class__.__name__, E)
                _log.exception(E)
                self.state = error
                raise E
            else:
                self.state = target
            return result
        return wrapper
    return decorator_state_change


class EDDPipeline(DeviceServer):
    """
    Abstract interface for EDD Pipelines

    Pipelines can implement functions to act within the following
    sequence of commands with associated state changes. After provisioning the
    pipeline is in state idle.

        ?set "partial config"
            Updates the current configuration with the provided partial config.
            After set, it remains in state idle as only the config dictionary
            may have changed. A wrong config is rejected without changing state
            as state remains valid. Multiple set commands can be send to the
            pipeline.
        ?configure "partial config"
            state change from idle to configuring and configured (on success)
            or error (on fail)
        ?capture_start
            state change from configured to streaming or ready (on success) or
            error (on fail).  Streaming indicates that no further changes to
            the state are expected and data is injected into the EDD.
        ?measurement_prepare "data"
            state change from ready to set or error
        ?measurement_start
            state change from set to running or error
        ?measurement_stop
            state change from running to set or error
        ?capture_stop
            return to state configured or idle
        ?deconfigure
            restore state idle

    * configure - optionally does a final update of the current config and
                  prepares the pipeline. Configuring the pipeline may take time,
                  so all lengthy preparations should be done here.
    * capture start - The pipeline should send data (into the EDD) after this command.
    * measurement prepare - receive optional configuration before each measurement.
                            The pipeline must not stop streaming on update.
    * measurement start - Start of an individual measurement. Should be quasi
                          instantaneous. E.g. a recorder should be already connected
                          to the data stream and just start writing to disk.
    * measurement stop -  Stop the measurement

    Pipelines can also implement:
        * populate_data_store to send data to the store. The address and port for a data store is received along the request.

    All pipelines have some default options, that are injected into the pipeline configurations, if not specified further:
        input_data_streams: (Empty) list of input data streams
        output_data_streams: (Empty) list of output data streams
        ignore_pipeline_errors: If true, the master controller will ignore errors in this pipeline
        ip: IP address via which the pipeline can be controlled
        port: Corresponding port
    """

    PIPELINE_STATES = PIPELINE_STATES

    VERSION = "mpikat-edd-pipeline-aio-0.2"
    BUILD_STATE = "mpikat-edd-pipeline-aio-0.2"

    def __init__(self, ip, port, default_config=None, loop=None):
        """
        Initialize the pipeline. Sub-classes are required to provide their
        default config dict and specify the data formats defined by the class,
        if any.

        Args:
            ip:             IP to accept connections from
            port:           port to listen
            default_config: default config of the pipeline
        """
        self._state = "idle"
        self.previous_state = "unprovisioned"
        self.__notes = set()
        self._output_files = dict()

        self._pipeline_tasks = set()

        if default_config is None:
            default_config = {}

        # Update build info with actual version string and class name
        self.BUILD_STATE = f'{self.__class__.__name__}-{VERSION}'
        if port == 0:
            port = get_port()
        super().__init__(ip, port, loop=loop)
        _log.info("Accepting connections from: {}:{}".format(ip, port))

        # inject data store data into all default configs.
        default_config.setdefault("data_store", dict(ip="localhost", port=6379))
        default_config.setdefault("id", "Unspecified")
        default_config.setdefault("type", self.__class__.__name__)
        default_config.setdefault("input_data_streams", [])
        default_config.setdefault("output_data_streams", [])
        default_config.setdefault("ignore_pipeline_errors", False)
        default_config.setdefault("data_root_directory", "/mnt")      # root directory inside the container
        default_config.setdefault("measurement_prepare_default", {})

        default_config["ip"] = socket.gethostname()
        default_config["port"] = port


        def update_stream(stream):
            """update stream with defaults from stream registry"""
            if not stream.get('format'):
                _log.warning("Data stream without format definition!")
                return stream
            frm = DataStreamRegistry.get(stream['format'])
            for key, value in frm.items():
                stream.setdefault(key, value)
            return stream


        for stream in value_list(default_config['input_data_streams']):
            stream.setdefault("source", "")
            stream = update_stream(stream)
        for stream in value_list(default_config['output_data_streams']):
            stream = update_stream(stream)


        self.__config = default_config.copy()
        self._default_config = default_config
        self._subprocesses = []
        self._subprocessMonitor = None

        # update the doc-strings for the requests by their subclass implementation
        for r, s in [(self.request_configure, self.configure),
                     (self.request_set, self.set),
                     (self.request_capture_start, self.capture_start),
                     (self.request_capture_stop, self.capture_stop),
                     (self.request_measurement_start, self.measurement_start),
                     (self.request_measurement_stop, self.measurement_stop)]:
            r.__func__.__doc__ = s.__doc__
        self.setup_sensors()
        _log.debug("Server __init__ done ")

    # --------------------------- #
    # Properties / getter methods #
    # --------------------------- #
    @property
    def _config(self):
        """
        The current configuration of the pipeline, i.e. the default
        after all updates received via set and configure commands. This value
        should then be used in the _configure method.
        """
        return self.__config


    @property
    def state(self):
        """
        State of the pipeline.
        """
        return self._state

    # -------------- #
    # Setter methods #
    # -------------- #
    @_config.setter
    def _config(self, value: dict):
        if not isinstance(value, dict):
            raise RuntimeError("_config has to be a dict!")

        if value == self.__config:
            _log.debug("No changes in config, not updating sensor")
        else:
            self.__config = value
            self._configUpdated()

    @state.setter
    def state(self, value):
        _log.info("Changing state: {} -> {}".format(self._state, value))
        self.previous_state = self._state
        self._state = value
        self._pipeline_sensor_status.set_value(self._state)


    # ------------- #
    # Basic methods #
    # ------------- #
    def setup_sensors(self):
        """
        Setup monitoring sensors.

        The EDDPipeline base provides default sensors. Should be called by
        every subclass to ensure default sensors are available.

        """

        self._pipeline_sensor_status = Sensor(
            str, "pipeline-status",
            description="Status of the pipeline",
            initial_status=Sensor.Status.UNKNOWN,
            default="idle")
        self.sensors.add(self._pipeline_sensor_status)

        self._edd_config_sensor = Sensor(
            str, "current-config",
            description="The current configuration for the EDD backend",
            default=json.dumps(self._config, indent=4),
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._edd_config_sensor)

        self._edd_log_level = Sensor(
            str, name="edd-log-level",
            description="Log level",
            default=logging.getLevelName(),
            initial_status=Sensor.Status.NOMINAL)
        self.sensors.add(self._edd_log_level)

        version_sensor = Sensor(
            str, "pipeline-version",
            description="Version of mpikat",
            default=VERSION,
            initial_status=Sensor.Status.NOMINAL)
        self.sensors.add(version_sensor)

        self._notes_sensor = Sensor(
            str, "notes",
            description="Text notifications to users",
            default="",
            initial_status=Sensor.Status.NOMINAL)
        self.sensors.add(self._notes_sensor)

    def _cancel_pipeline_tasks(self):
        """
        Cancels all running / pending pipeline tasks
        """
        _log.debug('canceling pipeline tasks: %i', len(self._pipeline_tasks))
        for f in self._pipeline_tasks:
            _log.debug('Cancelling %s', f)
            try:
                f.cancel()
            except Exception as E:
                # Log + reraise here as cancellation exceptions might get lost
                # otherwise
                _log.error('Error cancellation of task')
                _log.exception(E)
        _log.debug('All tasks cancelled')

    def add_pipeline_task(self, cor, timeout=None):
        _log.debug('Starting task for %s', cor)
        if timeout:
            task = asyncio.ensure_future(asyncio.wait_for(cor, timeout=timeout))
        else:
            task = asyncio.ensure_future(cor)
        _log.debug('Adding task to tasklist %s', cor)
        self._pipeline_tasks.add(task)
        task.add_done_callback(self._pipeline_tasks.discard)
        _log.debug('Returning task %s', cor)
        return task


    def get_output_files(self):
        return self._output_files

    def add_output_file(self, filename, meta=None):
        """
        Adds an outptufile to the list of outputfiles generated by the pipeline measurement
        """

        if filename.startswith(self._config['data_root_directory']):
            filename = os.path.relpath(filename, self._config['data_root_directory'])
        if meta is None:
            meta = {}
        meta.setdefault('pipeline_id', self._config['id'])
        meta.setdefault('pipeline_type', self._config['type'])

        _log.info('Registering outptufile %s', filename)
        self._output_files[filename] = meta


    def add_note(self, note):
        """
        Adds a note to the set of notes. Update sensor if needed.
        """
        if note in self.__notes:
            return
        self.__notes.add(note)
        self.__updateNoteSensor()

    def flush_notes(self):
        """Remove notes from sensor."""
        self.__notes = set()
        self.__updateNoteSensor()

    def clear_note(self, note):
        """
        Remove a note from the set of notes. Update sensor if needed.
        """

        if note not in self.__notes:
            return
        self.__notes.remove(note)
        self.__updateNoteSensor()

    def watchdog_error(self):
        """
        Set error mode requested by watchdog.
        """
        _log.error("Error state requested by watchdog!")
        self.state = "error"

    def _subprocess_error(self, proc):
        """
        Sets the error state because process has ended.
        """
        _log.error("Error handle called because subprocess {} ended with return code {}".format(proc.pid, proc.returncode))
        if self._subprocessMonitor is not None:
            self._subprocessMonitor.stop()
        self.state = "error"

    def updateConfig(self, cfg):
        updateConfig(self._config, cfg)
        self._configUpdated()


    def _configUpdated(self):
        """
        Signals that the config dict has been updated. Separate method as
        direct updates of _config items without writing a full dict to _config
        will not trigger the _config.setter and have to call this method
        manually.
        """
        self._edd_config_sensor.set_value(json.dumps(self.__config, indent=4))

    def _get_whoami(self):
        """
        Get the whoami string

        Return:
            string: string contains information on the pipeline
        """
        reply_msg = [""]
        reply_msg.append(f'Id:         {self._config["id"]}')
        reply_msg.append(f'Classname:  {self.__class__.__name__}')
        reply_msg.append(f'Type:       {self._config["type"]}')
        reply_msg.append(f'Version:    {VERSION}')
        return '\n'.join(reply_msg)

    def __updateNoteSensor(self):
        if len(self.__notes) == 1:
            msg = "".join(self.__notes)
        else:
            msg = "\n".join(" * {}".format(n) for n in self.__notes)

        self._notes_sensor.set_value(msg)

    # ------------------------ #
    # Common request functions #
    # ------------------------ #
    async def request_set_log_level(self, ctx: RequestContext, level: str = "INFO") -> str:
        """
        Sets the log level

        Return:
            katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        _log.info("Setting log level to: {}".format(level.upper()))
        logging.setLevel(level.upper())
        self._edd_log_level.set_value(level.upper())
        _log.debug("Successfully set log-level")

    async def request_log_level(self, ctx: RequestContext) -> str:  # We want to command our own logger only. pylint: disable=arguments-differ
        """
        Returns the log level

        Return:
            the edd_log_level sensor
        """
        return self._edd_log_level.value


    async def request_set_katcp_internal_log_level(self, ctx: RequestContext, msg: str) -> str:
        """
        Set the katcp internal log level

        Return:
            katcp reply object
        """
        return await super().request_log_level(ctx, msg)


    async def request_whoami(self, ctx: RequestContext) -> str:
        """
        Returns the name of the controlled pipeline

        Return:
            katcp reply object
        """
        return self._get_whoami()

    async def request_override_state(self, ctx: RequestContext, value: str) -> str:
        """
        Sets the state of the pipeline manually to a given value.

        Return:
            katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        if value not in self.PIPELINE_STATES:
            _log.warning("Trying to overriding pipeline state but state '{}' does not exist".format(value))
            raise FailReply("State '{}' does not exist.".format(value))
        _log.warning("Overriding pipeline state: {}".format(value))
        self.state = value


    async def request_set_default_config(self, ctx: RequestContext) -> str:
        """
        (Re-)set the config to the default.

        Returns:
            katcp reply object [[[ !reconfigure ok | (fail [error description]) ]]]
        """

        _log.info("Setting default configuration")
        self._config = self._default_config.copy()

    async def request_set(self, ctx: RequestContext, config_json: str) -> str:
        """
        Add the config_json to the current config

        Returns:
            katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        await self.set(config_json)

    async def request_register(self, ctx: RequestContext, msg: str) -> str:
        """
        Register the pipeline in the data-store. Optionally the data store can
        be specified as "ip:port". If not specified the value in the
        configuration will be used.

        katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        _log.debug("register request")
        if msg:
            host, port = msg.split(':')
            port = int(port)
        else:
            host = self._config['data_store']['ip']
            port = self._config['data_store']['port']
        await self.register(host, port)


    async def request_halt(self, ctx: RequestContext) -> str:
        """
        Halts the process. Re-implementation of base class halt without timeout as this crash
        """
        if self.state == "running":
            await self.capture_stop()
        await self.deconfigure()
        return await super().request_halt(ctx)

    # -------------------------------------- #
    # Requests coroutines for state changes  #
    # -------------------------------------- #
    async def request_configure(self, ctx: RequestContext, config_json: str = "") -> str:
        """
        Configure EDD to receive and process data

        Returns:
            katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        _log.debug("Received additonal configuration string: '{}'".format(config_json))
        await self.set(config_json)
        await self.configure()

    async def request_capture_start(self, ctx: RequestContext) -> str:
        """
        Start the EDD backend processing

        This is the KATCP wrapper for the capture_start command

        Returns:
            katcp reply object [[[ !capture_start ok | (fail [error description]) ]]]
        """
        await self.capture_start()

    async def request_measurement_prepare(self, ctx: RequestContext, config_json: str = ""):
        """
        Prepare measurement request

        Return:
            katcp reply object [[[ !measurement_prepare ok | (fail [error description]) ]]]
        """
        try:
            _log.debug("Received additonal preparation string: '{}'".format(config_json))
            config = cfgjson2dict(config_json)
        except json.JSONDecodeError as E:
            _log.error("Cannot parse json:\n%s\n%s", config_json, E)
            raise json.JSONDecodeError(f"Invalid json received: {config_json}") from E

        cfg = self._config['measurement_prepare_default'].copy()
        cfg.update(config)
        await self.measurement_prepare(cfg)

    async def request_measurement_start(self, ctx: RequestContext) -> str:
        """
        Start measurement.

        This is the KATCP wrapper for the measurement_start command

        Return:
            katcp reply object [[[ !measurement_start ok | (fail [error description]) ]]]
        """
        await self.measurement_start()

    async def request_measurement_stop(self, ctx: RequestContext, reply_outputfiles: bool=False) -> str:
        """
        Stop  measurement

        This is the KATCP wrapper for the measurement_stop command

        Return:
            katcp reply object [[[ !measurement_start ok | (fail [error description]) ]]]
        """
        _log.debug('Received measurement stop request with reply outputfiles %s', reply_outputfiles)
        await self.measurement_stop()

        res = json.dumps(self._output_files)
        _log.debug('%i outputfiles created by pipeline during measurement', len(self._output_files))
        self._output_files.clear()

        if int(reply_outputfiles):
            return res


    async def request_capture_stop(self, ctx: RequestContext) -> str:
        """
        Stop the EDD backend processing

        This is the KATCP wrapper for the capture_stop command

        Return:
            katcp reply object [[[ !capture_stop ok | (fail [error description]) ]]]
        """
        await self.capture_stop()


    async def request_deconfigure(self, ctx: RequestContext) -> str:
        """
        Deconfigure the pipeline.

        This is the KATCP wrapper for the deconfigure command

        Return:
            katcp reply object [[[ !deconfigure ok | (fail [error description]) ]]]
        """
        self.flush_notes()
        self._cancel_pipeline_tasks()
        await self.deconfigure()

    # ----------------- #
    # Common coroutines #
    # ----------------- #

    async def set(self, config_json: str):
        """
        Add the config_json to the current config. Input / output data streams
        will be filled with default values if not provided.

        The configuration will be rejected if no corresponding value is present
        in the default config. A warning is emitted on type changes.

        The final configuration is stored in self._config for access in derived classes.

        Returns:
            katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        _log.debug("Updating configuration: '{}'".format(config_json))
        cfg = cfgjson2dict(config_json)
        try:
            newcfg = updateConfig(self._config, cfg)
            # await self.check_config(newcfg)
            self._config = newcfg
            _log.debug("Updated config: '{}'".format(self._config))
        except KeyError as error:
            raise KeyError("Unknown configuration option: {}".format(str(error)))
        except Exception as error:
            raise Exception("Unknown ERROR: {}".format(str(error)))

    async def register(self, host=None, port=None):
        """
        Registers the pipeline in the data store.

        Args:
            host, port: IP and port of the data store.

        If no host and port are provided, values from the internal config are used.
        """
        if host is None:
            _log.debug("No host provided. Use value from current config.")
            host = self._config["data_store"]["ip"]
        if port is None:
            _log.debug("No port provided. Use value from current config.")
            port = self._config["data_store"]["port"]
        _log.debug("Register pipeline in data store @ {}:{}".format(host, port))
        dataStore = datastore.EDDDataStore(host, port)
        dataStore.updateProduct(self._config)

    # ----------------------- #
    # State change coroutines #
    # ----------------------- #
    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Default method for configuration.
        """
        _log.info("Running configure.")

    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """
        Default method - no effect
        """

    @state_change(target="set", allowed=["ready"], ignored=["streaming"], intermediate="measurement_preparing")
    async def measurement_prepare(self, config=None):
        """Default method - no effect"""

    @state_change(target="measuring", allowed=["set"], ignored=["streaming"], intermediate="measurement_starting")
    async def measurement_start(self):
        """Default method - no effect"""

    @state_change(target="ready", allowed=["measuring", "set"], ignored=['streaming', 'ready'], intermediate="measurement_stopping")
    async def measurement_stop(self):
        """Default method - no effect"""

    @state_change(target="ready", allowed=["configured"], intermediate="capture_stoping")
    async def capture_stop(self):
        """Default method - no effect"""

    @state_change(target="idle", intermediate="deconfiguring")
    async def deconfigure(self):
        """Default method - no effect"""


def getArgumentParser(description="", include_register_command=True):
    """
    Creates an argument parser with standard arguments for all pipelines. By
    this all pipelines have a standard set of command-line options for pipeline
    start and stop.

    Returns:
        ArgumentParser
    """
    parser = ArgumentParser(description=description)
    parser.add_argument('-H', '--host', dest='host', type=str, default='localhost',
                        help='Host interface to bind to')
    parser.add_argument('-p', '--port', dest='port', type=int, default=1235,
                        help='Port number to bind to. Use 0 to select port automatically from Kernel ip_local_port_range')
    parser.add_argument('--log-level', dest='log_level', type=str,
                        help='Port number of status server instance', default="INFO")
    if include_register_command:
        parser.add_argument('--register-id', dest='register_id', type=str,
                            help='The default pipeline to datastore.')
    parser.add_argument('--redis-ip', dest='redis_ip', type=str, default="localhost",
                        help='The ip for the redis server')
    parser.add_argument('--redis-port', dest='redis_port', type=int, default=6379,
                        help='The port number for the redis server')
    return parser


async def launchPipelineServer(pipeline: EDDPipeline, args=None):
    """
    Launch a pipeline server and install signal-handler to kill server on CTRL-C.

    Args:
        pipeline: Instance of a pipeline or ServerClass definition to launch.
        args: ArgumentParser args to use for launch of the pipeline.

    Example:
        Start a pipeline using a class definition::

            class MyPipeline(EDDPipeline):
                pass

            asyncio.run(launchPipelineServer(MyPipeline))
    """
    if not args:
        parser = getArgumentParser()
        args = parser.parse_args()

    logging.setLevel(args.log_level.upper())

    if isinstance(pipeline, type):
        _log.info("Created pipeline instance")
        server = pipeline(args.host, args.port)
    else:
        server = pipeline

    try:
        server.updateConfig({'data_store': {"ip": args.redis_ip, "port": args.redis_port}})
    except AttributeError as E:
        _log.trace(E)
        _log.debug('Not injecting redis ip!')

    if args.register_id:
        await server.set({"id": args.register_id})
        await server.register(args.redis_ip, args.redis_port)
    _log.info("Starting pipeline server")
    await server.start()
    asyncio.get_event_loop().add_signal_handler(signal.SIGINT, server.halt)
    _log.debug("Started pipeline server")
    _log.info("CTRL-C to terminate server")
    await server.join()
