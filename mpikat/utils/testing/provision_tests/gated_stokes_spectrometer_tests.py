import asyncio
import json

from mpikat.core import logger

from . registry import  register
from . common import Stage

_log = logger.getLogger('mpikat.utils.testing.provision_tests.gated_stokes_spectrometer_tests')

@register(stage=Stage.capture_start, class_name='GatedStokesSpectrometer')
async def test_is_receiving_data(context):
    """
    The pipeline should receive data.
    """
    _, results = await context.pipeline.request("sensor-value", f'integration-time')
    integration_time = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Waiting for 3x integration_time (%.1E)s:', integration_time)
    await asyncio.sleep(3*integration_time)

    _, results = await context.pipeline.request("sensor-value", f'input-buffer-total-write')
    i1 = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Received input buffer reads: %s', i1)

    assert i1 > 0, f"No input buffers processed!"


@register(stage=Stage.capture_start, class_name='GatedStokesSpectrometer')
async def test_is_sending_data(context):
    """
    The pipeline should send data.
    """
    _, results = await context.pipeline.request("sensor-value", f'integration-time')
    integration_time = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Waiting for 3x integration_time (%.1E)s:', integration_time)
    await asyncio.sleep(3*integration_time)

    _, results = await context.pipeline.request("sensor-value", f'output-buffer-total-read')
    i1 = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Received output buffer reads: %s', i1)

    assert i1 > 0, f"No output buffers processed! "


@register(stage=Stage.capture_start, class_name='GatedStokesSpectrometer')
async def test_epoch_difference(context):
    """
    Difference of mkrecv data to current time should be of the order of NTP sync time (better than 100 ms)
    """
    _, results = await context.pipeline.request("sensor-value", f'epoch_difference')
    dt = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Epoch difference: %.3fs', dt)
    assert dt < .1, "Epoch difference larger than 100 ms!"
