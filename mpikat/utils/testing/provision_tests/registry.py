import functools


from mpikat.core import logger
from . common import Stage, EDDTest

_log = logger.getLogger('mpikat.utils.testing.provision_tests.registry')

class Registry:
    """
    Test registry. All test in the registry are executed by the runner/
    """
    def __init__(self):
        self.tests_stages = {}
        self.tests_classes = {}
        self.clear()

    def clear(self):
        """
        Remove all test from registry
        """
        for s in Stage:
            self.tests_stages[s] = set()

    def add(self, test):
        """
        Add a test to the registry
        """
        if test.pipeline_name:
            _log.debug(f'Registering test {test} via pipeline_name')
            self.tests_stages[test.stage].add(test)
        elif test.class_name:
            _log.debug(f'Registering test {test} via class_name')
            if test.class_name not in self.tests_classes:
                self.tests_classes[test.class_name] = set()
            self.tests_classes[test.class_name].add(test)
        else:
            RuntimeError(f'Unclear test {test}')

    def get_tests_for_stage(self, stage):
        """
        Return  all test for a stage
        """
        return self.tests_stages[stage]

    def add_class_tests(self, product_configs):
        """
        Adds all tests registered by class to the correct pipeline for a given provision config
        """
        for p, c in product_configs.items():
            class_type = c['type']
            if not class_type in self.tests_classes:
                continue
            for test in self.tests_classes[class_type]:
                self.tests_stages[test.stage].add(EDDTest(func=test.func, stage=test.stage, pipeline_name=p, name=test.name, tbc=test.tbc))

# registry singleton
_registry = Registry()

def register(stage, pipeline_name="", class_name="", tbc=False, name="", registry=_registry):
    """
    Decorator to register a function.
    Args:
        stage:         Stage to register the test to
        pipeline_name: Pipeline the test acts on
        class_name:    Name of the class the test should act on. (can be also a list)
        tbc:           If true, the test result will have to be checked manually (TBC =to-be-checked)
        name:          Name of the test
        registry:      Registry to register the test in.
    """
    def func_wrapper(func):
        if isinstance(class_name, list):
            for cn in class_name:
                registry.add(EDDTest(stage, func, pipeline_name=pipeline_name, class_name=cn, name=name, tbc=tbc))
        else:
            registry.add(EDDTest(stage, func, pipeline_name=pipeline_name, class_name=class_name, name=name, tbc=tbc))
        @functools.wraps(func)
        def wrapper():
            func()
        return wrapper
    return func_wrapper


def instance():
    """
    return registry singleton
    """
    return _registry


