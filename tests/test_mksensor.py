# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring
import unittest
from mpikat.utils.mk_tools import MkrecvSensors

class TestMkrecvSensors(unittest.TestCase):
    def setUp(self):
        self.mks = MkrecvSensors('test')

    def test_global_heap_sensors(self):
        self.assertEqual(self.mks.sensors['global_completed_heaps'].value, 0)
        self.assertEqual(self.mks.sensors['global_missing_heaps'].value, 0)

        data = {'global-heaps-completed': 23, 'global-heaps-discarded': 42}

        self.mks.update_sensors(data, 123)

        # all updated sensors should have same timestamp
        self.assertEqual(self.mks.sensors['global_completed_heaps'].timestamp, 123.)
        self.assertEqual(self.mks.sensors['global_missing_heaps'].timestamp, 123.)


        self.assertEqual(self.mks.sensors['global_completed_heaps'].value, 23.)
        self.assertEqual(self.mks.sensors['global_missing_heaps'].value, 42.)

if __name__ == "__main__":
    unittest.main()
