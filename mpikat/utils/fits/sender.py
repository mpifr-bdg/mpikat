import errno
import socket
import struct
import time
import queue
from threading import Thread, Event

import astropy.time
import numpy as np

from mpikat.utils.fits.format import FitsPacket
from mpikat.core import logger
_log = logger.getLogger("mpikat.utils.fits.fits_sender")

_FW_QUEUE_SIZE = 2048 # max Size of output queue
_SOCKET_TIMEOUT = 12 # Ensure that we tmeout only after the Effelsberg Fits Writer


class Sender(Thread):
    """
    Manages TCP connections to the APEX FITS writer.

    This class implements a TCP/IP server that can accept connections from
    the FITS writer. Upon acceptance, the new communication socket is stored
    and packages from a local queue are streamed to the fits writer.

    Packages are delayed by (at default) 3s, as the pacakges might be out of
    time-order, and the fits writer expects to receive the packages in strict
    order.
    """
    def __init__(self, ip: str, port: int, delay: int = 3, keep_socket_alive: bool = False):
        """Constructor of Sender

        Args:
            ip (str): The IP address to accept connectons from.
            port (int): The port to serve on
            delay (int, optional): Number of seconds a package is kept in the queue before
                submission. Defaults to 3.
            keep_socket_alive (bool, optional): If True, the transmit socket will not be closed but
                kept alive until the enxt scan starts. This is needed for some FIts Writer
                implementations. Defaults to False.
        """
        super().__init__()
        self._shutdown = Event()
        self._has_connection = Event()
        self.is_measuring = Event()
        self._has_connection.clear()

        self.__output_queue = queue.PriorityQueue(maxsize=_FW_QUEUE_SIZE)
        self.__delay = delay
        self.__latest_package_time = b""
        self.send_items = 0
        self.dropped_items = 0

        self._transmit_socket = None
        self._keep_socket_alive = keep_socket_alive
        # Socket/server instantiation should probably go into seperate function
        self._server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._server_socket.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
        self._server_socket.setblocking(False)
        _log.info("Binding to %s:%i", ip, port)
        self._server_socket.bind((ip, port))
        self._server_socket.listen(1)


        self.start_time = np.inf

    def is_connected(self) -> bool:
        """
        Return true if it is connected
        """
        if self._has_connection.is_set():
            return True
        return False

    def _accept_connection(self) -> socket.socket:
        """_summary_

        Raises:
            error: _description_

        Returns:
            socket.socket: _description_
        """
        _log.debug("Accepting connections on FW server socket")
        while not self._shutdown.is_set():
            try:
                sock, addr = self._server_socket.accept()
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER,
                                                 struct.pack('ii', 1, _SOCKET_TIMEOUT))
                sock.settimeout(_SOCKET_TIMEOUT)
                self._has_connection.set()
                self.send_items = 0
                self.dropped_items = 0
                _log.info("Received connection from %s", addr)
                return sock
            except socket.error as error:
                if error.args[0] == errno.EAGAIN or error.args[0] == errno.EWOULDBLOCK:
                    time.sleep(1)
                    continue
                else:
                    _log.exception("Unexpected error on socket accept: %s", str(error))
                    raise error


    def wait_connected(self):
        """Waits until a client is connected
        """
        while not self._has_connection.is_set():
            time.sleep(.1)


    def disconnect(self):
        """
        Drop any current FITS writer connection. The Sender will be ready for new connections.
        """
        self._has_connection.clear()
        self.start_time = np.inf
        if self._transmit_socket is None or self._keep_socket_alive:
            _log.debug("No connection present to drop (or drop disabled)")
            return
        _log.debug("Dropping connection")
        try:
            self._transmit_socket.shutdown(socket.SHUT_RDWR)    # pylint: disable=no-member
            _log.debug("Socket shut-down")
        except Exception as E:
            _log.debug("Error shuting down socket - just dropping %s", E)
        try:
            self._transmit_socket.close()
            _log.debug("Socket closed")
        except Exception as E:
            _log.error("Error closing socket %s", E)
        self._transmit_socket = None


    def put(self, packet: FitsPacket):
        """
        Put a new output packet on the local queue if there is a fits server connection.
            Drop packages otherwise.
        """
        if self.is_measuring.is_set():
            pkt = astropy.time.Time(packet.header.timestamp[:24]).unix
            if pkt < self.start_time:
                _log.warning("package received with timestamp before start time: %f < %f",
                             pkt, self.start_time)
            else:
                try:
                    self.__output_queue.put([packet.header.timestamp, packet, time.time()])
                except queue.Full:
                    _log.error("Output queue full!")
        else:
            _log.debug("Not measuring: dropping package.")


    def send(self):
        """
        Send all packages in the queue older than delay seconds.
        """
        now = time.time()
        _log.debug('Send data, Queue size: %d', self.__output_queue.qsize())

        while not self.__output_queue.empty():
            ref_time, packet, timestamp = self.__output_queue.get(timeout=1)
            self.__output_queue.task_done()
            if self.is_measuring.is_set():
                # Packet too young, there might be others. Put back and return.
                if (now - timestamp) < self.__delay:
                    self.__output_queue.put([ref_time, packet, timestamp])
                    return
            else:
                _log.debug("Flushing queue. Current size: %d", self.__output_queue.qsize())

            if ref_time < self.__latest_package_time:
                _log.warning("Got ref time %s in queue, but previously send %s. Dropping.",
                             ref_time, self.__latest_package_time)
                self.dropped_items += 1
            else:
                _log.debug('Send item %d with ref time %s. Fits timestamp: %s', self.send_items,
                           time.ctime(timestamp), packet.header.timestamp)
                self.__latest_package_time = ref_time
                self._transmit_socket.sendall(bytes(packet))  # pylint: disable=no-member
                self.send_items += 1


    def queue_is_empty(self):
        """
        Check if output queue is empty
        """
        return self.__output_queue.empty()


    def clear_queue(self):
        """
        Empty queue without sending
        """
        self.send_items = 0
        self.dropped_items = 0
        while not self.__output_queue.empty():
            self.__output_queue.get(timeout=1)
            self.__output_queue.task_done()


    def stop(self):
        """
        Stop the server
        """
        self._shutdown.set()


    def run(self):
        """
        main loop of the server. The server will wait for a FITS writer
        connection and send any data as long as the connection exists. If
        connection drops, it will wait for a new connection.
        """
        while not self._shutdown.is_set():
            if not self._has_connection.is_set():
                self.disconnect()  # Just in case
                try:
                    self._transmit_socket = self._accept_connection()
                except Exception as error:
                    self.disconnect()
                    _log.exception(str(error))
                    continue
                else:
                    self.start_time = time.time()
                    _log.info("Setting start time to %f", self.start_time)
            else:
                try:
                    self.send()
                except Exception as error:
                    _log.error("Error sending data %s.", error)
                    self.disconnect()
                    self.is_measuring.clear()
                    self.clear_queue()
            time.sleep(0.1)
        time.sleep(0.1)
        self.disconnect()
        self._server_socket.close()
