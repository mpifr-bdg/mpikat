import time
import json
import spead2
import spead2.recv
import packaging.version
from threading import Thread, Event

from mpikat.core.data_stream import convert48_64
import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.utils.spead_capture")


class SpeadCapture(Thread):
    """
    Captures heaps from one or more streams that are transmitted in SPEAD
    format and passes them from spead_handler to the package_handler.
    """
    DEFAULT_CONFIG = {
        "numa_affinity": [],
        "interface_address": "",
        "memory_pool": {
            "lower": 16384,
            "upper": (2*4*16*1024**2)+1024,
            "max_free": 128,
            "initial": 128},
        "threadpoolsize": 1,        # yes, as we only have one stream. Should use multiple streams otherwise
        "stream_config_max_heaps": 64,
        "ringstream_size": 128,
        }

    def __init__(self, input_streams, speadhandler, package_handler, spead_config=None):
        """
        Args:
            mc_ip:              Array of multicast group IPs
            mc_port:            Port number of multicast streams
            speadhandler:       Object that handles data in the stream
            numa_affinity:      List of NUMA cores used for affinity of spead2 capture threads
        """
        Thread.__init__(self, name=self.__class__.__name__)
        if spead_config is None:
            spead_config = self.DEFAULT_CONFIG

        self._stop_event = Event()
        self._spead_handler = speadhandler
        self._package_handler = package_handler

        _log.debug("Initializing SpeadCapture with config:\n%s", json.dumps(spead_config, indent=4))

        if packaging.version.parse(spead2.__version__) >= packaging.version.parse("3.2.0"):
            thread_pool = spead2.ThreadPool(threads=spead_config['threadpoolsize'], affinity=[int(k) for k in spead_config["numa_affinity"]])
            pool = spead2.MemoryPool(lower=spead_config['memory_pool']['lower'], upper=spead_config['memory_pool']['upper'], max_free=spead_config['memory_pool']['max_free'], initial=spead_config['memory_pool']['initial'])
            stream_config = spead2.recv.StreamConfig(bug_compat=spead2.BUG_COMPAT_PYSPEAD_0_5_2, max_heaps=spead_config["stream_config_max_heaps"], memory_allocator=pool, allow_out_of_order=True)     # pylint: disable=no-member
            ring_stream_config = spead2.recv.RingStreamConfig(heaps=spead_config["ringstream_size"], contiguous_only=False)     # pylint: disable=no-member
            self.stream = spead2.recv.Stream(thread_pool, stream_config, ring_stream_config)
        else:
            _log.warning("Using old spead2 version, %s", spead2.__version__)
            thread_pool = spead2.ThreadPool(threads=spead_config['threadpoolsize'], affinity=[int(k) for k in spead_config["numa_affinity"]])
            self.stream = spead2.recv.Stream(thread_pool, spead2.BUG_COMPAT_PYSPEAD_0_5_2, max_heaps=spead_config["stream_config_max_heaps"], ring_heaps=spead_config["ringstream_size"], contiguous_only=False)
            pool = spead2.MemoryPool(lower=spead_config['memory_pool']['lower'], upper=spead_config['memory_pool']['upper'], max_free=spead_config['memory_pool']['max_free'], initial=spead_config['memory_pool']['initial'])
            self.stream.set_memory_allocator(pool)

        _log.debug("Subscribe to multicast groups:")
        for i, ips in enumerate(input_streams):
            _log.debug(" - Subs {}: ip: {}, port: {}".format(i, ips["ip"], ips["port"]))
            self.stream.add_udp_reader(ips["ip"], int(ips["port"]), max_size=9200,
                                       buffer_size=1073741820, interface_address=spead_config["interface_address"])

        self.nheaps = 0
        self.incomplete_heaps = 0
        self.complete_heaps = 0

    def stop(self):
        """
        Stop the capture thread
        """
        self.stream.stop()
        self._stop_event.set()

    def run(self):
        """
        Subscribe to MC groups and start processing.
        """
        _log.debug("Start processing heaps:")
        self.nheaps = 0
        self.incomplete_heaps = 0
        self.complete_heaps = 0

        while not self._stop_event.is_set():
            try:
                heap = self.stream.get_nowait()
            except spead2.Empty:
                time.sleep(0.5)
                continue
            except spead2._spead2.Stopped:
                _log.warning('Stream has been stopped, but not by the event!')
                break

            self.nheaps += 1
            _log.trace('Received heap %i - previously %i completed, %i incompleted', self.nheaps, self.complete_heaps, self.incomplete_heaps)
            if isinstance(heap, spead2.recv.IncompleteHeap):
                _log.warning('Received incomplete heap - received only %i / %i bytes.', heap.received_length, heap.heap_length)
                self.incomplete_heaps += 1
                continue
            self.complete_heaps += 1
            try:
                package = self._spead_handler(heap)
            except Exception as E:
                _log.error("Error handling heap. Cought exception:\n{}".format(E))

            try:
                self._package_handler(package)
            except Exception as E:
                _log.error("Error handling decoded data package. Cought exception:\n{}".format(E))
                _log.exception(E, exc_info=True)

        _log.debug('Stream done.')



class SpeadPacket:  # pylint: disable=too-few-public-methods
    """
    Contains the SPEAD items as members with conversion.
    """
    def __init__(self, items):
        self.integration_period = None
        self.reference_time = None

        for item in items.values():
            if item.name != 'data':
                setattr(self, item.name, convert48_64(item.value))
            else:
                setattr(self, item.name, item.value)
