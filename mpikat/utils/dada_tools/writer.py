import time
import asyncio
from mpikat.utils.dada_tools.reader import DadaClient
from mpikat.utils.dada_tools.header import make_dada_header

class DadaWriteClient(DadaClient):
    """
    is derived from DadaClient and the base class for all writing clients.
    Writing clients are clients which are writing TO the DADA buffer. It
    implement the method wait(), allowing to wait until the CLI program
    is exited.
    """
    def __init__(self, key: str, stdout_handler: callable=None, physcpu: str=None):
        """Base class for Writing clients

        Args:
            key (str): The hex key used for the DADA buffer
            stdout_handler (callable): A function that parses the standard output
                of the client
            physcpu (str): CPU cores on which to run the process

        Returns:
            DadaWriteClient: An instance of the the DadaWriteClient class.

        """
        DadaClient.__init__(self, key, stdout_handler, physcpu)

    def block_wait(self):
        """Waits for the DadaWriteClient to stop and blocks the calling thread."""
        while self.proc.is_alive():
            time.sleep(1)

    async def wait(self):
        """Waits asnyc for the DadaWriteClient to to stop."""
        while self.proc.is_alive():
            await asyncio.sleep(1)

class DiskDb(DadaWriteClient):
    """
    is a write client,  whic reads the contents of a file and writes it to
    the attached DADA buffer
    """
    def __init__(self, key: str, fname: str="/tmp/test.dat", header: str="", n_reads: int=1,
                 data_rate: float=0, stdout_handler: callable=None, physcpu: str=None):
        """is a write client, which reads the contents of a file and writes it
            to the attached DADA buffer

        Args:
            key (str): see DadaClient
            fname (str): The file that should be read and written to the buffer
            header (str): Name of the header file if any
            n_reads (int): Number of read repitions of the file
            data_rate (float): The data rate to read the file
            stdout_handler (callable): see DadaClient
            physcpu (str): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.

        """
        DadaWriteClient.__init__(self, key, stdout_handler, physcpu)
        self.fname = fname
        self.n_reads = n_reads
        self.header = header
        if self.header == "":
            self.header = make_dada_header()
        self.data_rate = data_rate
        self.cmd += "diskdb -k {}".format(self.key)
        self.cmd += " -d {}".format(self.fname)
        self.cmd += " -f {}".format(self.header)
        self.cmd += " -r {}".format(self.n_reads)
        self.cmd += " -s {}".format(self.data_rate)


class SyncDb(DadaWriteClient):
    """
    is a write client, which fills multiple DADA buffer synchronously.
    """
    def __init__(self, key: str, n_bytes: int=0, sync_epoch: int=0, period: int=1,
                 ts_per_block: int=1048576, stdout_handler: callable=None, physcpu: str=None):
        """is a write client, which fills multiple DADA buffer synchronously.
        Args:
            key (str): see DadaClient
            n_bytes (int):
            sync_epoch (int): The start time to sync to
            period (int):
            ts_per_block (int):
            stdout_handler (callable): see DadaClient
            physcpu (str): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.
        """
        DadaWriteClient.__init__(self, key, stdout_handler, physcpu)
        self.n_bytes = int(n_bytes)
        self.sync_epoch = int(sync_epoch)
        self.period = float(period)
        self.ts_per_block = int(ts_per_block)
        self.cmd += "syncdb -k {}".format(self.key)
        self.cmd += " -n {}".format(self.n_bytes)
        self.cmd += " -s {}".format(self.sync_epoch)
        self.cmd += " -p {}".format(self.period)
        self.cmd += " -t {}".format(self.ts_per_block)


class JunkDb(DadaWriteClient):
    """
    is a write client, which fills a single DADA buffer with "junk".
    """
    def __init__(self, key: str, header: str="", data_rate: float=64e6, duration: float=60,
                 charachter: str="", zero_copy: bool=True, gaussian: bool=False,
                 stdout_handler: callable=None, physcpu: str=None):
        """is a write client, which fills a single DADA buffer with "junk"

        Args:
            key (str): see DadaClient
            header (str): The DADA header if any
            data_rate (int): The data rate to write with
            duration (int): The duration to write
            charachter (char): Only write this charachter to the buffer
            zero_copy (bool): Avoid any copies to the buffer (direct access)
            gaussian (bool): Random gaussian noise to the buffer
            stdout_handler (callable): see DadaClient
            physcpu (str): see DadaClient

        Returns:
            DbNull: An instance of the the DbNull class.
        """
        DadaWriteClient.__init__(self, key, stdout_handler, physcpu)
        self.header = header
        if self.header == "":
            self.header = make_dada_header()
        self.data_rate = data_rate
        self.duration = duration
        self.cmd += "dada_junkdb -k {}".format(self.key)
        self.cmd += " -r {}".format(int(self.data_rate//(1e6)))
        self.cmd += " -t {}".format(self.duration)
        if zero_copy:
            self.cmd += " -z "
        if gaussian:
            self.cmd += " -g "
        if charachter:
            self.cmd += " -c {}".format(charachter)
        self.cmd += " "  + str(self.header)
