import asyncio
import json

import shutil
import os
import tempfile
import unittest
import unittest.mock as mock

import redislite

from mpikat.pipelines.pulsar_processing import EddPulsarPipeline
from mpikat.core import datastore
from mpikat.utils.dada_tools.ringbuffer import rm_buffers
from mpikat.utils import testing, get_port


# Stsart temp dir
data_dir = None
def setUpModule():
    global data_dir
    data_dir = tempfile.mkdtemp()
def tearDownModule():
    shutil.rmtree(data_dir)


# mocks for implementation tzpar access
def mock_check_tzpar_repo(*args):
    tzpar_dir = os.path.join(data_dir, 'tz_pardir')
    return tzpar_dir

def mock_par_file_check(*args):
    tzpar_file = os.path.join(data_dir, 'tz_pardir', 'tzparfile')
    pulsar_flag = True
    return tzpar_file, pulsar_flag


unittest.mock.patch("mpikat.pipelines.pulsar_processing.PulsarBase._check_tzpar_repo", wraps=mock_check_tzpar_repo).start()
unittest.mock.patch("mpikat.pipelines.pulsar_processing.EddPulsarTimingPipeline._par_file_check", wraps=mock_par_file_check).start()

class TestEddPulsarPipeline(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.pipeline = EddPulsarPipeline("localhost", get_port())
        self.config_json = {"cdd_dm":100}
        self.redis_db = testing.setup_redis()
        self.pipeline._config["data_store"] = {"ip": "127.0.0.1", "port": self.redis_db.server_config['port']}

        ds = datastore.EDDDataStore(port=self.redis_db.server_config['port'], host='127.0.0.1')
        ds.setTelescopeDataItem("source-name", "B1937+21")
        ds.setTelescopeDataItem("ra", 123)
        ds.setTelescopeDataItem("dec", -45)
        ds.setTelescopeDataItem("scannum",  "0000")
        ds.setTelescopeDataItem("subscannum", "0")
        ds.setTelescopeDataItem("receiver", "P217")
        ds.setTelescopeDataItem("project", "TEST")
        ds.setTelescopeDataItem("noise_diode_pattern", '{"period":0.5,"percentage":0.5}')


    async def asyncTearDown(self):
        # Clear out remaining buffer after crash
        for buffer in self.pipeline._implementation._dada_buffers:
            await buffer.destroy()
        self.redis_db.shutdown()

    async def __test_sequence(self):

        self.assertEqual(self.pipeline.state, 'idle')
        await self.pipeline.configure()
        self.assertEqual(self.pipeline.state, 'configured')

        await self.pipeline.capture_start()
        self.assertEqual(self.pipeline.state, 'ready')

        await self.pipeline.measurement_prepare(self.config_json)
        self.assertEqual(self.pipeline.state, 'set')

        # Ignore mesaurement start, stop prepare
        await self.pipeline.measurement_start()
        self.assertEqual(self.pipeline.state, 'measuring')

        await self.pipeline.measurement_stop()
        self.assertEqual(self.pipeline.state, 'ready')

        #await self.pipeline.measurement_prepare()
        #self.assertEqual(self.pipeline.state, 'set')

        #await self.pipeline.capture_stop()
        #self.assertEqual(self.pipeline.state, 'idle')

        # This test needs to be available,as otherwise on successfull test
        # pycoverage wont exit
        await self.pipeline.deconfigure()
        self.assertEqual(self.pipeline.state, 'idle')


    async def test_pulsar_processing_timing_sequence(self):
        await self.pipeline.set({"mode":"Timing","dummy_input":True,"input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        await self.__test_sequence()


    async def test_pulsar_processing_search_sequence(self):
        await self.pipeline.set({"mode":"Searching","dummy_input":True,"input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        await self.__test_sequence()


    async def test_pulsar_processing_baseband_sequence(self):
        await self.pipeline.set({"mode":"Baseband","dummy_input":True,"input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        await self.__test_sequence()


    async def test_pulsar_processing_leap_sequence(self):
        await self.pipeline.set({"mode":"Leap","dummy_input":True,"input_data_streams":[
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":0
            },
            {
                "format": "MPIFR_EDD_Packetizer:1",
                "ip":"225.0.0.156+2",
                "port": "7148",
                "sync_time":0,
                "sample_rate":3200000000,
                "samples_per_heap" :4096,
                "central_freq": 1400,
                "polarization":1
            }
        ]
        }
        )
        await self.__test_sequence()


if __name__ == '__main__':
    unittest.main()
