import asyncio
import json

from mpikat.core import logger

from . registry import  register
from . common import Stage

_log = logger.getLogger('mpikat.utils.testing.provision_tests.gated_spectrometer_tests')

@register(stage=Stage.capture_start, class_name='GatedSpectrometer')
async def test_is_receiving_data(context):
    """
    The pipeline should receive data.
    """
    # Check number of expected polarizations
    _log.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    active_polarizations = [i['polarization'] for i in cfg['input_data_streams']]
    _log.debug('Active polarizations: %s', active_polarizations)

    _, results = await context.pipeline.request("sensor-value", f'integration-time')
    integration_time = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Waiting for 3x integration_time (%.1E)s:', integration_time)
    await asyncio.sleep(3*integration_time)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-total-write-polarization_{p}')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Received input buffer reads: %s', i1)

    for p in i1:
        assert i1[p] > 0, f"No input buffers processed for polarization {p}, got {i1[p]}  and {i1[p]} buffers "


@register(stage=Stage.capture_start, class_name='GatedSpectrometer')
async def test_is_sending_data(context):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _log.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))

    active_polarizations = [i['polarization'] for i in cfg['input_data_streams']]
    _log.debug('Active polarizations: %s', active_polarizations)

    _, results = await context.pipeline.request("sensor-value", f'integration-time')
    integration_time = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Waiting for 3x integration_time (%.1E)s:', integration_time)
    await asyncio.sleep(3*integration_time)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'output-buffer-total-read-polarization_{p}')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _log.debug('Received output buffer reads: %s', i1)

    for p in i1:
        assert i1[p] > 0, f"No output buffers processed for polarization {p}, got {i1[p]}  and {i1[p]} buffers "

@register(stage=Stage.capture_start, class_name='GatedSpectrometer')
async def test_epoch_difference(context):
    """
    Difference of mkrecv data to current time should be of the order of NTP sync time (better than 100 ms)
    """
    # Check number of expected polarizations
    _log.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))

    active_polarizations = [i['polarization'] for i in cfg['input_data_streams']]
    _log.debug('Active polarizations: %s', active_polarizations)

    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'epoch_difference-polarization_{p}')
        dt = float(results[0].arguments[4].decode('ascii'))
        _log.debug('Epoch difference for polarization %s: %.3fs', p, dt)
        assert dt < .1, "Epoch difference larger than 100 ms!"




