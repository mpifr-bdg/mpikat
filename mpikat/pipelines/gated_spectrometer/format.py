import spead2
from mpikat.core.data_stream import DataStream, edd_spead, convert48_64

import mpikat.core.logger

_log = mpikat.core.logger.getLogger("mpikat.pipelines.gated_spectrometer.format")


class GatedSpectrometerDataStreamFormat(DataStream):
    """
    Data stream output for the gated spectrometer
    """

    data_items = [edd_spead(5632, "timestamp_count"),
                  edd_spead(5633, "polarization"),
                  edd_spead(5634, "noise_diode_status"),
                  edd_spead(5635, "fft_length"),
                  edd_spead(5636, "number_of_input_samples"),
                  edd_spead(5637, "sync_time"),
                  edd_spead(5638, "sampling_rate"),
                  edd_spead(5639, "naccumulate"),
                  edd_spead(5640, "data", "", shape=(), dtype="<f"),
                  edd_spead(5641, "number_of_saturated_samples"),
                  edd_spead(5642, "type"),  # 0: dual-pol, 1: Stokes
                 ]
    stream_meta = {
        "type": "GatedSpectrometer:1",
        "ip": "",
        "port": "",
        "description": "Spead stream of integrated spectra.",
        "central_freq": "",
        "receiver_id": "",
        "band_flip": ""
    }

    @classmethod
    def ig_update(cls, items: dict, ig: spead2.ItemGroup) -> bool:
        """
        Update an item group dependent on the received items
        """

        # On first heap only, get number of channels
        if 'data' in items:
            return False

        fft_length = convert48_64(items['fft_length'].value)
        nchannels = int((fft_length / 2) + 1)
        _log.debug("First item - setting data length to %i channels", nchannels)
        d = cls.get_data_item(name='data')
        ig.add_item(d.id, d.name, d.description, (nchannels,), d.dtype)

        return True

