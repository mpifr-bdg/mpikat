import unittest
import time
from aiokatcp.sensor import Sensor as AIOSensor
from mpikat.utils.sensor_watchdog import SensorWatchdog

def watchdog_error():
        pass
        
class Test_SensorWatchdogAiokatcp(unittest.TestCase):

    def setUp(self) -> None:
        self.sensor_under_test = AIOSensor(
            float, "test-me",
            description="Sensor for testing",
            initial_status=AIOSensor.Status.UNKNOWN,
        )
        self.watchdog = SensorWatchdog(self.sensor_under_test, 1, watchdog_error)
        self.watchdog.start()
    
    def tearDown(self) -> None:
         self.watchdog.stop_event.set()
         self.watchdog.join()

    def test_aiokatcp_sensor_read(self):
        """
        """
        iter = 10
        for i in range(iter):
            self.sensor_under_test.set_value(1)
            time.sleep(0.5)
        self.assertEqual(i, iter-1)
        self.assertTrue(self.watchdog.is_alive())

    def test_aiokatcp_sensor_timeout(self):
        """
        """
        time.sleep(1.5)
        self.assertFalse(self.watchdog.is_alive())

if __name__ == "__main__":
    unittest.main()