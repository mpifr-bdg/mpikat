import asyncio
import time
import numpy as np

from mpikat.core.edd_pipeline_aio import launchPipelineServer, getArgumentParser
from mpikat.core.telescope_meta_information_server import TMIServer
from mpikat.core.datastore import EDDDataStore

import mpikat.core.logger

_log = mpikat.core.logger.getLogger('mpikat.ska_proto.status_server')

class SKAMPIStatusServer(TMIServer):
    """
    Status Server for the SKA-MPI prototype dish
    """
    VERSION_INFO = "SMAMPI_STATUS_SERVER-1.0"
    BUILD_INFO = "SKAMPI_STATUS_SERVER-1.0"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.sensors.pop("wind-speed")
        self.sensors.pop("wind-direction")
        self.sensors.pop("air-temperature")
        self.sensors.pop("air-pressure")
        self.sensors.pop("humidity")
        self.sensors.pop("rain")



    async def ensure_update(self, cfg=None):
        _log.debug("Ensuring update of status server")
        if cfg is None:
            _log.warning("Empty measurement prepare received. No TMI update!")
            return

        timestamp = time.time()

        try:
            observation_config = cfg['observation_config']
        except KeyError:
            _log.warning("Observation config not available!")
            observation_config = {}
        try:
            scan = cfg['observation_config']['scans'][cfg['scannumber']]
        except KeyError:
            _log.warning("Scan not available!")
            scan = {}

        self.sensors['receiver'].set_value(observation_config.get('band', 'UNKNOWN'), timestamp=timestamp)
        self.sensors['observation-id'].set_value(observation_config.get('observation_id', '-'), timestamp=timestamp)
        #Those are required by the pulsar pipeline but only available in
        #Effelsberg
        #self.sensors['scannum'].set_value(observation_config.get('observation_id', '-'), timestamp=timestamp)
        #self.sensors['subscannum'].set_value(observation_config.get('scannumber', '-'), timestamp=timestamp)
        self.sensors['project'].set_value(str(observation_config.get('project_id', 0)), timestamp=timestamp)

        self.sensors['ra'].set_value(float(scan.get('ra', np.nan)), timestamp=timestamp)
        self.sensors['dec'].set_value(float(scan.get('dec', np.nan)), timestamp=timestamp)
        self.sensors['source-name'].set_value(scan.get('source_name', '-'), timestamp=timestamp)


async def main():
    """Wrapper to start the server"""
    parser = getArgumentParser()
    args = parser.parse_args()
    mpikat.core.logger.setLevel(args.log_level.upper())

    try:
        datastore = EDDDataStore(args.redis_ip, args.redis_port)
        datastore.ping()
    except ConnectionError:
        _log.warning('Datastore not avaialble at %s:%i - not updating redis!', args.redis_ip, args.redis_port)
        datastore = None
    server = SKAMPIStatusServer(
        args.host, args.port,
        datastore)
    await launchPipelineServer(server, args)



if __name__ == "__main__":
    asyncio.run(main())
