"""
Telescope meta information (TMI) server. Interface to telescope control systems, that
passively collects information about the telescope status and updates the
datastore accordingly. Has to be sub-classed for every telescope.
"""

import asyncio
import socket
import json
import datetime

import aiokatcp
from aiokatcp.server import RequestContext
from aiokatcp.sensor import Sensor
from aiokatcp import FailReply

from mpikat.core.edd_pipeline_aio import launchPipelineServer, getArgumentParser

from mpikat.core.datastore import EDDDataStore
from mpikat.core import logger as logging
from mpikat.utils import get_port

_log = logging.getLogger("mpikat.core.telescope_meta_information_server")

class OverridableSensor(Sensor):
    """
    Sensor with a hidden value override for the TMI
    """
    def __init__(self, *args, **kwargs):
        self._override = False
        try:
            self.datastore = kwargs.pop('datastore')
        except KeyError:
            self.datastore = None
        super().__init__(*args, **kwargs)

        if self.description is None:
            self.description = ""
        if self.datastore:
            params = {'type': self.stype.__name__, 'default': self.value, 'description': self.description, 'overriden': str(self._override)}
            self.datastore.addTelescopeDataItem(self.name, params)

    @property
    def override(self):
        """If true, the sensor is manually overridden"""
        return self._override

    @override.setter
    def override(self, value):
        self._override = value
        if self.datastore:
            self.datastore.setTelescopeDataItem(self.name, str(self._override), field='overriden')

    def set_value(self, *args, **kwargs):
        _log.debug('Setting value for sensor %s', self.name)
        if self.override:
            return
        super().set_value(*args, **kwargs)
        if self.datastore:
            _log.debug('Add value to datastore')
            self.datastore.setTelescopeDataItem(self.name, self.value, timestamp=self.timestamp)

    def override_value(self, *args, **kwargs):
        """Set a value even though the sensor is overridden"""
        super().set_value(*args, **kwargs)
        if self.datastore:
            self.datastore.setTelescopeDataItem(self.name, self.value, timestamp=self.timestamp)


class TMIServer(aiokatcp.server.DeviceServer):
    """ Telescope Meta Data server for the EDD
    """
    VERSION = "mpikat-edd-TMIServer-0.2"
    BUILD_STATE = "mpikat-edd-TMIServer-0.2"

    def add_sensor(self, *args, **kwargs):
        """
        Add a new overridable sensor that is mirrored to the data store
        """
        _log.debug('Add sensor with %s', args)
        _log.debug('Add sensor with %s', kwargs)
        self.sensors.add(OverridableSensor(*args, **kwargs, datastore=self.datastore))

    async def TMIupdate(self):
        """
        Periodic updates of the TMI server value in case of a db flush.
        """

        while True:
            self.datastore.setTelescopeDataItem('TMIServer', self.tmi_connection)
            await asyncio.sleep(10.)


    def __init__(self, ip, port, datastore=None,  loop=None):
        if port == 0:
            port = get_port()
        super().__init__(ip, port, loop=loop)
        _log.info("Accepting connections from: {}:{}".format(ip, port))
        self.tmi_connection = f"{socket.gethostname()}:{port}"

        self.datastore = datastore

        self.sensors.add(Sensor(
            str, name="edd-log-level",
            description="Log level set yo the status server",
            default=logging.getLevelName(),
            initial_status=Sensor.Status.NOMINAL))

        self.add_sensor(
            str, name="observation-id",
            description="Identifier for the current observation",
            default="",
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            str, name="source-name",
            description="Name of the currently observed source",
            default="",
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="ra",
            description="EQ2000 right ascension [deg] of the telesope target",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="dec",
            description="EQ2000 declination [deg] of the telesope target",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            str, name="receiver",
            description="Name of the current receiver",
            default="",
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            str, name="project",
            description="Identifier of the current project",
            default="",
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="wind-speed",
            description="On site wind speed [m/s]",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="wind-direction",
            description="On site wind direction [deg] (0 degree is to the North)",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="air-temperature",
            description="On site air temperature [deg C]",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="air-pressure",
            description="On site air pressure [hPa]",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="humidity",
            description="On site relative humidity [%]",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            float, name="rain",
            description="On site rain",
            default=0.0,
            initial_status=Sensor.Status.UNKNOWN)
        self.add_sensor(
            str, name="date",
            description="current date",
            default="YYYY-MM-DD",
            initial_status=Sensor.Status.UNKNOWN)



    async def start(self):
        _log.debug("Starting server")
        await super().start()
        if self.datastore:
            self.datastore.addTelescopeDataItem('TMIServer', {'description': 'Connection to the TMI server.', 'default': 'unconnected'})
            self.datastore.addTelescopeDataItem('noise_diode_pattern', {'description': 'Firing pattern of the noise diode.', 'default': '{"percentage": 0.5, "period": 0.5, "starttime": "now"}'})
            self.datastore.setTelescopeDataItem('TMIServer', self.tmi_connection)
            self.add_service_task(asyncio.create_task(self.TMIupdate()))



    async def request_set_log_level(self, ctx: RequestContext, level: str="INFO") -> str:
        """Sets the log level
        """
        _log.info("Setting log level to: {}".format(level.upper()))
        logging.setLevel(level.upper())
        self.sensors['edd-log-level'].set_value(level.upper(), status=Sensor.Status.NOMINAL)
        _log.debug("Successfully set log-level")

    async def request_log_level(self, ctx: RequestContext) -> str:  # We want to command our own logger only. pylint: disable=arguments-differ
        """Returns the log level
        """
        return self.sensors['edd-log-level'].value


    async def request_set_katcp_internal_log_level(self, ctx: RequestContext, msg: str) -> str:
        """Set the katcp internal log level
        """
        return await super().request_log_level(ctx, msg)


    async def ensure_update(self, cfg=None):
        """
        Ensures that the values have been updated. May be passed a dictionary containing the values.
        """

    async def request_ensure_update(self, ctx: RequestContext, msg: str) -> str:
        """Ensure that the values have been updated"""
        _log.debug('Received enusre-update request')
        cfg = json.loads(msg)
        await self.ensure_update(cfg)
        d = datetime.datetime.now()
        self.sensors['date'].value = f'{d.year}-{d.month:02}-{d.day:02}'

    async def request_sensor_control(self, ctx: RequestContext, msg: str) -> str:
        """Control a sensor manually, i.e. exclude the sensor from future updates"""
        _log.debug('Controling sensor %s', msg)
        if msg in self.sensors:
            self.sensors[msg].override = True
        else:
            raise FailReply(f"Unknown sensor '{msg}' -> Use valid sensor name")

    async def request_sensor_control_all(self, ctx: RequestContext) -> str:
        """Control a sensor manually, i.e. exclude the sensor from future updates"""
        _log.debug('Controling all sensors')
        for s in self.sensors.values():
            s.override = True

    async def request_sensor_release(self, ctx: RequestContext, msg: str) -> str:
        """Release a sensor from manual control"""
        _log.debug('Releasing sensor %s', msg)
        if msg in self.sensors:
            self.sensors[msg].override = False
        else:
            raise FailReply(f"Unknown sensor '{msg}' -> Use valid sensor name")

    async def request_sensor_release_all(self, ctx: RequestContext) -> str:
        """Release all sensors from manual control"""
        _log.debug('Releasing all sensors')
        for s in self.sensors.values():
            s.override = False

    async def request_sensor_set(self, ctx: RequestContext, sensor_name: str, value: str) -> str:
        """Control a sensor manually, i.e. exclude the sensor from future updates"""
        if sensor_name  not in self.sensors:
            raise FailReply(f"Unknown sensor '{sensor_name}' -> Use valid sensor")
        if not self.sensors[sensor_name].override:
            raise FailReply(f"Sensor '{sensor_name}' not controlled manually. Controls ensor first before setting a value ")
        self.sensors[sensor_name].override_value(self.sensors[sensor_name].stype(value))

    async def request_sensor_list_controlled(self, ctx: RequestContext) -> str:
        """List all controlled sensors"""
        result = []
        for r, s in self.sensors.items():
            if isinstance(s, OverridableSensor) and  s.override:
                result.append(r)

        return ", ".join(result)



async def main():
    """Async wrapper for main"""
    parser = getArgumentParser()
    args = parser.parse_args()
    logging.setLevel(args.log_level.upper())

    try:
        datastore = EDDDataStore(args.redis_ip, args.redis_port)
        datastore.ping()
    except ConnectionError:
        _log.warning('Datastore not avaialble at %s:%i - not updating redis!', args.redis_ip, args.redis_port)
        datastore = None
    server = TMIServer(
        args.host, args.port,
        datastore)
    await launchPipelineServer(server, args)


if __name__ == "__main__":
    asyncio.run(main())
