import copy

from mpikat.core import logger
import mpikat.utils.numa as numa

log = logger.getLogger('mpikat.core_manager')

class CoreManager(object):
    """
    Manage cores on one numa nude to reserve for one tasksets
    """
    def __init__(self, numa_node='0', task_override=None):
        """
        Args:
            numa_node: Numa node to place tasks on
            task_override: Dictionary of tasks with explicit core placing
        """
        self.__numa_node = numa_node
        self.__tasks = {}
        self.__new_task = True
        if task_override is None:
            task_override = {}
        self.__task_override = task_override

    def add_task(self, taskname, requested_cores, prefere_isolated=False, require_isolated=False):
        """
        Add a new task to the core manager.

        Args:
            taskname: name of the task
            requested_cores: number of cores required for the task
            prefere_isolated: If true, prefere isolated cores over non isolated
            require_isolated: If true, will be only placed on isolated cores
        """
        log.debug("Task {}: Requeste cores {} ".format(taskname, requested_cores))
        if not self.__new_task:
            log.warning("Core Management for node {} was already finalized, reevaluating core usage!".format(self.__numa_node))
            self.__new_task = True
        self.__tasks[taskname] = dict(requested_cores=requested_cores, prefere_isolated=prefere_isolated, require_isolated=require_isolated)

    @property
    def tasknames(self) -> list:
        return list(self.__tasks.keys())

    def __finalize_cores(self):
        self.__new_task = False
        numa_info = numa.getInfo()[self.__numa_node]

        cores = copy.deepcopy(numa_info['cores'])
        iso_cores = copy.deepcopy(numa_info['isolated_cores'])
        # ToDo: Add advanced checks so that tasks requiring isolated cores get them first etc.

        for taskname, taskprops in self.__tasks.items():
            if taskname in self.__task_override and self.__task_override[taskname] != 'auto':
                log.debug("Task %s: Using override %s", taskname, self.__task_override[taskname])
                taskprops['reserved_cores'] = self.__task_override[taskname].split(",")
            elif taskprops['require_isolated']:
                log.debug("Task {}: Requeste cores {} - available: {}".format(taskname, taskprops['requested_cores'], len(iso_cores)))
                if len(iso_cores) < taskprops['requested_cores']:
                    raise RuntimeError("Task {} requires {} isolated cores, but only {} are available.".format(taskname, taskprops['requested_cores'], len(iso_cores)))
                taskprops['reserved_cores'] = iso_cores[:taskprops['requested_cores']]
                iso_cores = iso_cores[taskprops['requested_cores']:]
            else:
                if taskprops['prefere_isolated']:
                    if len(iso_cores) < taskprops['requested_cores']:
                        cores = iso_cores + cores
                        iso_cores = []
                    else:
                        cores = iso_cores[:taskprops['requested_cores']] + cores
                        iso_cores = iso_cores[taskprops['requested_cores']:]

                if len(cores) < taskprops['requested_cores']:
                    cores = cores + iso_cores
                    iso_cores = []
                if len(cores) < taskprops['requested_cores']:
                    log.warning("Requested reservation of {} cores for task {}, but only {} available on node!".format(taskprops['requested_cores'], taskname, len(cores)))
                    taskprops['reserved_cores'] = numa_info['cores']
                else:
                    taskprops['reserved_cores'] = cores[:taskprops['requested_cores']]
                    cores = cores[taskprops['requested_cores']:]
                log.debug("Assigned cores: %s", ", ".join(cores))


    def get_cores(self, taskname):
        """
        Get list of cores reserved for the task
        """
        if self.__new_task:
            self.__finalize_cores()
        return self.__tasks[taskname]['reserved_cores']

    def get_coresstr(self, taskname):
        """
        Get list of cores reserved for the task as comma seperated string ready to be used e.g. for taskset.
        """
        return ",".join(self.get_cores(taskname))
