HEADER       DADA                # Distributed aquisition and data analysis
HDR_VERSION  1.0                 # Version of this ASCII header
HDR_SIZE     4096                # Size of the header in bytes

DADA_VERSION 1.0                 # Version of the DADA Software
PIC_VERSION  1.0                 # Version of the PIC FPGA Software

# DADA parameters
OBS_ID       0000_0          # observation ID
PRIMARY      unset               # primary node host name
SECONDARY    unset               # secondary node host name
FILE_NAME    unset               # full path of the data file

FILE_SIZE    5120000000          # requested size of data files
FILE_NUMBER  0                   # number of data file

# time of the rising edge of the first time sample
UTC_START    2024-03-01-12:40:25.641268               # yyyy-mm-dd-hh:mm:ss.fs
MJD_START    57000

OBS_OFFSET   0                   # bytes offset from the start MJD/UTC
OBS_OVERLAP  0                   # bytes by which neighbouring files overlap

# description of the source
SOURCE       B1937+21               # name of the astronomical source
CALFREQ      1
MODE         PSR
RA           08:12:00               # Right Ascension of the source
DEC          -45:00:00               # Declination of the source

# description of the instrument
TELESCOPE    Effelsberg       # telescope name
INSTRUMENT   EDD              # instrument name
RECEIVER     P217           # Frontend receiver
FREQ         1400           # centre frequency in MHz
BW           1600.0           # bandwidth of in MHz (-ve lower sb)
TSAMP        0.0003125       # sampling interval in microseconds
BYTES_PER_SECOND  6400000000.0

NBIT              8              # number of bits per sample
NDIM              1               # 1=real, 2=complex
NPOL              2                 # number of polarizations observed
NCHAN             1                 # number of frequency channels
RESOLUTION        1
DSB               0

#MeerKAT specifics
DADA_KEY     dada                    # The dada key to write to
DADA_MODE    4                       # The mode, 4=full dada functionality
ORDER        FTP                       # Here we are only capturing one polarisation, so data is time only
SYNC_TIME    0
CLOCK_SAMPLE  3200000000.0
SAMPLE_CLOCK 3200000000.0
PACKET_SIZE 8400
MCAST_SOURCES 225.0.0.156+2,225.0.0.156+2   # 239.2.1.150 (+7)
PORT         7148
UDP_IF       unset
IBV_IF       10.42.0.207  # This is the ethernet interface on which to capture
IBV_VECTOR   -1          # IBV forced into polling mode
IBV_MAX_POLL 10
BUFFER_SIZE 16777216
SAMPLE_CLOCK_START  unset # This should be updated with the sync-time of the packetiser to allow for UTC conversion from the sample clock
HEAP_NBYTES    4096
#SPEAD specifcation for EDD packetiser data stream
NINDICES    2   
# The first index item is the running timestamp
IDX1_ITEM   0      # First item of a SPEAD heap
IDX1_STEP   4096   # The difference between successive timestamps
IDX1_MODULO 3200000000.0

IDX2_ITEM   1
IDX2_LIST   0,1
IDX2_MASK   0x1

SLOTS_SKIP 0
DADA_NSLOTS 3

