# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring
import unittest
import tempfile
import os
import asyncio 

from mpikat.utils.process_tools import ManagedProcessPool, ManagedProcess, command_watcher, ProcessException
from subprocess import Popen, PIPE

class Test_ManagedProcessPool(unittest.TestCase):

    def setUp(self) -> None:
        self.pool = ManagedProcessPool()

    def tearDown(self) -> None:
        self.pool.terminate()
        self.pool.clean()

    def test_add_valid_object(self):
        for __ in range(10):
            self.pool.add(ManagedProcess("sleep 1"))
        self.assertEqual(len(self.pool), 10)

    def test_add_invalid_object(self):
        for __ in range(10):
            self.pool.add("dummy")
        self.assertEqual(len(self.pool), 0)

    def test_add_existing_object(self):
        proc = ManagedProcess("sleep 1")
        self.pool.add(proc)
        self.pool.add(proc)
        self.assertEqual(len(self.pool), 1)

    def test_terminate(self):
        for __ in range(5):
            self.pool.add(ManagedProcess("sleep 60"))
        self.pool.terminate(timeout=2)
        for proc in self.pool:
            self.assertFalse(proc.is_alive())
        for val in self.pool.alive().values():
            self.assertFalse(val)
        self.pool.clean()
        self.assertEqual(len(self.pool), 0)

    def test_terminate_threaded(self):
        for __ in range(10):
            self.pool.add(ManagedProcess("sleep 60"))
        self.pool.terminate(timeout=2, threaded=True)
        for proc in self.pool:
            self.assertFalse(proc.is_alive())
        for val in self.pool.alive().values():
            self.assertFalse(val)
        self.pool.clean()
        self.assertEqual(len(self.pool), 0)

    def test_clean(self):
        for __ in range(10):
            self.pool.add(ManagedProcess("sleep 60"))
        self.pool.terminate(timeout=2, threaded=True)
        self.pool.add(ManagedProcess("sleep 60"))
        self.pool.clean()
        self.assertEqual(len(self.pool), 1)



class TestCommandWatcher(unittest.IsolatedAsyncioTestCase):
    async def test_decode(self):
        """
        Should decode unicode output
        """

        stdout, stderr = await command_watcher("echo â")
        self.assertEqual(stdout.strip(), "â")

    async def test_non_existing_programm(self):
        """
        Should raise on error
        """
        with self.assertRaises(FileNotFoundError):
            await command_watcher("NON_EXISTING_COMMAND")

    async def test_allow_fail_non_existing_program(self):
        """
        Allow fail option should not throw
        """
        stdout, stderr = await command_watcher("NON_EXISTING_COMMAND", allow_fail=True)

    async def test_raise_return_code(self):
        """
        Should raise on error
        """
        with self.assertRaises(ProcessException):
            await command_watcher("false")

    async def test_noraise_allow_fail(self):
        """
        Should not raise on error
        """
        await command_watcher("false", allow_fail=True)


    async def test_timeout(self):
        """
        Should raise on error
        """
        with self.assertRaises((TimeoutError, asyncio.exceptions.TimeoutError)):
            await command_watcher("sleep 1", timeout=0.1)

    async def test_timeout_allow_fail(self):
        """
        Should not raise on error
        """
        await command_watcher("sleep 1", timeout=0.1, allow_fail=True)


    async def test_environment_propagation(self):
        """ Environemnt variable should be added to th executing environment"""
        stdout, stderr = await command_watcher("bash -c 'echo $FOO'", env={'FOO': 'BAR'})
        self.assertEqual(stdout.strip(), 'BAR')

    async def test_environment_inheritance(self):
        """ Command shoul inherit environment"""

        os.environ["ISHOULDNOTEXIST42"] = "FOO"
        stdout, stderr = await command_watcher("bash -c 'echo $ISHOULDNOTEXIST42'")
        self.assertEqual(stdout.strip(), 'FOO')



if __name__ == "__main__":
    unittest.main()
