import tempfile
import typing as tp
from dataclasses import dataclass, asdict
import mpikat.core.logger
import json

_LOG = mpikat.core.logger.getLogger("mpikat.utils.dada_tools.header")

# pylint: disable=too-many-instance-attributes
@dataclass
class DadaBaseHeader:
    """A data class representing the properties of a DadaHeader
    """
    # ---------------------- #
    # Dada header parameters # - may moved to another data class
    # ---------------------- #
    header: str = 'DADA'
    hdr_version: str = 1
    hdr_size: int = 4096
    dada_version: float = 1.0
    dada_key: str = 'unset'              # PSRDADA ring-buffer key
    file_size: int = 67108864
    utc_start: str = "unset"
    dim: int = 1
    nbit: int = 8
    bytes_per_second: int = "unset"

@dataclass
class SigProcHeader:
    """A data class representing the properties of a SigProcHeader
    """
    # ------------------------------- #
    # Observation specific parameters #
    # ------------------------------- #
    telescope: str = "unset"
    receiver: str = "unset"
    instrument: str = "unset"
    obs_offset : str = "unset"
    resolution: str = "unset"
    frequency: float = .0
    bandwidth: float = .0
    source_name: str = "unset"
    source_ra: float = .0
    source_dec: float = .0
    beam_id: int = 0

def make_dada_header(header: dataclass=DadaBaseHeader(), ignore_unset=True) -> str:
    """Creates a temporary file containing Mkrecv header information

    Args:
        header (dataclass): The dataclass of the header
        ignore_unset (bool, optional): Ignores all item which are set to 'unset'. Defaults to True.

    Returns:
        str: The path of the tempfile
    """
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as f:
        for key, val in asdict(header).items():
            if val == "unset" and ignore_unset:
                continue
            f.write(f"{key.upper()} {val}\n")
    return f.name


class DadaHeader:
    """The DadaHeader allows to manipulate, write and read DADA header.
    """
    def __init__(self):
        """Constructor
        """
        self.header_file = ""
        self._header_list: tp.List[dataclass] = []
        self._addtional_params: dict = {}

    def __getattr__(self, name):
        """_summary_

        Args:
            name (_type_): _description_

        Returns:
            _type_: _description_
        """
        for header in self._header_list:
            if hasattr(header, name):
                return getattr(header, name)
        return getattr(self, name)

    def asdict(self) -> dict:
        d = dict(self._addtional_params)
        for header in self._header_list:
            d.update(asdict(header))
        return d


    def set(self, key: str, value: any):
        """Sets the value of an existing parameter or creates a parameter if not exists already

        Args:
            key (str): Name of the parameter
            value (any): Value of the parameter
        """
        _LOG.debug("DadaHeader setting key %s to value %s", key, value)
        for header in self._header_list:
            if hasattr(header, key):
                _LOG.debug("Header %s has key %s", type(header), key)
                setattr(header, key, value)
                return
        _LOG.warning("Key %s does not exists yet, adding addtional parameter", key)
        self.add_param(key, value)

    def get(self, key: str) -> any:
        """Get the value of an parameter

        Args:
            key (str): The name of the parameter

        Returns:
            any: The value, return None if parameter does not exist
        """
        for header in self._header_list:
            if hasattr(header, key):
                return getattr(header, key)
        for k, v in self._addtional_params.items():
            if key == k:
                return v
        _LOG.warning("Key %s does not exists")
        return None

    def is_key(self, key: str) -> bool:
        """Checks if key exists in the current DadaHeader

        Args:
            key (str): Name of the key

        Returns:
            bool: Return True if it exists otherwise False
        """
        if self.get(key) is None:
            return False
        return True


    def add(self, header: any): #, overwrite: bool=True):
        """Add a dataclass to the DadaHeader

        Args:
            header (dataclass): The dataclass
        """
        if isinstance(header, type(self)):
            for hdr in header._header_list:
                if type(hdr) in self._header_list: # and not overwrite:
                    continue
                self._header_list.append(hdr)
            self._addtional_params.update(header._addtional_params)
        else:
            self._header_list.append(header)

    def update(self, input_dict: dict=None, mapping: dict=None):
        """Updates all added dataclasses by the given input dictioanry

        Args:
            input_dict (dict, optional): The dictionary. Defaults to None.
            mapping (dict, optional): A dictioanry that allows to map between input_dict and
                dataclasses. Defaults to None.
        """
        if input_dict is not None:
            for header in self._header_list:
                temp_dict: dict = dict({})
                if isinstance(mapping, dict):
                    for old_key, new_key in mapping.items():
                        if old_key in input_dict and hasattr(header, new_key):
                            temp_dict[new_key] = input_dict[old_key]
                temp_dict.update({key: value for key, value in input_dict.items() if hasattr(header, key)})
                _LOG.debug("Updating header %s with dict %s", type(header), json.dumps(temp_dict, indent=4))
                header.__dict__.update(temp_dict)

    def write(self, ignore_unset=True) -> str:
        """Writes all parameters of added headers to an ASCII file

        Args:
            ignore_unset (bool, optional): Ignore all 'unset' parameters. Defaults to True.

        Returns:
            str: The name of the written file
        """
        with tempfile.NamedTemporaryFile(delete=False, mode="w") as f:
            for header in self._header_list:
                for key, val in asdict(header).items():
                    if val == "unset" and ignore_unset:
                        continue
                    f.write(f"{key.upper()} {val}\n")
            for key, val in self._addtional_params.items():
                f.write(f"{key.upper()} {val}\n")
        self.header_file = f.name
        return self.header_file

    def read(self) -> dict:
        """Reads an ASCII header and returns the key-value pairs as dictionary

        Returns:
            dict: The dictionary
        """
        return read_header(self.header_file)

    def add_param(self, key: str, val: any):
        """Adds an addtional paramer to the DADA header.

        Note:
            If a key is added which exists in one the added dataclasses (e.g. DadaBaseHeader)
            this param will overwrite its value

        Args:
            key (str): The key of the parameter.
            val (any): The value of the parameter
        """
        self._addtional_params[key] = val

    def rm_param(self, key: str):
        """Remove an addtional parameter

        Note:
            Only removes key-value pairs which have been added with 'add_param'

        Args:
            key (str): _description_
        """
        self._addtional_params.pop(key)



def read_header(fname: str) -> dict:
    """Reads file of the DADA format, parses its content
        and returns it as a dictionary

    Args:
        fname (str): The filename

    Returns:
        dict: The resulting dictionary
    """
    res = {}
    with open(fname, "r", encoding="ascii") as f:
        for line in f:
            line = line.strip()
            if line and not line.startswith("#"):  # Skip empty lines and comments
                key, value = line.split(maxsplit=1)
                key = key.strip()
                value = value.split("#", 1)[0].strip()  # Remove comments
                res[key.lower()] = value
    return res
