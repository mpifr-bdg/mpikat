import unittest
import json
import tempfile
import datetime
import os

from mpikat.core import cfgjson2dict, updateConfig
from mpikat.utils import output

class TestCfgJson2Dict(unittest.TestCase):
    def test_invalid_json(self):
        """
        Should throw on invalid json
        """
        with self.assertRaises(Exception):
            cfgjson2dict("not_valid_json'")

    def test_return_dict(self):
        """
        Should return dict as is
        """
        inp = {'a': 2}
        out = cfgjson2dict(inp)
        self.assertEqual(inp, out)

    def test_return_dict_from_json(self):
        inp = {'a': 2}
        out = cfgjson2dict(json.dumps(inp))
        self.assertEqual(inp, out)

    def test_unhandled_types(self):
        with self.assertRaises(TypeError):
            cfgjson2dict(None)



class TestOutput(unittest.TestCase):
    def test_date_subdir_creation(self):
        with tempfile.TemporaryDirectory() as root:
            d = datetime.datetime(1789, 5, 5)
            path = output.create_datebased_subdirs(root, d)

            self.assertTrue(os.path.isdir(path))

            self.assertTrue(path.startswith(root))

            ps = os.path.split(path)
            for el in ['05', '05', '1789']:
                self.assertEqual(str(el), ps[-1])
                ps = os.path.split(ps[0])

    def test_date_subdir_creation_ifexists(self):
        """Existing directory should throw no error"""
        with tempfile.TemporaryDirectory() as root:
            d = datetime.datetime(1789, 5, 5)
            path = output.create_datebased_subdirs(root, d)
            path = output.create_datebased_subdirs(root, d)


    def test_defaults_to_today(self):
        with tempfile.TemporaryDirectory() as root:
            path = output.create_datebased_subdirs(root)
            self.assertTrue(os.path.isdir(path))

    def test_file_name_creation(self):
        """
        Two filenames should differ and not exist
        """
        with tempfile.TemporaryDirectory() as root:
            f1 = output.create_file_name(root, 'dat')
            f2 = output.create_file_name(root, 'dat')
        self.assertNotEqual(f1, f2)
        self.assertFalse(os.path.isdir(f1))
        self.assertFalse(os.path.isdir(f2))

    def test_file_name_contains_id(self):
        """
        Filename should contain id and suffix 
        """
        with tempfile.TemporaryDirectory() as root:
            f = output.create_file_name(root, 'dat', file_id_no='1234567890')
            self.assertTrue(f.endswith('.dat'))
            self.assertIn('1234567890', f)



class TestUpdateConfig(unittest.TestCase):
    def setUp(self):
        self.cfg = {
                'search1': {'sub1': {'a': 'b', 'd': 'd'}, 'foo': 'foo'},
                'search2': {'foo': 'foo'},
                'foo': { 'foo': 'foo'}
                }

    def test_example(self):
        old = {'a:': 0, 'b': {'ba': 0, 'bb': 0}}
        new = {'a:': 1, 'b': {'ba': 2}}

        self.assertEqual(updateConfig(old, new), {'a:': 1, 'b': {'ba': 2, 'bb': 0}})

    def test_recursive_update(self):
        """
        Should update sub dirs
        """
        c = updateConfig(self.cfg, {"search1": {'sub1': {'a': 'c'}}})
        self.assertEqual(c['search1']['sub1']['a'], 'c')
        self.assertEqual(c['search1']['sub1']['d'], 'd')
        self.assertEqual(self.cfg['search1']['sub1']['a'], 'b' )

    def test_update_of_all(self):
        c = updateConfig(self.cfg, {"*": {'foo': 'bar'}})

        # updated config should contain all keys from input and all should
        # contain the update value 
        for k in self.cfg:
            self.assertEqual(c[k]['foo'], 'bar')

        # input should not be modified
        self.assertEqual(self.cfg['foo']['foo'], 'foo')

    def test_update_of_pattern_only(self):
        c = updateConfig(self.cfg, {"search?": {'foo': 'bar'}})

        self.assertEqual(c['search1']['foo'], 'bar')
        self.assertEqual(c['search2']['foo'], 'bar')
        self.assertEqual(c['foo']['foo'], 'foo')

    def test_update_of_single_only(self):
        c = updateConfig(self.cfg, {"search1": {'foo': 'bar'}})
        self.assertEqual(c['search1']['foo'], 'bar')
        self.assertEqual(c['foo']['foo'], 'foo')
        self.assertEqual(c['search2']['foo'], 'foo')

    def test_raise_on_key_error(self):
        """
        If no match keyword is found a exception should be raised 
        """
        with self.assertRaises(KeyError):
            updateConfig(self.cfg, {"I_HAVE_A+TYPO*": {'foo': 'bar'}})





if __name__ == '__main__':
    unittest.main()
