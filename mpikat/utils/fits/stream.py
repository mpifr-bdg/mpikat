from mpikat.core.data_stream import DataStream, DataStreamRegistry

class FitsGatedSpectrumDataStreamFormat(DataStream):
    """
    Data stream output for the gated spectrometer
    """

    data_items = []
    stream_meta = {
        "type": "FitsGatedSpectrum:1",
        "ip": "",
        "port": "",
        "description": "FITS packet stream of integrated spectra.",
        "central_freq": "",
        "receiver_id": "",
        "band_flip": ""
    }

DataStreamRegistry.register(FitsGatedSpectrumDataStreamFormat)