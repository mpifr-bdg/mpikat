# pylint: disable=protected-access,missing-function-docstring,too-few-public-methods,missing-class-docstring
import unittest

from mpikat.utils.testing import state_change_patch, Test_StateModel, Test_StateModelOnRequests, setup_redis
# Patch the state_change decorator / Must be called before importing pipeline implementations
unittest.mock.patch("mpikat.core.edd_pipeline_aio.state_change", wraps=state_change_patch).start()


from mpikat.pipelines.gated_dual_pol_spectrometer import GatedSpectrometerPipeline
from mpikat.pipelines.gated_full_stokes_spectrometer import GatedFullStokesSpectrometerPipeline
from mpikat.pipelines.master_controller import EddMasterController
from mpikat.pipelines.fits_interface import FitsInterfaceServer
from mpikat.pipelines.hdf5_writer import HDF5Writer
from mpikat.pipelines.mock_fits_writer import MockFitsWriter
from mpikat.pipelines.gs_plotter import GSPlotter
from mpikat.pipelines.digpack_controller import DigitizerControllerPipeline



# ------------------------------------------------ #
# State model test using with direct function call #
# ------------------------------------------------ #
class TEST_GatedDualPolSpectrometer_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(GatedSpectrometerPipeline("localhost", 1234), True)

class TEST_GatedFullStokesSpectrometer_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(GatedFullStokesSpectrometerPipeline("localhost", 1234), True)
class TEST_GSPlotter_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(GSPlotter("localhost", 1234), True)

class TEST_FitsInterfaceServer_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(FitsInterfaceServer("localhost", 1234))

class TEST_HDF5Writer_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(HDF5Writer("localhost", 1234))

class TEST_MockFitsWriter_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(MockFitsWriter("localhost", 1234))

class TEST_DigitizerControllerPipeline_StateModel(Test_StateModel):
    def setUp(self):
        super().setUpBase(DigitizerControllerPipeline("localhost", 1234,"localhost", 1235), True)

class TEST_MasterControler_StateModel(Test_StateModel):
    def setUp(self):
        self.redis = setup_redis("localhost")
        super().setUpBase(EddMasterController("localhost", 1234, "localhost", self.redis.server_config['port'], "", ""))

    def tearDown(self) -> None:
        self.redis.shutdown()
        super().tearDown()

    async def test_unprovisioned(self):
        await self.trigger_transition({
            self.pipeline.provision: "idle",
            self.pipeline.deconfigure: "idle"
        }, 'unprovisioned')


# -------------------------------------------------- #
# State model test using a KATCP Client for requests #
# -------------------------------------------------- #
class TEST_GatedDualPolSpectrometer_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(GatedSpectrometerPipeline, True)

class TEST_GatedFullStokesSpectrometer_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(GatedFullStokesSpectrometerPipeline, True)

class TEST_GSPlotter_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(GSPlotter, True)

class TEST_HDF5Writer_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(HDF5Writer)

class TEST_FitsInterface_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(FitsInterfaceServer)

class TEST_MockFitsWriter_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(MockFitsWriter)

class TEST_DigitizerControllerPipeline_StateModelOnRequests(Test_StateModelOnRequests):
    def setUp(self) -> None:
        return super().setUp()

    async def asyncSetUp(self):
        await super().setUpBase(DigitizerControllerPipeline, True)


if __name__ == "__main__":
    unittest.main()
