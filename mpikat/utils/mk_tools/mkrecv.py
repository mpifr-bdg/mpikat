import spead2
import io
from typing import List, Union
from dataclasses import asdict

from mpikat.core.data_stream import DataStreamRegistry, DataStream
from mpikat.core.logger import getLogger
from mpikat.utils.ip_utils import ips_from_stream_description
from mpikat.utils.process_tools import ManagedProcess
from mpikat.utils.dada_tools import DadaClient, DadaHeader, DadaBaseHeader

from .mkheader import MkItem, MkRecvHeader, get_item, validate

_LOG = getLogger("mpikat.utils.mk_tools.mkrecv")

mkrecv_mapping = {
    "sample_rate":"sample_clock",
    "nic":"ibv_if",
    "bit_depth":"nbit",
    "heap_size":"heap_nbytes"
}

def is_hex(s: str) -> bool:
    """Checks if provided string is a hex string

    Args:
        s (str): the string to check

    Returns:
        bool: True if it is a hex string, otherwise False
    """
    try:
        int(s, 16)
    except ValueError:
        return False
    return True

def get_index_mask(mask: Union[int, str]) -> str:
    """Proofs or converts the given input to a hex string
    Args:
        mask (Union[int, str]): The inital mask

    Returns:
        str: Returns the mask. If input is a invalid mask, returns None
    """
    if isinstance(mask, int):
        return hex(mask)
    if isinstance(mask, str):
        if is_hex(mask):
            return mask
    return None

def get_index_list(index_list: Union[range, str]) -> str:
    """Proofs or converts the given input to a Mk index list

    Args:
        index_list (Union[range, str]): The inital index list

    Returns:
        str: returns modified index list, returns None if input is an invalid list
    """
    if isinstance(index_list, range):
        return f"{index_list.start}:{index_list.stop}:{index_list.step}"
    if isinstance(index_list, str):
        if "," in index_list or ":" in index_list:
            return index_list
    return None

# def arithmetic_squence(seq: Union[str, list]):
#     """_summary_

#     Args:
#         s (str): _description_

#     Returns:
#         _type_: _description_
#     """
#     if isinstance(seq, str):
#         elements = [int(element) for element in seq.split(",")]
#     else:
#         elements = [int(element) for element in seq]
#     try:
#         step = elements[1] - elements[0]
#         if len(elements) > 1 and all(elements[i] - elements[i-1] == step for i in range(1, len(elements))):
#             r = range(elements[0], elements[-1] + step, step)
#             return f"{r.start}:{r.stop}:{r.step}"
#     except:
#         return seq

def create_mkrecv_items(streams: List[dict], items: List[spead2.Item], sort=True) -> List[MkItem]:
    """Creates a list of MkItem's. If the stream descriptions contains
        keys which match the spead2.Item.name's, the MkItem.list is automatically
        assigned by concatenating values with the same key.
    Example:
        streams = [{'polarization':0},{'polarization':1}] results in an MkItem.list='0,1'

    Args:
        streams (List[dict]): List of data streams
        items (List[spead2.Item]): List of spead2.Items

    Returns:
        List[MkItem]: _description_
    """
    if not isinstance(streams, list):
        streams = [streams]
    res: List[MkItem] = []
    for item in items:
        index_list = set()
        res.append(MkItem(name=item.name))
        for stream in streams:
            for key, val in stream.items():
                if item.name in key and val is not None:
                    index_list.add(str(val))
        if len(index_list) > 0:
            if sort: res[-1].list=",".join(sorted(index_list))
            else: res[-1].list=",".join(index_list)
    return res

def auto_tune_cores(ip_list: list, core_list: list) -> tuple:
    """Parses core and ip list to find a good solution for mst, ptc and atc

    Args:
        ip_list (list): list of multicast address
        core_list (list): list of use CPU cores

    Returns:
        tuple: (mst, ptc, atc)
    """
    n_ips = len(ip_list)
    n_core = len(core_list)
    # More cores than necessary
    if n_ips + 2 <= n_core:
        return (1, core_list[1], core_list[0])
    if n_ips + 1 == n_core:
        return (1, -1, core_list[0])
    # Only capture threads are asssigned to cores
    if n_ips % n_core == 0:
        return n_ips//n_core, -1, -1
    # One core more -> AUX thread gets a core
    if n_ips % (n_core-1) == 0:
        return n_ips//(n_core-1), -1, core_list[0]
    # Two cores more -> AUX and Switching thread get a core each
    if n_ips % (n_core-2) == 0:
        return n_ips//(n_core-2), core_list[1], core_list[0]
    # No match between cores and MC stream
    return n_ips//n_core, -1, -1


def write_recv_item(fid: io.TextIOWrapper, item: MkItem):
    """Writes an item to a opend file as it is expected by Mkrecv

    Args:
        fid (io.TextIOWrapper): The file pointer to the opened file
        item (MkItem): The item to write to the file
    """
    if item.index is not None and item.index > 0:
        for key, val in asdict(item).items():
            if val is None: continue
            if key == "name": continue
            if key == "index": key = "item"; val = f"{int(val)-1} # {item.name}"
            fid.write(f"IDX{item.index}_{key.upper()} {val}\n")
        fid.write("\n")


def write_recv_header(header: DadaHeader, items: List[MkItem]=[], ignore_unset=True) -> str:
    """Creates a temporary file containing Mkrecv header information

    Args:
        header (MkHeader): The MkHeader
        items (List[MkItem], optional): A list of all items going to the file. Defaults to [].
        ignore_unset (bool, optional): Ignores all item which are set to 'unset'. Defaults to True.

    Returns:
        str: The path of the tempfile
    """
    fname = header.write(ignore_unset)
    with open(fname, "a") as f:
        f.write("# Spead Items\n")
        for item in items:
            write_recv_item(f, item)
    return fname


class Mkrecv(DadaClient):

    def __init__(self, streams: List[dict], key: str, physcpu: str="",
                 ibv: bool=True, mapping=mkrecv_mapping):
        """The Mkrecv wrapper object.

        Args:
            streams (List[dict]): The data streams from which information are taken
            key (str): The dada key to connect to
            physcpu (str, optional): The physical CPU to run the CLI on. Defaults to "".
            ibv (bool, optional): Use ibv or udp -> ibv for maximum performance. Defaults to True.
            mapping (dict, optional): A mapping dictionary allowing to translate streams
                to mkheader. Defaults to None.
        """
        super().__init__(key, physcpu=physcpu)
        self.mapping = mapping
        self.header = DadaHeader()
        self.header.add(DadaBaseHeader())
        self.header.add(MkRecvHeader())

        if not isinstance(streams, list):
            streams = [streams]

        if not ibv:
            self.mapping["nic"] = "udp_if"

        self.header.update(streams[0], self.mapping)
        self.header.set("dada_key", key)

        self.formats: List[DataStream] = list(set(
            [DataStreamRegistry.get_class(stream["format"]) for stream in streams]))

        self.items: List[MkItem] = create_mkrecv_items(streams,
            [item for sublist in [f.data_items for f in self.formats] for item in sublist])

        self.ip_list: List[str] = ips_from_stream_description(streams)

    def start(self, env: dict=None) -> ManagedProcess:
        """Start and return the ManagedProcess

        Args:
            env (dict, optional): Environmental variables to run the command with. Defaults to None.

        Returns:
            ManagedProcess: The running process
        """
        # Prepare MkItems
        items = [item for item in self.items if item.index is not None]
        items.sort(key=lambda x: x.index)
        self.header.set("nindices", len(items))
        self.cmd += "mkrecv_v4 --quiet --dnswtwr"
        # Auto tune core usage
        if self._physcpu:
            core_list = self._physcpu.split(",")
            mst, ptc, atc = auto_tune_cores(self.ip_list, core_list)
            self.header.set("nthreads", len(core_list))
            if atc != -1:
                self.header.set("nthreads", self.header.get("nthreads") - 1)
            if ptc != -1:
                self.header.set("nthreads", self.header.get("nthreads") - 1)
            self.cmd += f" --mst {mst} --atc {atc}"
        else:
            _LOG.warning("No auto tuning for core usage")
            self.header.set("nthreads", 1)
        validate(self)
        self.header_file = write_recv_header(self.header, items)
        self.cmd += f" --header {self.header_file} {','.join(self.ip_list)}"

        _LOG.info("Starting Mkrecv with cmd: %s", self.cmd)

        return super().start(env)

    def set_item(self, name, key, val):
        """Sets the a property of an item by its name and key
            The property must exists in the Mkitem-dataclass

        Args:
            name (str): The item's name
            key (str): The key/property to set
            val (any): The value of the property
        """
        if key in ["sci", "addr"]:
            _LOG.warning("Item property %s not used for mkrecv", key)
            return
        if key == "list":
            val = get_index_list(val)
        if key == "mask":
            val = get_index_mask(val)
        setattr(get_item(self.items, name), key, val)

    def get_item(self, name: str) -> MkItem:
        """Returns the item by its name

        Args:
            name (str): The item's name

        Returns:
            MkItem: The item
        """
        return get_item(self.items, name)

    def set_header(self, key: str, val: any):
        """Sets an header parameter by given key-value pair

        Args:
            key (str): The name of the parameter to set
            val (any): The value to set
        """
        self.header.set(key, val)

